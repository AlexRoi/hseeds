$(document).ready(function() {

    // detect transit support
    var transitFlag = Modernizr.cssanimations;


    function validation() {
        $('.wForm').each(function() {
            var formValid = $(this);
            formValid.validate({
                showErrors: function(errorMap, errorList) {
                    if (errorList.length) {
                        var s = errorList.shift();
                        var n = [];
                        n.push(s);
                        this.errorList = n;
                    }
                    this.defaultShowErrors();
                },
                invalidHandler: function(form, validator) {
                    formValid.addClass('no_valid');
                    $(formValid).data('validator').focusInvalid();
                },
                submitHandler: function(form) {
                    formValid.removeClass('no_valid');
                    var $form = $(form);
                    formValid.removeClass('no_valid').addClass('success');
                    if (form.tagName === 'FORM') {
                        form.submit();
                    } else {
                        if( $form.data('ajax') ) {
                            if($form.data('loader')) {
                                loader($form.data('loader'), 1);
                            }
                            var data = new FormData();
                            var name;
                            var val;
                            var type;
                            $form.find('input,textarea,select').each(function(){
                                name = $(this).attr('data-name');
                                val = $(this).val();
                                type = $(this).attr('type');
                                if((type != 'checkbox' && name) || (type == 'checkbox' && $(this).prop('checked') && name)) {
                                    if(type == 'file') {
                                        data.append(name, $(this)[0].files[0]);
                                    } else if(type == 'radio' && $(this).prop('checked')) {
                                        data.append(name, val);
                                    } else if(type != 'radio') {
                                        data.append(name, val);
                                    }
                                }
                            });
                            var request = new XMLHttpRequest();
                            request.open("POST", $form.data('ajax'));
                            request.onreadystatechange = function() {
                                var status;
                                var resp;
                                if (request.readyState == 4) {
                                    status = request.status;
                                    resp = request.response;
                                    resp = jQuery.parseJSON(resp);
                                    if (status == 200) {
                                        if( resp.success ) {
                                            if (!resp.noclear) {
                                                $form.find('input').each(function(){
                                                    if( $(this).attr('type') != 'hidden' && $(this).attr('type') != 'checkbox' ) {
                                                        $(this).val('');
                                                    }
                                                });
                                                $form.find('textarea').val('');
                                            }
                                            if (resp.clear) {
                                                for(var i = 0; i < resp.clear.length; i++) {
                                                    $('input[name="' + resp.clear[i] + '"]').val('');
                                                    $('textarea[name="' + resp.clear[i] + '"]').val('');
                                                }
                                            }
                                            if (resp.insert && resp.insert.selector && resp.insert.html) {
                                                $(resp.insert.selector).html(resp.insert.html);
                                            }
                                            if ( resp.response ) {
                                                generate(resp.response, 'success', 5000);
                                            }
                                        } else {
                                            if ( resp.response ) {
                                                generate(resp.response, 'warning');
                                            }
                                        }
                                        if( resp.redirect ) {
                                            if(window.location.href == resp.redirect) {
                                                window.location.reload();
                                            } else {
                                                window.location.href = resp.redirect;
                                            }
                                        }
                                    } else {
                                        alert('Something went wrong.');
                                    }
                                }
                                if($form.data('loader')) {
                                    loader($form.data('loader'), 0);
                                }
                            };
                            request.send(data);
                            return false;
                        }
                    }
                }
            });
        });

        $('.wForm').on('change', '.wFile', function(event) {
            var m = $(this).prop('multiple'),
                f = this.files,
                label = $(this).siblings('.wFileVal'),
                t = label.data('txt');
            if (f.length) {
                if (m) {
                    var v = t[1].replace('%num%', f.length),
                        a = [];
                    for (var i = 0; i < f.length; i++) {
                        a.push(f[i].name);
                    }
                    label.html('<span>' + v + ' <ins>(' + a.join(', ') + ')</ins></span>');
                    $(this).blur();
                } else {
                    label.html(t[1] + ': ' + f[0].name);
                    $(this).blur();
                }
            } else {
                label.html(t[0]);
            }
        });

        /* Без тега FORM */
        $('.wForm').on('click', '.wSubmit', function(event) {
            var form = $(this).closest('.wForm');
            form.valid();
            if (form.valid()) {
                form.submit();
            }
        });

        /* Сброс Без тега FORM */
        $('.wForm').on('click', '.wReset', function(event) {
            var form = $(this).closest('.wForm');
            if (form.is('DIV')) {
                form.validReset();
            }
        });
    }

    validation();

    /* magnificPopup */

    $('body').magnificPopup({
        delegate: '.mfiA',
        callbacks: {
            elementParse: function(item) {
                this.st.ajax.settings = {
                    url: item.el.data('url'),
                    type: 'POST',
                    data: (typeof item.el.data('param') !== 'undefined') ? item.el.data('param') : ''
                };
            },
            ajaxContentAdded: function(el) {
                validation();
            }
        },
        type: 'ajax',
        removalDelay: 300,
        mainClass: 'zoom-in'
    });

    /*wTxt iframe*/
    function wTxtIFRAME() {
        var list = $('.wTxt').find('iframe');
        if (list.length) {
            for (var i = 0; i < list.length; i++) {
                var ifr = list[i];
                var filter = /youtu.be|youtube|vimeo/g; // d
                //if (typeof $(ifr).data('wraped') === 'undefined') { // без фильтра
                if (typeof $(ifr).data('wraped') === 'undefined' && !!ifr.src.match(filter)) {
                    var ratio = (+ifr.height / +ifr.width * 100).toFixed(0);
                    $(ifr).data('wraped', true).wrap('<div class="iframeHolder ratio_' + ratio.slice() + '"></div>');
                }
            }
        }
    }

    if($('.wAccordeon').length) {
        $('.wAccordeon').on('click', '.wAccOpen', function(){
            if(!$(this).hasClass('open')) {
                $(this).addClass('open');
                $(this).next('.wAccList').stop().slideDown(300);
            }
            else {
                $(this).removeClass('open');
                $(this).next('.wAccList').stop().slideUp(300);
            }
        });
    }

    if($('.wCatListBlock').length) {
        $('.wCatListBlock').on('mouseenter', '.wCatLink', function(){
            var data_src = $(this).attr('data-img');
            $('.wCatImg img').attr('src',data_src);
        });
    }

    $(window).load(function() {
        wTxtIFRAME();

        if($('.wSliderMain').length) {
            $('.wSliderMain ul').carouFredSel({
                responsive: true,
                auto: true,
                scroll: {
                    items: 1,
                    duration: 1000,
                    timeoutDuration: 3000,
                    fx: 'crossfade',
                    pauseOnHover: true
                },
                swipe: {
                    onTouch: true,
                    onMouse: false
                }
            }, {
                transition: transitFlag
            });
        }

        if($('.wItemSlider').length) {
            $('.wItemSlider ul').carouFredSel({
                responsive: true,
                auto: true,
                scroll: {
                    items: 1,
                    duration: 1000,
                    timeoutDuration: 3000,
                    fx: 'crossfade',
                    pauseOnHover: true
                },
                prev: '.prev',
                next: '.next',
                swipe: {
                    onTouch: true,
                    onMouse: false
                }
            }, {
                transition: transitFlag
            });
        }

        if($('.wCompSlider').length) {
            $(".wCompSliderList").smoothDivScroll({
                mousewheelScrolling: "allDirections",
                manualContinuousScrolling: true,
                autoScrollingMode: "onStart"
            });
        }

        if($('.wTxt table').length) {
            $('.wTxt table').wrap('<div class="scroll_table"></div>');
        }

    });

});