/*-------------------------------  LANG_CONSTANTS  ------------------------------------*/
    function siteLang(){var t=[true,'ru','ua','pl','en'],i,w=window.navigator,l=document.documentElement.getAttribute("lang")||w.language||w.browserLanguage||w.userLanguage||"en";l=l.toLowerCase().substr(0,2);for(i=1;i<t.length;i++){if(t[i]===l)t[0]=false;};if(t[0]){l='en'};return l;}
    $.LANG_SITE=siteLang();
    var LANG_PLUGIN = Object.create(null);
    
    LANG_PLUGIN.Magnific = {
        ru: {
            tClose: 'Закрыть (ESC)',
            tLoading: 'Загрузка контента ...',
            tNotFound: 'Контент не найден',
            tError: 'Невозможно загрузить <a href="%url%" target="_blank">Контент</a>.',
            tErrorImage: 'Невозможно загрузить <a href="%url%" target="_blank">Изображение #%curr%</a>.',
            tPrev: 'Предыдущая (клавиша Left)',
            tNext: 'Следующая (клавиша Right)',
            tCounter: '%curr% из %total%'
        },
        ua: {
            tClose: 'Закрити (ESC)',
            tLoading: 'Завантаження контенту ...',
            tNotFound: 'Контент не знайдено',
            tError: 'Неможливо завантажити <a href="%url%" target="_blank">контент</a>.',
            tErrorImage: 'Неможливо завантажити <a href="%url%" target="_blank">Зображенння #%curr%</a>.',
            tPrev: 'Попередня (клавіша Left)',
            tNext: 'Наступна (клавіша Right)',
            tCounter: '%curr% з %total%'
        },
        pl: {
            tClose: 'Zamknąć (ESC)',
            tLoading: 'Zawartość do treści ...',
            tNotFound: 'Nie znaleziono treści',
            tError: 'Niemożliwe do ściągnięcia <a href="%url%" target="_blank">treści</a>.',
            tErrorImage: 'Niemożliwe do ściągnięcia <a href="%url%" target="_blank">Obrazów #%curr%</a>.',
            tPrev: 'Poprzednia (klucz Left)',
            tNext: 'Następna (klucz Right)',
            tCounter: '%curr% z %total%'
        },
        en: {
            tClose: 'Close (ESC)',
            tLoading: 'Loading ...',
            tNotFound: 'Content not found',
            tError: '<a href="%url%" target="_blank">The content</a> could not be loaded.',
            tErrorImage: '<a href="%url%" target="_blank">The image #%curr%</a> could not be loaded.',
            tPrev: 'Previous (Left arrow key)',
            tNext: 'Next (Right arrow key)',
            tCounter: '%curr% of %total%'
        }
    };

    LANG_PLUGIN.validate = {
        ru: {
            required: "Это поле необходимо заполнить!",
            password: "Укажите пароль!",
            remote: "Пожалуйста, введите правильное значение!",
            email: "Пожалуйста, введите корректный адрес электронной почты!",
            url: "Пожалуйста, введите корректный URL!",
            date: "Пожалуйста, введите корректную дату!",
            dateISO: "Пожалуйста, введите корректную дату в формате ISO!",
            number: "Пожалуйста, введите число!",
            digits: "Пожалуйста, вводите только цифры!",
            creditcard: "Пожалуйста, введите правильный номер кредитной карты!",
            equalTo: "Пожалуйста, введите такое же значение ещё раз!",
            maxlength: "Пожалуйста, введите не больше {0} символов!",
            minlength: "Пожалуйста, введите не меньше {0} символов!",
            rangelength: "Пожалуйста, введите значение длиной от {0} до {1} символов!",
            range: "Пожалуйста, введите число от {0} до {1}!",
            filetype: "Допустимые расширения файлов: {0}!",
            filesize: "Максимальный размер {0} KB!",
            filesizeEach: "Максимальный размер каждого файла {0} KB!",
            max: "Пожалуйста, введите число, меньшее или равное {0}!",
            min: "Пожалуйста, введите число, большее или равное {0}!",
            // add
            word: "Введите корректное словестное значение!",
            login: "Введите корректный логин!",
            phoneUA: "Укажите корректный номер +38ХХХХХХХХХХ"
        },
        ua: {
            required: "Це поле необхідно заповнити!",
            password: "Вкажіть пароль!",
            remote: "Будь ласка, введіть правильне значення!",
            email: "Будь ласка, введіть коректну адресу електронної пошти!",
            url: "Будь ласка, введіть коректний URL!",
            date: "Будь ласка, введіть коректну дату!",
            dateISO: "Будь ласка, введіть коректну дату у форматі ISO!",
            number: "Будь ласка, введіть число!",
            digits: "Будь ласка, вводите тільки цифри!",
            creditcard: "Будь ласка, введіть правильний номер кредитної картки!",
            equalTo: "Будь ласка, введіть таке ж значення ще раз!",
            maxlength: "Будь ласка, введіть не більш {0} символів!",
            minlength: "Будь ласка, введіть не менш {0} символів!",
            rangelength: "Будь ласка , введіть значення довжиною від {0} до {1} символів!",
            range: "Будь ласка, введіть число від {0} до {1}!",
            filetype: "Допустимые расширения файлов: {0}!",
            filesize: "Максимальный размер {0} KB!",
            filesizeEach: "Максимальный размер каждого файла {0} KB!",
            max: "Будь ласка, введіть число, менше або рівне {0}!",
            min: "Будь ласка, введіть число, більше або рівне {0}!.",
            // add
            word: "Введіть коректне ім'я!",
            login: "Введіть коректний логін!",
            phoneUA: "Введіть корректний номер +38ХХХХХХХХХХ"
        },
        pl: {
            required: "To pole należy wypełnić!",
            password: "Określ paroll!",
            remote: "Proszę, wprowadźcie prawidłowe znaczenie!",
            email: "Proszę wpisać poprawny adres e-mail!",
            url: "Proszę podać poprawny adres URL!",
            date: "Wpisz poprawną datę!",
            dateISO: "Proszę podać poprawną datę w formacie ISO!",
            number: "Proszę wpisać numery!",
            digits: "Proszę wpisać tylko liczby!",
            creditcard: "Proszę podać poprawny numer karty kredytowej!",
            equalTo: "Proszę ponownie wprowadzić wartość!",
            maxlength: "Proszę wpisać nie więcej niż {0} znaków!",
            minlength: "Proszę podać co najmniej {0} znaków!",
            rangelength: "Prosimy podać wartość pomiędzy {0} {1} znaków!",
            range: "Wprowadź liczbę między {0} - {1}!",
            filetype: "Dopuszczalne rozszerzenia plików: {0}!",
            filesize: "Maksymalny rozmiar {0} KB!",
            filesizeEach: "Maksymalny rozmiar każdego plika {0} KB!",
            max: "Podaj numer mniejsza lub równa {0}!",
            min: "Wprowadź liczbę większą lub równą {0}!",
            // add
            word: "Wprowadź poprawne znaczenie słów!",
            login: "Proszę podać poprawną nazwę użytkownika!",
            phoneUA: "Proszę podać poprawny numer +38ХХХХХХХХХХ"
        },
        en: {
            required: "This field is required!",
            password: "Specify paroll!",
            remote: "Please fix this field!",
            email: "Please enter a valid email address!",
            url: "Please enter a valid URL!",
            date: "Please enter a valid date!",
            dateISO: "Please enter a valid date ISO!",
            number: "Please enter a valid number!",
            digits: "Please enter only digits!",
            creditcard: "Please enter a valid credit card number!",
            equalTo: "Please enter the same value again!",
            maxlength: "Please enter no more than {0} characters!",
            minlength: "Please enter at least {0} characters!",
            rangelength: "Please enter a value between {0} and {1} characters long!",
            range: "Please enter a value between {0} and {1}!",
            filetype: "Допустимые расширения файлов: {0}!",
            filesize: "Максимальный размер {0} KB!",
            filesizeEach: "Максимальный размер каждого файла {0} KB!",
            max: "Please enter a value less than or equal to {0}!",
            min: "Please enter a value greater than or equal to {0}!",
            // add
            word: "Please enter the correct word meanings!",
            login: "Please enter a valid username!",
            phoneUA: "Please enter a valid number +38ХХХХХХХХХХ"
        }
    };
/*-------------------------------  validate  ------------------------------------*/

    $.extend($.fn, {
        validateDelegate: function(delegate, type, handler) {
            return this.bind(type, function(event) {
                var target = $(event.target);
                if (target.is(delegate)) {
                    return handler.apply(target, arguments);
                }
            });
        }
    });

    $.extend($.fn, {
    // http://jqueryvalidation.org/validate/
    validate: function(options) {

        // if nothing is selected, return nothing; can't chain anyway
        if (!this.length) {
            if (options && options.debug && window.console) {
                console.warn("Nothing selected, can't validate, returning nothing!");
            }
            return;
        }

        // check if a validator for this form was already created
        var validator = $.data(this[0], "validator");
        if (validator) {
            return validator;
        }

        // Add novalidate tag if HTML5.
        this.attr("novalidate", "novalidate");

        validator = new $.validator(options, this[0]);
        $.data(this[0], "validator", validator);

        if (validator.settings.onsubmit) {

            this.validateDelegate(":submit", "click", function(event) {
                if (validator.settings.submitHandler) {
                    validator.submitButton = event.target;
                }
                // allow suppressing validation by adding a cancel class to the submit button
                if ($(event.target).hasClass("cancel")) {
                    validator.cancelSubmit = true;
                }

                // allow suppressing validation by adding the html5 formnovalidate attribute to the submit button
                if ($(event.target).attr("formnovalidate") !== undefined) {
                    validator.cancelSubmit = true;
                }
            });

            // validate the form on submit
            this.submit(function(event) {
                if (validator.settings.debug) {
                    // prevent form submit to be able to see console output
                    event.preventDefault();
                }

                function handle() {
                    var hidden, result;
                    if (validator.settings.submitHandler) {
                        if (validator.submitButton) {
                            // insert a hidden input as a replacement for the missing submit button
                            hidden = $("<input type='hidden'/>")
                                .attr("name", validator.submitButton.name)
                                .val($(validator.submitButton).val())
                                .appendTo(validator.currentForm);
                        }
                        result = validator.settings.submitHandler.call(validator, validator.currentForm, event);
                        if (validator.submitButton) {
                            // and clean up afterwards; thanks to no-block-scope, hidden can be referenced
                            hidden.remove();
                        }
                        if (result !== undefined) {
                            return result;
                        }
                        return false;
                    }
                    return true;
                }

                // prevent submit for invalid forms or custom submit handlers
                if (validator.cancelSubmit) {
                    validator.cancelSubmit = false;
                    return handle();
                }
                if (validator.form()) {
                    if (validator.pendingRequest) {
                        validator.formSubmitted = true;
                        return false;
                    }
                    return handle();
                } else {
                    validator.focusInvalid();
                    return false;
                }
            });
        }

        return validator;
    },
    // http://jqueryvalidation.org/valid/
    valid: function() {
        var valid, validator, errorList;

        if ($(this[0]).is("form")) {
            valid = this.validate().form();
        } else if ($(this[0]).is("div")) {
            valid = this.validate().form();
        } else {
            errorList = [];
            valid = true;
            validator = $(this[0]).validate();
            this.each(function() {
                valid = validator.element(this) && valid;
                errorList = errorList.concat(validator.errorList);
            });
            validator.errorList = errorList;
        }
        return valid;
    },
    // attributes: space separated list of attributes to retrieve and remove
    removeAttrs: function(attributes) {
        var result = {},
            $element = this;
        $.each(attributes.split(/\s/), function(index, value) {
            result[value] = $element.attr(value);
            $element.removeAttr(value);
        });
        return result;
    },
    // http://jqueryvalidation.org/rules/
    rules: function(command, argument) {
        var element = this[0],
            settings, staticRules, existingRules, data, param, filtered;

        if (command) {
            settings = $.data(element.form, "validator").settings;
            staticRules = settings.rules;
            existingRules = $.validator.staticRules(element);
            switch (command) {
                case "add":
                    $.extend(existingRules, $.validator.normalizeRule(argument));
                    // remove messages from rules, but allow them to be set separately
                    delete existingRules.messages;
                    staticRules[element.name] = existingRules;
                    if (argument.messages) {
                        settings.messages[element.name] = $.extend(settings.messages[element.name], argument.messages);
                    }
                    break;
                case "remove":
                    if (!argument) {
                        delete staticRules[element.name];
                        return existingRules;
                    }
                    filtered = {};
                    $.each(argument.split(/\s/), function(index, method) {
                        filtered[method] = existingRules[method];
                        delete existingRules[method];
                        if (method === "required") {
                            $(element).removeAttr("aria-required");
                        }
                    });
                    return filtered;
            }
        }

        data = $.validator.normalizeRules(
            $.extend({},
                $.validator.classRules(element),
                $.validator.attributeRules(element),
                $.validator.dataRules(element),
                $.validator.staticRules(element)
            ), element);

        // make sure required is at front
        if (data.required) {
            param = data.required;
            delete data.required;
            data = $.extend({
                required: param
            }, data);
            $(element).attr("aria-required", "true");
        }

        // make sure remote is at back
        if (data.remote) {
            param = data.remote;
            delete data.remote;
            data = $.extend(data, {
                remote: param
            });
        }

        return data;
    },
    validReset: function() {
        var ths = $(this[0]);
        var sett = ths.validate().settings;
        reset_InTx(ths.find('input'));
        reset_InTx(ths.find('textarea'));
        reset_Sel(ths.find('select'));

        function reset_InTx(els) {
            for (var i = 0; i < els.length; i++) {
                var t = els[i];
                var jt = $(t);
                switch (t.type) {
                    case 'radio':
                    case 'checkbox':
                        t.checked = t.defaultChecked;
                        break;
                    case 'file':
                        var fV = $(t).siblings('.wFileVal');
                        t.outerHTML = t.outerHTML;
                        fV.html(fV.data('txt')[0]);
                        jt = ths.find('#' + t.id);
                        break;
                    default:
                        t.value = t.defaultValue;
                }
                jt.removeClass(sett.errorClass).trigger('change').siblings(sett.errorElement + '.' + sett.errorClass).css('display', 'none');
            };
        }

        function reset_Sel(els) {
            for (var i = 0; i < els.length; i++) {
                [].forEach.call(els[i].options, function(el) {
                    el.selected = el.defaultSelected;
                });
                $(els[i]).removeClass(sett.errorClass).trigger('change').siblings(sett.errorElement + '.' + sett.errorClass).css('display', 'none');
            };
        }

        }
    });

    // Custom selectors
    $.extend($.expr[":"], {
        // http://jqueryvalidation.org/blank-selector/
        blank: function(a) {
            return !$.trim("" + $(a).val());
        },
        // http://jqueryvalidation.org/filled-selector/
        filled: function(a) {
            return !!$.trim("" + $(a).val());
        },
        // http://jqueryvalidation.org/unchecked-selector/
        unchecked: function(a) {
            return !$(a).prop("checked");
        }
    });

    // constructor for validator
    $.validator = function(options, form) {
        this.settings = $.extend(true, {}, $.validator.defaults, options);
        this.currentForm = form;
        this.init();
    };

    // http://jqueryvalidation.org/jQuery.validator.format/
    $.validator.format = function(source, params) {
        if (arguments.length === 1) {
            return function() {
                var args = $.makeArray(arguments);
                args.unshift(source);
                return $.validator.format.apply(this, args);
            };
        }
        if (arguments.length > 2 && params.constructor !== Array) {
            params = $.makeArray(arguments).slice(1);
        }
        if (params.constructor !== Array) {
            params = [params];
        }
        $.each(params, function(i, n) {
            source = source.replace(new RegExp("\\{" + i + "\\}", "g"), function() {
                return n;
            });
        });
        return source;
    };

    $.extend($.validator, {

        defaults: {
            messages: {},
            groups: {},
            rules: {},
            errorClass: "error",
            validClass: "valid",
            errorElement: "label",
            focusCleanup: false,
            focusInvalid: true,
            errorContainer: $([]),
            errorLabelContainer: $([]),
            onsubmit: true,
            ignore: ":hidden",
            ignoreTitle: false,
            onfocusin: function(element) {
                this.lastActive = element;

                // Hide error label and remove error class on focus if enabled
                if (this.settings.focusCleanup) {
                    if (this.settings.unhighlight) {
                        this.settings.unhighlight.call(this, element, this.settings.errorClass, this.settings.validClass);
                    }
                    this.hideThese(this.errorsFor(element));
                }
            },
            onfocusout: function(element) {
                if (!this.checkable(element) && (element.name in this.submitted || !this.optional(element))) {
                    this.element(element);
                }
            },
            onkeyup: function(element, event) {
                var excludedKeys = [
                    16, 17, 18, 20, 35, 36, 37,
                    38, 39, 40, 45, 144, 225
                ];

                if (event.which === 9 && this.elementValue(element) === "" || $.inArray(event.keyCode, excludedKeys) !== -1) {
                    return;
                } else if (event.which === 13 && element.tagName !== 'TEXTAREA') {
                    if ($(this.currentForm).data('validator').checkForm()) {
                        $(this.currentForm).submit();
                    }
                } else if (element.name in this.submitted || this.isValidElement(element)) {
                    this.element(element);
                }
            },
            onclick: function(element) {
                // click on selects, radiobuttons and checkboxes
                if (element.name in this.submitted) {
                    this.element(element);

                    // or option elements, check parent select in that case
                } else if (element.parentNode.name in this.submitted) {
                    this.element(element.parentNode);
                }
            },
            highlight: function(element, errorClass, validClass) {
                if (element.type === "radio") {
                    this.findByName(element.name).addClass(errorClass).removeClass(validClass);
                } else {
                    $(element).addClass(errorClass).removeClass(validClass);
                }
            },
            unhighlight: function(element, errorClass, validClass) {
                if (element.type === "radio") {
                    this.findByName(element.name).removeClass(errorClass).addClass(validClass);
                } else {
                    $(element).removeClass(errorClass).addClass(validClass);
                }
            }
        },

        // http://jqueryvalidation.org/jQuery.validator.setDefaults/
        setDefaults: function(settings) {
            $.extend($.validator.defaults, settings);
        },

        messages: {
            required: LANG_PLUGIN.validate[$.LANG_SITE].required,
            password: LANG_PLUGIN.validate[$.LANG_SITE].password,
            remote: LANG_PLUGIN.validate[$.LANG_SITE].remote,
            email: LANG_PLUGIN.validate[$.LANG_SITE].email,
            url: LANG_PLUGIN.validate[$.LANG_SITE].url,
            date: LANG_PLUGIN.validate[$.LANG_SITE].date,
            dateISO: LANG_PLUGIN.validate[$.LANG_SITE].dateISO,
            number: LANG_PLUGIN.validate[$.LANG_SITE].number,
            digits: LANG_PLUGIN.validate[$.LANG_SITE].digits,
            creditcard: LANG_PLUGIN.validate[$.LANG_SITE].creditcard,
            equalTo: LANG_PLUGIN.validate[$.LANG_SITE].equalTo,
            maxlength: $.validator.format(LANG_PLUGIN.validate[$.LANG_SITE].maxlength),
            minlength: $.validator.format(LANG_PLUGIN.validate[$.LANG_SITE].minlength),
            rangelength: $.validator.format(LANG_PLUGIN.validate[$.LANG_SITE].rangelength),
            range: $.validator.format(LANG_PLUGIN.validate[$.LANG_SITE].range),
            filetype: $.validator.format(LANG_PLUGIN.validate[$.LANG_SITE].filetype),
            filesize: $.validator.format(LANG_PLUGIN.validate[$.LANG_SITE].filesize),
            filesizeEach: $.validator.format(LANG_PLUGIN.validate[$.LANG_SITE].filesizeEach),
            max: $.validator.format(LANG_PLUGIN.validate[$.LANG_SITE].max),
            min: $.validator.format(LANG_PLUGIN.validate[$.LANG_SITE].min),
            // add
            word: LANG_PLUGIN.validate[$.LANG_SITE].word,
            login: LANG_PLUGIN.validate[$.LANG_SITE].login,
            phoneUA: LANG_PLUGIN.validate[$.LANG_SITE].phoneUA,
            extension: LANG_PLUGIN.validate[$.LANG_SITE].extension
        },

        autoCreateRanges: false,

        prototype: {

            init: function() {
                this.labelContainer = $(this.settings.errorLabelContainer);
                this.errorContext = this.labelContainer.length && this.labelContainer || $(this.currentForm);
                this.containers = $(this.settings.errorContainer).add(this.settings.errorLabelContainer);
                this.submitted = {};
                this.valueCache = {};
                this.pendingRequest = 0;
                this.pending = {};
                this.invalid = {};
                this.reset();

                var groups = (this.groups = {}),
                    rules;
                $.each(this.settings.groups, function(key, value) {
                    if (typeof value === "string") {
                        value = value.split(/\s/);
                    }
                    $.each(value, function(index, name) {
                        groups[name] = key;
                    });
                });
                rules = this.settings.rules;
                $.each(rules, function(key, value) {
                    rules[key] = $.validator.normalizeRule(value);
                });

                function delegate(event) {
                    var validator, form, eventType;
                    form = this[0].form;

                    if (!form) {
                        form = $(this).closest("div[data-form='true']").get(0);
                    }
                    validator = $.data(form, "validator"),
                        eventType = "on" + event.type.replace(/^validate/, ""),
                        this.settings = validator.settings;
                    if (this.settings[eventType] && !this.is(this.settings.ignore)) {
                        this.settings[eventType].call(validator, this[0], event);
                    }
                }
                $(this.currentForm)
                    .validateDelegate(":text, [type='password'], [type='file'], select, textarea, " +
                        "[type='number'], [type='search'] ,[type='tel'], [type='url'], " +
                        "[type='email'], [type='datetime'], [type='date'], [type='month'], " +
                        "[type='week'], [type='time'], [type='datetime-local'], " +
                        "[type='range'], [type='color'], [type='radio'], [type='checkbox']",
                        "focusin focusout keyup", delegate)
                    // Support: Chrome, oldIE
                    // "select" is provided as event.target when clicking a option
                    .validateDelegate("select, option, [type='radio'], [type='checkbox']", "click", delegate);

                if (this.settings.invalidHandler) {
                    $(this.currentForm).bind("invalid-form.validate", this.settings.invalidHandler);
                }

                // Add aria-required to any Static/Data/Class required fields before first validation
                // Screen readers require this attribute to be present before the initial submission http://www.w3.org/TR/WCAG-TECHS/ARIA2.html
                $(this.currentForm).find("[required], [data-rule-required], .required").attr("aria-required", "true");
            },

            // http://jqueryvalidation.org/Validator.form/
            form: function() {
                this.checkForm();
                $.extend(this.submitted, this.errorMap);
                this.invalid = $.extend({}, this.errorMap);
                if (!this.valid()) {
                    $(this.currentForm).triggerHandler("invalid-form", [this]);
                }
                this.showErrors();
                return this.valid();
            },

            checkForm: function() {
                this.prepareForm();
                for (var i = 0, elements = (this.currentElements = this.elements()); elements[i]; i++) {
                    this.check(elements[i]);
                }
                return this.valid();
            },

            // http://jqueryvalidation.org/Validator.element/
            element: function(element) {
                var cleanElement = this.clean(element),
                    checkElement = this.validationTargetFor(cleanElement),
                    result = true;
                if (checkElement === undefined) {
                    delete this.invalid[cleanElement.name];
                } else {
                    this.prepareElement(checkElement);
                    this.currentElements = $(checkElement);

                    result = this.check(checkElement) !== false;
                    if (result) {
                        this.invalid[checkElement.name] = false;
                    } else {
                        this.invalid[checkElement.name] = true;
                    }
                }
                // Add aria-invalid status for screen readers
                $(element).attr("aria-invalid", !result);

                if (!this.numberOfInvalids()) {
                    // Hide error containers on last error
                    this.toHide = this.toHide.add(this.containers);
                }
                this.showErrors();
                return result;
            },

            // http://jqueryvalidation.org/Validator.showErrors/
            showErrors: function(errors) {
                if (errors) {
                    // add items to error list and map
                    $.extend(this.errorMap, errors);
                    this.errorList = [];
                    for (var name in errors) {
                        this.errorList.push({
                            message: errors[name],
                            element: this.findByName(name)[0]
                        });
                    }
                    // remove items from success list
                    this.successList = $.grep(this.successList, function(element) {
                        return !(element.name in errors);
                    });
                }
                if (this.settings.showErrors) {
                    this.settings.showErrors.call(this, this.errorMap, this.errorList);
                } else {
                    this.defaultShowErrors();
                }
            },

            // http://jqueryvalidation.org/Validator.resetForm/
            resetForm: function() {
                if ($.fn.resetForm) {
                    $(this.currentForm).resetForm();
                }
                this.submitted = {};
                this.prepareForm();
                this.hideErrors();
                var i, elements = this.elements()
                    .removeData("previousValue")
                    .removeAttr("aria-invalid");

                if (this.settings.unhighlight) {
                    for (i = 0; elements[i]; i++) {
                        this.settings.unhighlight.call(this, elements[i],
                            this.settings.errorClass, "");
                    }
                } else {
                    elements.removeClass(this.settings.errorClass);
                }
            },

            numberOfInvalids: function() {
                return this.objectLength(this.invalid);
            },

            objectLength: function(obj) {
                /* jshint unused: false */
                var count = 0,
                    i;
                for (i in obj) {

                    if (obj[i]) {
                        count++;
                    }
                }
                return count;
            },

            hideErrors: function() {
                this.hideThese(this.toHide);
            },

            hideThese: function(errors) {
                errors.not(this.containers).text("");
                this.addWrapper(errors).hide();
            },

            valid: function() {
                return this.size() === 0;
            },

            // Check if the given element is valid
            // return
            //          true  If the field is valid
            //         false  If the field is invalid
            //     undefined  If the field is not validated yet.
            //
            // Note that this method assumes that you have
            // already called `validate()` on your form
            isValidElement: function(element) {
                return this.invalid[element.name] === undefined ? undefined : !this.invalid[element.name];
            },

            size: function() {
                return this.errorList.length;
            },

            focusInvalid: function() {
                if (this.settings.focusInvalid) {
                    try {
                        $(this.errorList.length && this.errorList[0].element || [])
                            .filter(":visible")
                            .focus()
                            // manually trigger focusin event; without it, focusin handler isn't called, findLastActive won't have anything to find
                            .trigger("focusin");
                    } catch (e) {
                        // ignore IE throwing errors when focusing hidden elements
                    }
                }
            },

            findLastActive: function() {
                var lastActive = this.lastActive;
                return lastActive && $.grep(this.errorList, function(n) {
                    return n.element.name === lastActive.name;
                }).length === 1 && lastActive;
            },

            elements: function() {
                var validator = this,
                    rulesCache = {};

                // select all valid inputs inside the form (no submit or reset buttons)
                return $(this.currentForm)
                    .find("input, select, textarea")
                    .not(":submit, :reset, :image, :disabled")
                    .not(this.settings.ignore)
                    .filter(function() {
                        if (!this.name && validator.settings.debug && window.console) {
                            console.error("%o has no name assigned", this);
                        }

                        // select only the first element for each name, and only those with rules specified
                        if (this.name in rulesCache || !validator.objectLength($(this).rules())) {
                            return false;
                        }

                        rulesCache[this.name] = true;
                        return true;
                    });
            },

            clean: function(selector) {
                return $(selector)[0];
            },

            errors: function() {
                var errorClass = this.settings.errorClass.split(" ").join(".");
                return $(this.settings.errorElement + "." + errorClass, this.errorContext);
            },

            reset: function() {
                this.successList = [];
                this.errorList = [];
                this.errorMap = {};
                this.toShow = $([]);
                this.toHide = $([]);
                this.currentElements = $([]);
            },

            prepareForm: function() {
                this.reset();
                this.toHide = this.errors().add(this.containers);
            },

            prepareElement: function(element) {
                this.reset();
                this.toHide = this.errorsFor(element);
            },

            elementValue: function(element) {
                var val,
                    $element = $(element),
                    type = element.type;

                if (type === "radio" || type === "checkbox") {
                    return this.findByName(element.name).filter(":checked").val();
                } else if (type === "number" && typeof element.validity !== "undefined") {
                    return element.validity.badInput ? false : $element.val();
                }

                val = $element.val();
                if (typeof val === "string") {
                    return val.replace(/\r/g, "");
                }
                return val;
            },

            check: function(element) {
                element = this.validationTargetFor(this.clean(element));

                var rules = $(element).rules(),
                    rulesCount = $.map(rules, function(n, i) {
                        return i;
                    }).length,
                    dependencyMismatch = false,
                    val = this.elementValue(element),
                    result, method, rule;

                for (method in rules) {
                    rule = {
                        method: method,
                        parameters: rules[method]
                    };
                    try {

                        result = $.validator.methods[method].call(this, val, element, rule.parameters);

                        // if a method indicates that the field is optional and therefore valid,
                        // don't mark it as valid when there are no other rules
                        if (result === "dependency-mismatch" && rulesCount === 1) {
                            dependencyMismatch = true;
                            continue;
                        }
                        dependencyMismatch = false;

                        if (result === "pending") {
                            this.toHide = this.toHide.not(this.errorsFor(element));
                            return;
                        }

                        if (!result) {
                            this.formatAndAdd(element, rule);
                            return false;
                        }
                    } catch (e) {
                        if (this.settings.debug && window.console) {
                            console.log("Exception occurred when checking element " + element.id + ", check the '" + rule.method + "' method.", e);
                        }
                        if (e instanceof TypeError) {
                            e.message += ".  Exception occurred when checking element " + element.id + ", check the '" + rule.method + "' method.";
                        }
                        throw e;
                    }
                }
                if (dependencyMismatch) {
                    return;
                }
                if (this.objectLength(rules)) {
                    this.successList.push(element);
                }
                return true;
            },

            // return the custom message for the given element and validation method
            // specified in the element's HTML5 data attribute
            // return the generic message if present and no method specific message is present
            customDataMessage: function(element, method) {
                return $(element).data("msg" + method.charAt(0).toUpperCase() +
                    method.substring(1).toLowerCase()) || $(element).data("msg");
            },

            // return the custom message for the given element name and validation method
            customMessage: function(name, method) {
                var m = this.settings.messages[name];
                return m && (m.constructor === String ? m : m[method]);
            },

            // return the first defined argument, allowing empty strings
            findDefined: function() {
                for (var i = 0; i < arguments.length; i++) {
                    if (arguments[i] !== undefined) {
                        return arguments[i];
                    }
                }
                return undefined;
            },

            defaultMessage: function(element, method) {
                return this.findDefined(
                    this.customMessage(element.name, method),
                    this.customDataMessage(element, method),
                    // title is never undefined, so handle empty string as undefined
                    !this.settings.ignoreTitle && element.title || undefined,
                    $.validator.messages[method],
                    "<strong>Warning: No message defined for " + element.name + "</strong>"
                );
            },

            formatAndAdd: function(element, rule) {
                var message = this.defaultMessage(element, rule.method),
                    theregex = /\$?\{(\d+)\}/g;
                if (typeof message === "function") {
                    message = message.call(this, rule.parameters, element);
                } else if (theregex.test(message)) {
                    message = $.validator.format(message.replace(theregex, "{$1}"), rule.parameters);
                }
                this.errorList.push({
                    message: message,
                    element: element,
                    method: rule.method
                });

                this.errorMap[element.name] = message;
                this.submitted[element.name] = message;
            },

            addWrapper: function(toToggle) {
                if (this.settings.wrapper) {
                    toToggle = toToggle.add(toToggle.parent(this.settings.wrapper));
                }
                return toToggle;
            },

            defaultShowErrors: function() {
                var i, elements, error;
                for (i = 0; this.errorList[i]; i++) {
                    error = this.errorList[i];
                    if (this.settings.highlight) {
                        this.settings.highlight.call(this, error.element, this.settings.errorClass, this.settings.validClass);
                    }
                    this.showLabel(error.element, error.message);
                }
                if (this.errorList.length) {
                    this.toShow = this.toShow.add(this.containers);
                }
                if (this.settings.success) {
                    for (i = 0; this.successList[i]; i++) {
                        this.showLabel(this.successList[i]);
                    }
                }
                if (this.settings.unhighlight) {
                    for (i = 0, elements = this.validElements(); elements[i]; i++) {
                        this.settings.unhighlight.call(this, elements[i], this.settings.errorClass, this.settings.validClass);
                    }
                }
                this.toHide = this.toHide.not(this.toShow);
                this.hideErrors();
                this.addWrapper(this.toShow).show();
            },

            validElements: function() {
                return this.currentElements.not(this.invalidElements());
            },

            invalidElements: function() {
                return $(this.errorList).map(function() {
                    return this.element;
                });
            },

            showLabel: function(element, message) {
                var place, group, errorID,
                    error = this.errorsFor(element),
                    elementID = this.idOrName(element),
                    describedBy = $(element).attr("aria-describedby");
                if (error.length) {
                    // refresh error/success class
                    error.removeClass(this.settings.validClass).addClass(this.settings.errorClass);
                    // replace message on existing label
                    error.html(message);
                } else {
                    // create error element
                    error = $("<" + this.settings.errorElement + ">")
                        .attr("id", elementID + "-error")
                        .addClass(this.settings.errorClass)
                        .html(message || "");

                    // Maintain reference to the element to be placed into the DOM
                    place = error;
                    if (this.settings.wrapper) {
                        // make sure the element is visible, even in IE
                        // actually showing the wrapped element is handled elsewhere
                        place = error.hide().show().wrap("<" + this.settings.wrapper + "/>").parent();
                    }
                    if (this.labelContainer.length) {
                        this.labelContainer.append(place);
                    } else if (this.settings.errorPlacement) {
                        this.settings.errorPlacement(place, $(element));
                    } else {
                        place.insertAfter(element);
                    }

                    // Link error back to the element
                    if (error.is("label")) {
                        // If the error is a label, then associate using 'for'
                        error.attr("for", elementID);
                    } else if (error.parents("label[for='" + elementID + "']").length === 0) {
                        // If the element is not a child of an associated label, then it's necessary
                        // to explicitly apply aria-describedby

                        errorID = error.attr("id").replace(/(:|\.|\[|\]|\$)/g, "\\$1");
                        // Respect existing non-error aria-describedby
                        if (!describedBy) {
                            describedBy = errorID;
                        } else if (!describedBy.match(new RegExp("\\b" + errorID + "\\b"))) {
                            // Add to end of list if not already present
                            describedBy += " " + errorID;
                        }
                        $(element).attr("aria-describedby", describedBy);

                        // If this element is grouped, then assign to all elements in the same group
                        group = this.groups[element.name];
                        if (group) {
                            $.each(this.groups, function(name, testgroup) {
                                if (testgroup === group) {
                                    $("[name='" + name + "']", this.currentForm)
                                        .attr("aria-describedby", error.attr("id"));
                                }
                            });
                        }
                    }
                }
                if (!message && this.settings.success) {
                    error.text("");
                    if (typeof this.settings.success === "string") {
                        error.addClass(this.settings.success);
                    } else {
                        this.settings.success(error, element);
                    }
                }
                this.toShow = this.toShow.add(error);
            },

            errorsFor: function(element) {
                var name = this.idOrName(element),
                    describer = $(element).attr("aria-describedby"),
                    selector = "label[for='" + name + "'], label[for='" + name + "'] *";

                // aria-describedby should directly reference the error element
                if (describer) {
                    selector = selector + ", #" + describer.replace(/\s+/g, ", #");
                }
                return this
                    .errors()
                    .filter(selector);
            },

            idOrName: function(element) {
                return this.groups[element.name] || (this.checkable(element) ? element.name : element.id || element.name);
            },

            validationTargetFor: function(element) {

                // If radio/checkbox, validate first element in group instead
                if (this.checkable(element)) {
                    element = this.findByName(element.name);
                }

                // Always apply ignore filter
                return $(element).not(this.settings.ignore)[0];

            },

            checkable: function(element) {
                return (/radio|checkbox/i).test(element.type);
            },

            findByName: function(name) {
                return $(this.currentForm).find("[name='" + name + "']");
            },

            getLength: function(value, element) {
                switch (element.nodeName.toLowerCase()) {
                    case "select":
                        return $("option:selected", element).length;
                    case "input":
                        if (this.checkable(element)) {
                            return this.findByName(element.name).filter(":checked").length;
                        }
                }
                return value.length;
            },

            depend: function(param, element) {
                return this.dependTypes[typeof param] ? this.dependTypes[typeof param](param, element) : true;
            },

            dependTypes: {
                "boolean": function(param) {
                    return param;
                },
                "string": function(param, element) {
                    return !!$(param, element.form).length;
                },
                "function": function(param, element) {
                    return param(element);
                }
            },

            optional: function(element) {
                var val = this.elementValue(element);
                return !$.validator.methods.required.call(this, val, element) && "dependency-mismatch";
            },

            startRequest: function(element) {
                if (!this.pending[element.name]) {
                    this.pendingRequest++;
                    this.pending[element.name] = true;
                }
            },

            stopRequest: function(element, valid) {
                this.pendingRequest--;
                // sometimes synchronization fails, make sure pendingRequest is never < 0
                if (this.pendingRequest < 0) {
                    this.pendingRequest = 0;
                }
                delete this.pending[element.name];
                if (valid && this.pendingRequest === 0 && this.formSubmitted && this.form()) {
                    $(this.currentForm).submit();
                    this.formSubmitted = false;
                } else if (!valid && this.pendingRequest === 0 && this.formSubmitted) {
                    $(this.currentForm).triggerHandler("invalid-form", [this]);
                    this.formSubmitted = false;
                }
            },

            previousValue: function(element) {
                return $.data(element, "previousValue") || $.data(element, "previousValue", {
                    old: null,
                    valid: true,
                    message: this.defaultMessage(element, "remote")
                });
            },

            // cleans up all forms and elements, removes validator-specific events
            destroy: function() {
                this.resetForm();

                $(this.currentForm)
                    .off(".validate")
                    .removeData("validator");
            }

        },

        classRuleSettings: {
            required: {
                required: true
            },
            email: {
                email: true
            },
            url: {
                url: true
            },
            date: {
                date: true
            },
            dateISO: {
                dateISO: true
            },
            number: {
                number: true
            },
            digits: {
                digits: true
            },
            creditcard: {
                creditcard: true
            }
        },

        addClassRules: function(className, rules) {
            if (className.constructor === String) {
                this.classRuleSettings[className] = rules;
            } else {
                $.extend(this.classRuleSettings, className);
            }
        },

        classRules: function(element) {
            var rules = {},
                classes = $(element).attr("class");

            if (classes) {
                $.each(classes.split(" "), function() {
                    if (this in $.validator.classRuleSettings) {
                        $.extend(rules, $.validator.classRuleSettings[this]);
                    }
                });
            }
            return rules;
        },

        normalizeAttributeRule: function(rules, type, method, value) {

            // convert the value to a number for number inputs, and for text for backwards compability
            // allows type="date" and others to be compared as strings
            if (/min|max/.test(method) && (type === null || /number|range|text/.test(type))) {
                value = Number(value);

                // Support Opera Mini, which returns NaN for undefined minlength
                if (isNaN(value)) {
                    value = undefined;
                }
            }

            if (value || value === 0) {
                rules[method] = value;
            } else if (type === method && type !== "range") {

                // exception: the jquery validate 'range' method
                // does not test for the html5 'range' type
                rules[method] = true;
            }
        },

        attributeRules: function(element) {
            var rules = {},
                $element = $(element),
                type = element.getAttribute("type"),
                method, value;

            for (method in $.validator.methods) {

                // support for <input required> in both html5 and older browsers
                if (method === "required") {
                    value = element.getAttribute(method);
                    // Some browsers return an empty string for the required attribute
                    // and non-HTML5 browsers might have required="" markup
                    if (value === "") {
                        value = true;
                    }
                    // force non-HTML5 browsers to return bool
                    value = !!value;
                } else {
                    value = $element.attr(method);
                }

                this.normalizeAttributeRule(rules, type, method, value);
            }

            // maxlength may be returned as -1, 2147483647 ( IE ) and 524288 ( safari ) for text inputs
            if (rules.maxlength && /-1|2147483647|524288/.test(rules.maxlength)) {
                delete rules.maxlength;
            }

            return rules;
        },

        dataRules: function(element) {
            var rules = {},
                $element = $(element),
                type = element.getAttribute("type"),
                method, value;

            for (method in $.validator.methods) {
                value = $element.data("rule" + method.charAt(0).toUpperCase() + method.substring(1).toLowerCase());
                this.normalizeAttributeRule(rules, type, method, value);
            }
            return rules;
        },

        staticRules: function(element) {
            if (element.form) {
                validator = $.data(element.form, "validator");
            } else {
                validator = $.data($(element).closest("div[data-form='true']").get(0), "validator");
            }

            var rules = {},
                validator = validator;

            if (validator.settings.rules) {
                rules = $.validator.normalizeRule(validator.settings.rules[element.name]) || {};
            }
            return rules;
        },

        normalizeRules: function(rules, element) {
            // handle dependency check
            $.each(rules, function(prop, val) {
                // ignore rule when param is explicitly false, eg. required:false
                if (val === false) {
                    delete rules[prop];
                    return;
                }
                if (val.param || val.depends) {
                    var keepRule = true;
                    switch (typeof val.depends) {
                        case "string":
                            keepRule = !!$(val.depends, element.form).length;
                            break;
                        case "function":
                            keepRule = val.depends.call(element, element);
                            break;
                    }
                    if (keepRule) {
                        rules[prop] = val.param !== undefined ? val.param : true;
                    } else {
                        delete rules[prop];
                    }
                }
            });

            // evaluate parameters
            $.each(rules, function(rule, parameter) {
                rules[rule] = $.isFunction(parameter) ? parameter(element) : parameter;
            });

            // clean number parameters
            $.each(["minlength", "maxlength"], function() {
                if (rules[this]) {
                    rules[this] = Number(rules[this]);
                }
            });
            $.each(["rangelength", "range"], function() {
                var parts;
                if (rules[this]) {
                    if ($.isArray(rules[this])) {
                        rules[this] = [Number(rules[this][0]), Number(rules[this][1])];
                    } else if (typeof rules[this] === "string") {
                        parts = rules[this].replace(/[\[\]]/g, "").split(/[\s,]+/);
                        rules[this] = [Number(parts[0]), Number(parts[1])];
                    }
                }
            });

            if ($.validator.autoCreateRanges) {
                // auto-create ranges
                if (rules.min != null && rules.max != null) {
                    rules.range = [rules.min, rules.max];
                    delete rules.min;
                    delete rules.max;
                }
                if (rules.minlength != null && rules.maxlength != null) {
                    rules.rangelength = [rules.minlength, rules.maxlength];
                    delete rules.minlength;
                    delete rules.maxlength;
                }
            }

            return rules;
        },

        // Converts a simple string to a {string: true} rule, e.g., "required" to {required:true}
        normalizeRule: function(data) {
            if (typeof data === "string") {
                var transformed = {};
                $.each(data.split(/\s/), function() {
                    transformed[this] = true;
                });
                data = transformed;
            }
            return data;
        },

        // http://jqueryvalidation.org/jQuery.validator.addMethod/
        addMethod: function(name, method, message) {
            $.validator.methods[name] = method;
            $.validator.messages[name] = message !== undefined ? message : $.validator.messages[name];
            if (method.length < 3) {
                $.validator.addClassRules(name, $.validator.normalizeRule(name));
            }
        },

        methods: {

            // http://jqueryvalidation.org/required-method/
            required: function(value, element, param) {
                // check if dependency is met
                if (!this.depend(param, element)) {
                    return "dependency-mismatch";
                }
                if (element.nodeName.toLowerCase() === "select") {
                    // could be an array for select-multiple or a string, both are fine this way
                    var val = $(element).val();
                    return val && val.length > 0;
                }
                if (this.checkable(element)) {
                    return this.getLength(value, element) > 0;
                }
                return value.length > 0;
            },

            phone: function(value, element, param) {
                return this.optional(element) || /^([+]?[0-9]{1,2})?([0-9]{3})([\d]{7})$/.test(value);
            },

            phoneUA: function(value, element, param) {
                return this.optional(element) || /^([+]38)?([0-9]{3})([\d]{7})$/.test(value);
            },

            validTrue: function(value, element, param) {
                if ($(element).data('valid') === true) {
                    return true;
                } else {
                    return false;
                }
            },

            filesize: function(value, element, param) {
                var kb = 0;
                for (var i = 0; i < element.files.length; i++) {
                    kb += element.files[i].size;
                }
                return this.optional(element) || (kb / 1024 <= param);
            },

            filesizeEach: function(value, element, param) {
                var flag = true;
                for (var i = 0; i < element.files.length; i++) {
                    if (element.files[i].size / 1024 > param) {
                        flag = false;
                    }
                }
                return this.optional(element) || (flag);
            },

            filetype: function(value, element, param) {
                param = typeof param === "string" ? param.replace(/,/g, "|") : "png|jpe?g|doc|pdf|gif|zip|rar|tar|html|swf|txt|xls|docx|xlsx|odt";
                return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"));
            },

            word: function(value, element) {
                return this.optional(element) || /^[a-zA-Zа-яА-ЯіІїЇєёЁЄґҐ\'\`\- ]*$/.test(value);
            },

            login: function(value, element) {
                return this.optional(element) || /^[a-zA-Zа-яА-ЯіІїЇєЄёЁґҐ][0-9a-zA-Zа-яА-ЯіІїЇєЄґҐ\-\._| ]+$/.test(value);
            },

            // http://jqueryvalidation.org/email-method/
            email: function(value, element) {
                return this.optional(element) || /^([a-zA-Z0-9_\.\-]{2,})+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
            },

            // http://jqueryvalidation.org/url-method/
            url: function(value, element) {
                // contributed by Scott Gonzalez: http://projects.scottsplayground.com/iri/
                return this.optional(element) || /^((https?|s?ftp):\/\/)?(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(value);
            },

            // http://jqueryvalidation.org/date-method/
            date: function(value, element) {
                return this.optional(element) || !/Invalid|NaN/.test(new Date(value).toString());
            },

            // http://jqueryvalidation.org/dateISO-method/
            dateISO: function(value, element) {
                return this.optional(element) || /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/.test(value);
            },

            // http://jqueryvalidation.org/number-method/
            number: function(value, element) {
                return this.optional(element) || /^(?:-?\d+|-?\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test(value);
            },

            // http://jqueryvalidation.org/digits-method/
            digits: function(value, element) {
                return this.optional(element) || /^\d+$/.test(value);
            },

            or: function(value, element, param) {
                var $module = $(element).parents('.wForm');
                return $module.find('.' + param + ':filled').length;
            },

            // http://jqueryvalidation.org/creditcard-method/
            // based on http://en.wikipedia.org/wiki/Luhn_algorithm
            creditcard: function(value, element) {
                if (this.optional(element)) {
                    return "dependency-mismatch";
                }
                // accept only spaces, digits and dashes
                if (/[^0-9 \-]+/.test(value)) {
                    return false;
                }
                var nCheck = 0,
                    nDigit = 0,
                    bEven = false,
                    n, cDigit;

                value = value.replace(/\D/g, "");

                // Basing min and max length on
                // http://developer.ean.com/general_info/Valid_Credit_Card_Types
                if (value.length < 13 || value.length > 19) {
                    return false;
                }

                for (n = value.length - 1; n >= 0; n--) {
                    cDigit = value.charAt(n);
                    nDigit = parseInt(cDigit, 10);
                    if (bEven) {
                        if ((nDigit *= 2) > 9) {
                            nDigit -= 9;
                        }
                    }
                    nCheck += nDigit;
                    bEven = !bEven;
                }

                return (nCheck % 10) === 0;
            },

            // http://jqueryvalidation.org/minlength-method/
            minlength: function(value, element, param) {
                var length = $.isArray(value) ? value.length : this.getLength(value, element);
                return this.optional(element) || length >= param;
            },

            // http://jqueryvalidation.org/maxlength-method/
            maxlength: function(value, element, param) {
                var length = $.isArray(value) ? value.length : this.getLength(value, element);
                return this.optional(element) || length <= param;
            },

            // http://jqueryvalidation.org/rangelength-method/
            rangelength: function(value, element, param) {
                var length = $.isArray(value) ? value.length : this.getLength(value, element);
                return this.optional(element) || (length >= param[0] && length <= param[1]);
            },

            // http://jqueryvalidation.org/min-method/
            min: function(value, element, param) {
                return this.optional(element) || value >= param;
            },

            // http://jqueryvalidation.org/max-method/
            max: function(value, element, param) {
                return this.optional(element) || value <= param;
            },

            // http://jqueryvalidation.org/range-method/
            range: function(value, element, param) {
                return this.optional(element) || (value >= param[0] && value <= param[1]);
            },

            // http://jqueryvalidation.org/equalTo-method/
            equalTo: function(value, element, param) {
                // bind to the blur event of the target in order to revalidate whenever the target field is updated
                // TODO find a way to bind the event just once, avoiding the unbind-rebind overhead
                var target = $(param);
                if (this.settings.onfocusout) {
                    target.off(".validate-equalTo").on("blur.validate-equalTo", function() {
                        $(element).valid();
                    });
                }
                return value === target.val();
            },

            // http://jqueryvalidation.org/remote-method/
            remote: function(value, element, param) {
                if (this.optional(element)) {
                    return "dependency-mismatch";
                }

                var previous = this.previousValue(element),
                    validator, data, optionDataString;

                if (!this.settings.messages[element.name]) {
                    this.settings.messages[element.name] = {};
                }
                previous.originalMessage = this.settings.messages[element.name].remote;
                this.settings.messages[element.name].remote = previous.message;

                param = typeof param === "string" && {
                    url: param
                } || param;
                optionDataString = $.param($.extend({
                    data: value
                }, param.data));
                if (previous.old === optionDataString) {
                    return previous.valid;
                }

                previous.old = optionDataString;
                validator = this;
                this.startRequest(element);
                data = {};
                data[element.name] = value;
                $.ajax($.extend(true, {
                    mode: "abort",
                    port: "validate" + element.name,
                    dataType: "json",
                    data: data,
                    context: validator.currentForm,
                    success: function(response) {
                        var valid = response === true || response === "true",
                            errors, message, submitted;

                        validator.settings.messages[element.name].remote = previous.originalMessage;
                        if (valid) {
                            submitted = validator.formSubmitted;
                            validator.prepareElement(element);
                            validator.formSubmitted = submitted;
                            validator.successList.push(element);
                            delete validator.invalid[element.name];
                            validator.showErrors();
                        } else {
                            errors = {};
                            message = response || validator.defaultMessage(element, "remote");
                            errors[element.name] = previous.message = $.isFunction(message) ? message(value) : message;
                            validator.invalid[element.name] = true;
                            validator.showErrors(errors);
                        }
                        previous.valid = valid;
                        validator.stopRequest(element, valid);
                    }
                }, param));
                return "pending";
            }

        }

    });

    $.format = function deprecated() {
        throw "$.format has been deprecated. Please use $.validator.format instead.";
    };
/*-------------------------------  Magnific  ------------------------------------*/
    /*! Magnific Popup - v0.9.9 - 2014-09-06
     * http://dimsemenov.com/plugins/magnific-popup/
     * Copyright (c) 2014 Dmitry Semenov; */
    ;
    (function($) {

        /*>>core*/
        /**
         *
         * Magnific Popup Core JS file
         *
         */


        /**
         * Private static constants
         */
        var CLOSE_EVENT = 'Close',
            BEFORE_CLOSE_EVENT = 'BeforeClose',
            AFTER_CLOSE_EVENT = 'AfterClose',
            BEFORE_APPEND_EVENT = 'BeforeAppend',
            MARKUP_PARSE_EVENT = 'MarkupParse',
            OPEN_EVENT = 'Open',
            CHANGE_EVENT = 'Change',
            NS = 'mfp',
            EVENT_NS = '.' + NS,
            READY_CLASS = 'mfp-ready',
            REMOVING_CLASS = 'mfp-removing',
            PREVENT_CLOSE_CLASS = 'mfp-prevent-close';


        /**
         * Private vars
         */
        var mfp, // As we have only one instance of MagnificPopup object, we define it locally to not to use 'this'
            MagnificPopup = function() {},
            _isJQ = !! (window.jQuery),
            _prevStatus,
            _window = $(window),
            _body,
            _document,
            _prevContentType,
            _wrapClasses,
            _currPopupType;


        /**
         * Private functions
         */
        var _mfpOn = function(name, f) {
            mfp.ev.on(NS + name + EVENT_NS, f);
        },
            _getEl = function(className, appendTo, html, raw) {
                var el = document.createElement('div');
                el.className = 'mfp-' + className;
                if (html) {
                    el.innerHTML = html;
                }
                if (!raw) {
                    el = $(el);
                    if (appendTo) {
                        el.appendTo(appendTo);
                    }
                } else if (appendTo) {
                    appendTo.appendChild(el);
                }
                return el;
            },
            _mfpTrigger = function(e, data) {
                mfp.ev.triggerHandler(NS + e, data);

                if (mfp.st.callbacks) {
                    // converts "mfpEventName" to "eventName" callback and triggers it if it's present
                    e = e.charAt(0).toLowerCase() + e.slice(1);
                    if (mfp.st.callbacks[e]) {
                        mfp.st.callbacks[e].apply(mfp, $.isArray(data) ? data : [data]);
                    }
                }
            },
            _getCloseBtn = function(type) {
                if (type !== _currPopupType || !mfp.currTemplate.closeBtn) {
                    mfp.currTemplate.closeBtn = $(mfp.st.closeMarkup.replace('%title%', mfp.st.tClose));
                    _currPopupType = type;
                }
                return mfp.currTemplate.closeBtn;
            },
            // Initialize Magnific Popup only when called at least once
            _checkInstance = function() {
                if (!$.magnificPopup.instance) {
                    mfp = new MagnificPopup();
                    mfp.init();
                    $.magnificPopup.instance = mfp;
                }
            },
            // CSS transition detection, http://stackoverflow.com/questions/7264899/detect-css-transitions-using-javascript-and-without-modernizr
            supportsTransitions = function() {
                var s = document.createElement('p').style, // 's' for style. better to create an element if body yet to exist
                    v = ['ms', 'O', 'Moz', 'Webkit']; // 'v' for vendor

                if (s['transition'] !== undefined) {
                    return true;
                }

                while (v.length) {
                    if (v.pop() + 'Transition' in s) {
                        return true;
                    }
                }

                return false;
            };



        /**
         * Public functions
         */
        MagnificPopup.prototype = {

            constructor: MagnificPopup,

            /**
             * Initializes Magnific Popup plugin.
             * This function is triggered only once when $.fn.magnificPopup or $.magnificPopup is executed
             */
            init: function() {
                var appVersion = navigator.appVersion;
                mfp.isIE7 = appVersion.indexOf("MSIE 7.") !== -1;
                mfp.isIE8 = appVersion.indexOf("MSIE 8.") !== -1;
                mfp.isLowIE = mfp.isIE7 || mfp.isIE8;
                mfp.isAndroid = (/android/gi).test(appVersion);
                mfp.isIOS = (/iphone|ipad|ipod/gi).test(appVersion);
                mfp.supportsTransition = supportsTransitions();

                // We disable fixed positioned lightbox on devices that don't handle it nicely.
                // If you know a better way of detecting this - let me know.
                mfp.probablyMobile = (mfp.isAndroid || mfp.isIOS || /(Opera Mini)|Kindle|webOS|BlackBerry|(Opera Mobi)|(Windows Phone)|IEMobile/i.test(navigator.userAgent));
                _document = $(document);

                mfp.popupsCache = {};
            },

            /**
             * Opens popup
             * @param  data [description]
             */
            open: function(data) {

                if (!_body) {
                    _body = $(document.body);
                }

                var i;

                if (data.isObj === false) {
                    // convert jQuery collection to array to avoid conflicts later
                    mfp.items = data.items.toArray();

                    mfp.index = 0;
                    var items = data.items,
                        item;
                    for (i = 0; i < items.length; i++) {
                        item = items[i];
                        if (item.parsed) {
                            item = item.el[0];
                        }
                        if (item === data.el[0]) {
                            mfp.index = i;
                            break;
                        }
                    }
                } else {
                    mfp.items = $.isArray(data.items) ? data.items : [data.items];
                    mfp.index = data.index || 0;
                }

                // if popup is already opened - we just update the content
                if (mfp.isOpen) {
                    mfp.updateItemHTML();
                    return;
                }

                mfp.types = [];
                _wrapClasses = '';
                if (data.mainEl && data.mainEl.length) {
                    mfp.ev = data.mainEl.eq(0);
                } else {
                    mfp.ev = _document;
                }

                if (data.key) {
                    if (!mfp.popupsCache[data.key]) {
                        mfp.popupsCache[data.key] = {};
                    }
                    mfp.currTemplate = mfp.popupsCache[data.key];
                } else {
                    mfp.currTemplate = {};
                }



                mfp.st = $.extend(true, {}, $.magnificPopup.defaults, data);
                mfp.fixedContentPos = mfp.st.fixedContentPos === 'auto' ? !mfp.probablyMobile : mfp.st.fixedContentPos;

                if (mfp.st.modal) {
                    mfp.st.closeOnContentClick = false;
                    mfp.st.closeOnBgClick = false;
                    mfp.st.showCloseBtn = false;
                    mfp.st.enableEscapeKey = false;
                }


                // Building markup
                // main containers are created only once
                if (!mfp.bgOverlay) {

                    // Dark overlay
                    mfp.bgOverlay = _getEl('bg').on('click' + EVENT_NS, function() {
                        mfp.close();
                    });

                    mfp.wrap = _getEl('wrap').attr('tabindex', -1).on('click' + EVENT_NS, function(e) {
                        if (mfp._checkIfClose(e.target)) {
                            mfp.close();
                        }
                    });

                    mfp.container = _getEl('container', mfp.wrap);
                }

                mfp.contentContainer = _getEl('content');
                if (mfp.st.preloader) {
                    mfp.preloader = _getEl('preloader', mfp.container, mfp.st.tLoading);
                }


                // Initializing modules
                var modules = $.magnificPopup.modules;
                for (i = 0; i < modules.length; i++) {
                    var n = modules[i];
                    n = n.charAt(0).toUpperCase() + n.slice(1);
                    mfp['init' + n].call(mfp);
                }
                _mfpTrigger('BeforeOpen');


                if (mfp.st.showCloseBtn) {
                    // Close button
                    if (!mfp.st.closeBtnInside) {
                        mfp.wrap.append(_getCloseBtn());
                    } else {
                        _mfpOn(MARKUP_PARSE_EVENT, function(e, template, values, item) {
                            values.close_replaceWith = _getCloseBtn(item.type);
                        });
                        _wrapClasses += ' mfp-close-btn-in';
                    }
                }

                if (mfp.st.alignTop) {
                    _wrapClasses += ' mfp-align-top';
                }



                if (mfp.fixedContentPos) {
                    mfp.wrap.css({
                        overflow: mfp.st.overflowY,
                        overflowX: 'hidden',
                        overflowY: mfp.st.overflowY
                    });
                } else {
                    mfp.wrap.css({
                        top: _window.scrollTop(),
                        position: 'absolute'
                    });
                }
                if (mfp.st.fixedBgPos === false || (mfp.st.fixedBgPos === 'auto' && !mfp.fixedContentPos)) {
                    mfp.bgOverlay.css({
                        height: _document.height(),
                        position: 'absolute'
                    });
                }



                if (mfp.st.enableEscapeKey) {
                    // Close on ESC key
                    _document.on('keyup' + EVENT_NS, function(e) {
                        if (e.keyCode === 27) {
                            mfp.close();
                        }
                    });
                }

                _window.on('resize' + EVENT_NS, function() {
                    mfp.updateSize();
                });


                if (!mfp.st.closeOnContentClick) {
                    _wrapClasses += ' mfp-auto-cursor';
                }

                if (_wrapClasses)
                    mfp.wrap.addClass(_wrapClasses);


                // this triggers recalculation of layout, so we get it once to not to trigger twice
                var windowHeight = mfp.wH = _window.height();


                var windowStyles = {};

                if (mfp.fixedContentPos) {
                    if (mfp._hasScrollBar(windowHeight)) {
                        var s = mfp._getScrollbarSize();
                        if (s) {
                            windowStyles.marginRight = s;
                        }
                    }
                }

                if (mfp.fixedContentPos) {
                    if (!mfp.isIE7) {
                        windowStyles.overflow = 'hidden';
                    } else {
                        // ie7 double-scroll bug
                        $('body, html').css('overflow', 'hidden');
                    }
                }



                var classesToadd = mfp.st.mainClass;
                if (mfp.isIE7) {
                    classesToadd += ' mfp-ie7';
                }
                if (classesToadd) {
                    mfp._addClassToMFP(classesToadd);
                }

                // add content
                mfp.updateItemHTML();

                _mfpTrigger('BuildControls');

                // remove scrollbar, add margin e.t.c
                $('html').css(windowStyles);

                // add everything to DOM
                mfp.bgOverlay.add(mfp.wrap).prependTo(mfp.st.prependTo || _body);

                // Save last focused element
                mfp._lastFocusedEl = document.activeElement;

                // Wait for next cycle to allow CSS transition
                setTimeout(function() {

                    if (mfp.content) {
                        mfp._addClassToMFP(READY_CLASS);
                        mfp._setFocus();
                    } else {
                        // if content is not defined (not loaded e.t.c) we add class only for BG
                        mfp.bgOverlay.addClass(READY_CLASS);
                    }

                    // Trap the focus in popup
                    _document.on('focusin' + EVENT_NS, mfp._onFocusIn);

                }, 16);

                mfp.isOpen = true;
                mfp.updateSize(windowHeight);
                _mfpTrigger(OPEN_EVENT);

                return data;
            },

            /**
             * Closes the popup
             */
            close: function() {
                if (!mfp.isOpen) return;
                _mfpTrigger(BEFORE_CLOSE_EVENT);

                mfp.isOpen = false;
                // for CSS3 animation
                if (mfp.st.removalDelay && !mfp.isLowIE && mfp.supportsTransition) {
                    mfp._addClassToMFP(REMOVING_CLASS);
                    setTimeout(function() {
                        mfp._close();
                    }, mfp.st.removalDelay);
                } else {
                    mfp._close();
                }
            },

            /**
             * Helper for close() function
             */
            _close: function() {
                _mfpTrigger(CLOSE_EVENT);

                var classesToRemove = REMOVING_CLASS + ' ' + READY_CLASS + ' ';

                mfp.bgOverlay.detach();
                mfp.wrap.detach();
                mfp.container.empty();

                if (mfp.st.mainClass) {
                    classesToRemove += mfp.st.mainClass + ' ';
                }

                mfp._removeClassFromMFP(classesToRemove);

                if (mfp.fixedContentPos) {
                    var windowStyles = {
                        marginRight: ''
                    };
                    if (mfp.isIE7) {
                        $('body, html').css('overflow', '');
                    } else {
                        windowStyles.overflow = '';
                    }
                    $('html').css(windowStyles);
                }

                _document.off('keyup' + EVENT_NS + ' focusin' + EVENT_NS);
                mfp.ev.off(EVENT_NS);

                // clean up DOM elements that aren't removed
                mfp.wrap.attr('class', 'mfp-wrap').removeAttr('style');
                mfp.bgOverlay.attr('class', 'mfp-bg');
                mfp.container.attr('class', 'mfp-container');

                // remove close button from target element
                if (mfp.st.showCloseBtn &&
                    (!mfp.st.closeBtnInside || mfp.currTemplate[mfp.currItem.type] === true)) {
                    if (mfp.currTemplate.closeBtn)
                        mfp.currTemplate.closeBtn.detach();
                }


                if (mfp._lastFocusedEl) {
                    $(mfp._lastFocusedEl).focus(); // put tab focus back
                }
                mfp.currItem = null;
                mfp.content = null;
                mfp.currTemplate = null;
                mfp.prevHeight = 0;

                _mfpTrigger(AFTER_CLOSE_EVENT);
            },

            updateSize: function(winHeight) {

                if (mfp.isIOS) {
                    // fixes iOS nav bars https://github.com/dimsemenov/Magnific-Popup/issues/2
                    var zoomLevel = document.documentElement.clientWidth / window.innerWidth;
                    var height = window.innerHeight * zoomLevel;
                    mfp.wrap.css('height', height);
                    mfp.wH = height;
                } else {
                    mfp.wH = winHeight || _window.height();
                }
                // Fixes #84: popup incorrectly positioned with position:relative on body
                if (!mfp.fixedContentPos) {
                    mfp.wrap.css('height', mfp.wH);
                }

                _mfpTrigger('Resize');

            },

            /**
             * Set content of popup based on current index
             */
            updateItemHTML: function() {
                var item = mfp.items[mfp.index];

                // Detach and perform modifications
                mfp.contentContainer.detach();

                if (mfp.content)
                    mfp.content.detach();

                if (!item.parsed) {
                    item = mfp.parseEl(mfp.index);
                }

                var type = item.type;

                _mfpTrigger('BeforeChange', [mfp.currItem ? mfp.currItem.type : '', type]);
                // BeforeChange event works like so:
                // _mfpOn('BeforeChange', function(e, prevType, newType) { });

                mfp.currItem = item;





                if (!mfp.currTemplate[type]) {
                    var markup = mfp.st[type] ? mfp.st[type].markup : false;

                    // allows to modify markup
                    _mfpTrigger('FirstMarkupParse', markup);

                    if (markup) {
                        mfp.currTemplate[type] = $(markup);
                    } else {
                        // if there is no markup found we just define that template is parsed
                        mfp.currTemplate[type] = true;
                    }
                }

                if (_prevContentType && _prevContentType !== item.type) {
                    mfp.container.removeClass('mfp-' + _prevContentType + '-holder');
                }

                var newContent = mfp['get' + type.charAt(0).toUpperCase() + type.slice(1)](item, mfp.currTemplate[type]);
                mfp.appendContent(newContent, type);

                item.preloaded = true;

                _mfpTrigger(CHANGE_EVENT, item);
                _prevContentType = item.type;

                // Append container back after its content changed
                mfp.container.prepend(mfp.contentContainer);

                _mfpTrigger('AfterChange');
            },


            /**
             * Set HTML content of popup
             */
            appendContent: function(newContent, type) {
                mfp.content = newContent;

                if (newContent) {
                    if (mfp.st.showCloseBtn && mfp.st.closeBtnInside &&
                        mfp.currTemplate[type] === true) {
                        // if there is no markup, we just append close button element inside
                        if (!mfp.content.find('.mfp-close').length) {
                            mfp.content.append(_getCloseBtn());
                        }
                    } else {
                        mfp.content = newContent;
                    }
                } else {
                    mfp.content = '';
                }

                _mfpTrigger(BEFORE_APPEND_EVENT);
                mfp.container.addClass('mfp-' + type + '-holder');

                mfp.contentContainer.append(mfp.content);
            },

            /**
             * Creates Magnific Popup data object based on given data
             * @param  {int} index Index of item to parse
             */
            parseEl: function(index) {
                var item = mfp.items[index],
                    type;

                if (item.tagName) {
                    item = {
                        el: $(item)
                    };
                } else {
                    type = item.type;
                    item = {
                        data: item,
                        src: item.src
                    };
                }

                if (item.el) {
                    var types = mfp.types;


                    // check for 'mfp-TYPE' class
                    for (var i = 0; i < types.length; i++) {
                        if (item.el.hasClass('mfp-' + types[i])) {
                            type = types[i];
                            break;
                        }
                    }

                    item.src = item.el.attr('data-mfp-src');
                    if (!item.src) {
                        item.src = item.el.attr('href');
                    }
                }

                item.type = type || mfp.st.type || 'inline';
                item.index = index;
                item.parsed = true;
                mfp.items[index] = item;
                _mfpTrigger('ElementParse', item);

                return mfp.items[index];
            },


            /**
             * Initializes single popup or a group of popups
             */
            addGroup: function(el, options) {
                var eHandler = function(e) {
                    e.mfpEl = this;
                    mfp._openClick(e, el, options);
                };

                if (!options) {
                    options = {};
                }

                var eName = 'click.magnificPopup';
                options.mainEl = el;

                if (options.items) {
                    options.isObj = true;
                    el.off(eName).on(eName, eHandler);
                } else {
                    options.isObj = false;
                    if (options.delegate) {
                        el.off(eName).on(eName, options.delegate, eHandler);
                    } else {
                        options.items = el;
                        el.off(eName).on(eName, eHandler);
                    }
                }
            },
            _openClick: function(e, el, options) {
                var midClick = options.midClick !== undefined ? options.midClick : $.magnificPopup.defaults.midClick;


                if (!midClick && (e.which === 2 || e.ctrlKey || e.metaKey)) {
                    return;
                }

                var disableOn = options.disableOn !== undefined ? options.disableOn : $.magnificPopup.defaults.disableOn;

                if (disableOn) {
                    if ($.isFunction(disableOn)) {
                        if (!disableOn.call(mfp)) {
                            return true;
                        }
                    } else { // else it's number
                        if (_window.width() < disableOn) {
                            return true;
                        }
                    }
                }

                if (e.type) {
                    e.preventDefault();

                    // This will prevent popup from closing if element is inside and popup is already opened
                    if (mfp.isOpen) {
                        e.stopPropagation();
                    }
                }


                options.el = $(e.mfpEl);
                if (options.delegate) {
                    options.items = el.find(options.delegate);
                }
                mfp.open(options);
            },


            /**
             * Updates text on preloader
             */
            updateStatus: function(status, text) {

                if (mfp.preloader) {
                    if (_prevStatus !== status) {
                        mfp.container.removeClass('mfp-s-' + _prevStatus);
                    }

                    if (!text && status === 'loading') {
                        text = mfp.st.tLoading;
                    }

                    var data = {
                        status: status,
                        text: text
                    };
                    // allows to modify status
                    _mfpTrigger('UpdateStatus', data);

                    status = data.status;
                    text = data.text;

                    mfp.preloader.html(text);

                    mfp.preloader.find('a').on('click', function(e) {
                        e.stopImmediatePropagation();
                    });

                    mfp.container.addClass('mfp-s-' + status);
                    _prevStatus = status;
                }
            },


            /*
                "Private" helpers that aren't private at all
             */
            // Check to close popup or not
            // "target" is an element that was clicked
            _checkIfClose: function(target) {

                if ($(target).hasClass(PREVENT_CLOSE_CLASS)) {
                    return;
                }

                var closeOnContent = mfp.st.closeOnContentClick;
                var closeOnBg = mfp.st.closeOnBgClick;

                if (closeOnContent && closeOnBg) {
                    return true;
                } else {

                    // We close the popup if click is on close button or on preloader. Or if there is no content.
                    if (!mfp.content || $(target).hasClass('mfp-close') || (mfp.preloader && target === mfp.preloader[0])) {
                        return true;
                    }

                    // if click is outside the content
                    if ((target !== mfp.content[0] && !$.contains(mfp.content[0], target))) {
                        if (closeOnBg) {
                            // last check, if the clicked element is in DOM, (in case it's removed onclick)
                            if ($.contains(document, target)) {
                                return true;
                            }
                        }
                    } else if (closeOnContent) {
                        return true;
                    }

                }
                return false;
            },
            _addClassToMFP: function(cName) {
                mfp.bgOverlay.addClass(cName);
                mfp.wrap.addClass(cName);
            },
            _removeClassFromMFP: function(cName) {
                this.bgOverlay.removeClass(cName);
                mfp.wrap.removeClass(cName);
            },
            _hasScrollBar: function(winHeight) {
                return ((mfp.isIE7 ? _document.height() : document.body.scrollHeight) > (winHeight || _window.height()));
            },
            _setFocus: function() {
                (mfp.st.focus ? mfp.content.find(mfp.st.focus).eq(0) : mfp.wrap).focus();
            },
            _onFocusIn: function(e) {
                if (e.target !== mfp.wrap[0] && !$.contains(mfp.wrap[0], e.target)) {
                    mfp._setFocus();
                    return false;
                }
            },
            _parseMarkup: function(template, values, item) {
                var arr;
                if (item.data) {
                    values = $.extend(item.data, values);
                }
                _mfpTrigger(MARKUP_PARSE_EVENT, [template, values, item]);

                $.each(values, function(key, value) {
                    if (value === undefined || value === false) {
                        return true;
                    }
                    arr = key.split('_');
                    if (arr.length > 1) {
                        var el = template.find(EVENT_NS + '-' + arr[0]);

                        if (el.length > 0) {
                            var attr = arr[1];
                            if (attr === 'replaceWith') {
                                if (el[0] !== value[0]) {
                                    el.replaceWith(value);
                                }
                            } else if (attr === 'img') {
                                if (el.is('img')) {
                                    el.attr('src', value);
                                } else {
                                    el.replaceWith('<img src="' + value + '" class="' + el.attr('class') + '" />');
                                }
                            } else {
                                el.attr(arr[1], value);
                            }
                        }

                    } else {
                        template.find(EVENT_NS + '-' + key).html(value);
                    }
                });
            },

            _getScrollbarSize: function() {
                // thx David
                if (mfp.scrollbarSize === undefined) {
                    var scrollDiv = document.createElement("div");
                    scrollDiv.style.cssText = 'width: 99px; height: 99px; overflow: scroll; position: absolute; top: -9999px;';
                    document.body.appendChild(scrollDiv);
                    mfp.scrollbarSize = scrollDiv.offsetWidth - scrollDiv.clientWidth;
                    document.body.removeChild(scrollDiv);
                }
                return mfp.scrollbarSize;
            }

        }; /* MagnificPopup core prototype end */




        /**
         * Public static functions
         */
        $.magnificPopup = {
            instance: null,
            proto: MagnificPopup.prototype,
            modules: [],

            open: function(options, index) {
                _checkInstance();

                if (!options) {
                    options = {};
                } else {
                    options = $.extend(true, {}, options);
                }


                options.isObj = true;
                options.index = index || 0;
                return this.instance.open(options);
            },

            close: function() {
                return $.magnificPopup.instance && $.magnificPopup.instance.close();
            },

            registerModule: function(name, module) {
                if (module.options) {
                    $.magnificPopup.defaults[name] = module.options;
                }
                $.extend(this.proto, module.proto);
                this.modules.push(name);
            },

            defaults: {

                // Info about options is in docs:
                // http://dimsemenov.com/plugins/magnific-popup/documentation.html#options

                disableOn: 0,

                key: null,

                midClick: false,

                mainClass: '',

                preloader: true,

                focus: '', // CSS selector of input to focus after popup is opened

                closeOnContentClick: false,

                closeOnBgClick: true,

                closeBtnInside: true,

                showCloseBtn: true,

                enableEscapeKey: true,

                modal: false,

                alignTop: false,

                removalDelay: 0,

                prependTo: null,

                fixedContentPos: 'auto',

                fixedBgPos: 'auto',

                overflowY: 'auto',

                closeMarkup: '<button title="%title%" type="button" class="mfp-close">&times;</button>',

                tClose: LANG_PLUGIN.Magnific[$.LANG_SITE].tClose,

                tLoading: LANG_PLUGIN.Magnific[$.LANG_SITE].tLoading

            }
        };



        $.fn.magnificPopup = function(options) {
            _checkInstance();

            var jqEl = $(this);

            // We call some API method of first param is a string
            if (typeof options === "string") {

                if (options === 'open') {
                    var items,
                        itemOpts = _isJQ ? jqEl.data('magnificPopup') : jqEl[0].magnificPopup,
                        index = parseInt(arguments[1], 10) || 0;

                    if (itemOpts.items) {
                        items = itemOpts.items[index];
                    } else {
                        items = jqEl;
                        if (itemOpts.delegate) {
                            items = items.find(itemOpts.delegate);
                        }
                        items = items.eq(index);
                    }
                    mfp._openClick({
                        mfpEl: items
                    }, jqEl, itemOpts);
                } else {
                    if (mfp.isOpen)
                        mfp[options].apply(mfp, Array.prototype.slice.call(arguments, 1));
                }

            } else {
                // clone options obj
                options = $.extend(true, {}, options);

                /*
                 * As Zepto doesn't support .data() method for objects
                 * and it works only in normal browsers
                 * we assign "options" object directly to the DOM element. FTW!
                 */
                if (_isJQ) {
                    jqEl.data('magnificPopup', options);
                } else {
                    jqEl[0].magnificPopup = options;
                }

                mfp.addGroup(jqEl, options);

            }
            return jqEl;
        };


        //Quick benchmark
        /*
        var start = performance.now(),
            i,
            rounds = 1000;

        for(i = 0; i < rounds; i++) {

        }
        console.log('Test #1:', performance.now() - start);

        start = performance.now();
        for(i = 0; i < rounds; i++) {

        }
        console.log('Test #2:', performance.now() - start);
        */


        /*>>core*/

        /*>>inline*/

        var INLINE_NS = 'inline',
            _hiddenClass,
            _inlinePlaceholder,
            _lastInlineElement,
            _putInlineElementsBack = function() {
                if (_lastInlineElement) {
                    _inlinePlaceholder.after(_lastInlineElement.addClass(_hiddenClass)).detach();
                    _lastInlineElement = null;
                }
            };

        $.magnificPopup.registerModule(INLINE_NS, {
            options: {
                hiddenClass: 'hide', // will be appended with `mfp-` prefix
                markup: '',
                tNotFound: LANG_PLUGIN.Magnific[$.LANG_SITE].tNotFound
            },
            proto: {

                initInline: function() {
                    mfp.types.push(INLINE_NS);

                    _mfpOn(CLOSE_EVENT + '.' + INLINE_NS, function() {
                        _putInlineElementsBack();
                    });
                },

                getInline: function(item, template) {

                    _putInlineElementsBack();

                    if (item.src) {
                        var inlineSt = mfp.st.inline,
                            el = $(item.src);

                        if (el.length) {

                            // If target element has parent - we replace it with placeholder and put it back after popup is closed
                            var parent = el[0].parentNode;
                            if (parent && parent.tagName) {
                                if (!_inlinePlaceholder) {
                                    _hiddenClass = inlineSt.hiddenClass;
                                    _inlinePlaceholder = _getEl(_hiddenClass);
                                    _hiddenClass = 'mfp-' + _hiddenClass;
                                }
                                // replace target inline element with placeholder
                                _lastInlineElement = el.after(_inlinePlaceholder).detach().removeClass(_hiddenClass);
                            }

                            mfp.updateStatus('ready');
                        } else {
                            mfp.updateStatus('error', inlineSt.tNotFound);
                            el = $('<div>');
                        }

                        item.inlineElement = el;
                        return el;
                    }

                    mfp.updateStatus('ready');
                    mfp._parseMarkup(template, {}, item);
                    return template;
                }
            }
        });

        /*>>inline*/

        /*>>ajax*/
        var AJAX_NS = 'ajax',
            _ajaxCur,
            _removeAjaxCursor = function() {
                if (_ajaxCur) {
                    _body.removeClass(_ajaxCur);
                }
            },
            _destroyAjaxRequest = function() {
                _removeAjaxCursor();
                if (mfp.req) {
                    mfp.req.abort();
                }
            };

        $.magnificPopup.registerModule(AJAX_NS, {

            options: {
                settings: null,
                cursor: 'mfp-ajax-cur',
                tError: LANG_PLUGIN.Magnific[$.LANG_SITE].tError
            },

            proto: {
                initAjax: function() {
                    mfp.types.push(AJAX_NS);
                    _ajaxCur = mfp.st.ajax.cursor;

                    _mfpOn(CLOSE_EVENT + '.' + AJAX_NS, _destroyAjaxRequest);
                    _mfpOn('BeforeChange.' + AJAX_NS, _destroyAjaxRequest);
                },
                getAjax: function(item) {

                    if (_ajaxCur)
                        _body.addClass(_ajaxCur);

                    mfp.updateStatus('loading');

                    var opts = $.extend({
                        url: item.src,
                        success: function(data, textStatus, jqXHR) {
                            var temp = {
                                data: data,
                                xhr: jqXHR
                            };

                            _mfpTrigger('ParseAjax', temp);

                            mfp.appendContent($(temp.data), AJAX_NS);

                            item.finished = true;

                            _removeAjaxCursor();

                            mfp._setFocus();

                            setTimeout(function() {
                                mfp.wrap.addClass(READY_CLASS);
                            }, 16);

                            mfp.updateStatus('ready');

                            _mfpTrigger('AjaxContentAdded');
                        },
                        error: function() {
                            _removeAjaxCursor();
                            item.finished = item.loadError = true;
                            mfp.updateStatus('error', mfp.st.ajax.tError.replace('%url%', item.src));
                        }
                    }, mfp.st.ajax.settings);

                    mfp.req = $.ajax(opts);

                    return '';
                }
            }
        });







        /*>>ajax*/

        /*>>image*/
        var _imgInterval,
            _getTitle = function(item) {
                if (item.data && item.data.title !== undefined)
                    return item.data.title;

                var src = mfp.st.image.titleSrc;

                if (src) {
                    if ($.isFunction(src)) {
                        return src.call(mfp, item);
                    } else if (item.el) {
                        return item.el.attr(src) || '';
                    }
                }
                return '';
            };

        $.magnificPopup.registerModule('image', {

            options: {
                markup: '<div class="mfp-figure">' +
                    '<div class="mfp-close"></div>' +
                    '<figure>' +
                    '<div class="mfp-img"></div>' +
                    '<figcaption>' +
                    '<div class="mfp-bottom-bar">' +
                    '<div class="mfp-title"></div>' +
                    '<div class="mfp-counter"></div>' +
                    '</div>' +
                    '</figcaption>' +
                    '</figure>' +
                    '</div>',
                cursor: 'mfp-zoom-out-cur',
                titleSrc: 'title',
                verticalFit: true,

                tError: LANG_PLUGIN.Magnific[$.LANG_SITE].tErrorImage
            },

            proto: {
                initImage: function() {
                    var imgSt = mfp.st.image,
                        ns = '.image';

                    mfp.types.push('image');

                    _mfpOn(OPEN_EVENT + ns, function() {
                        if (mfp.currItem.type === 'image' && imgSt.cursor) {
                            _body.addClass(imgSt.cursor);
                        }
                    });

                    _mfpOn(CLOSE_EVENT + ns, function() {
                        if (imgSt.cursor) {
                            _body.removeClass(imgSt.cursor);
                        }
                        _window.off('resize' + EVENT_NS);
                    });

                    _mfpOn('Resize' + ns, mfp.resizeImage);
                    if (mfp.isLowIE) {
                        _mfpOn('AfterChange', mfp.resizeImage);
                    }
                },
                resizeImage: function() {
                    var item = mfp.currItem;
                    if (!item || !item.img) return;

                    if (mfp.st.image.verticalFit) {
                        var decr = 0;
                        // fix box-sizing in ie7/8
                        if (mfp.isLowIE) {
                            decr = parseInt(item.img.css('padding-top'), 10) + parseInt(item.img.css('padding-bottom'), 10);
                        }
                        item.img.css('max-height', mfp.wH - decr);
                    }
                },
                _onImageHasSize: function(item) {
                    if (item.img) {

                        item.hasSize = true;

                        if (_imgInterval) {
                            clearInterval(_imgInterval);
                        }

                        item.isCheckingImgSize = false;

                        _mfpTrigger('ImageHasSize', item);

                        if (item.imgHidden) {
                            if (mfp.content)
                                mfp.content.removeClass('mfp-loading');

                            item.imgHidden = false;
                        }

                    }
                },

                /**
                 * Function that loops until the image has size to display elements that rely on it asap
                 */
                findImageSize: function(item) {

                    var counter = 0,
                        img = item.img[0],
                        mfpSetInterval = function(delay) {

                            if (_imgInterval) {
                                clearInterval(_imgInterval);
                            }
                            // decelerating interval that checks for size of an image
                            _imgInterval = setInterval(function() {
                                if (img.naturalWidth > 0) {
                                    mfp._onImageHasSize(item);
                                    return;
                                }

                                if (counter > 200) {
                                    clearInterval(_imgInterval);
                                }

                                counter++;
                                if (counter === 3) {
                                    mfpSetInterval(10);
                                } else if (counter === 40) {
                                    mfpSetInterval(50);
                                } else if (counter === 100) {
                                    mfpSetInterval(500);
                                }
                            }, delay);
                        };

                    mfpSetInterval(1);
                },

                getImage: function(item, template) {

                    var guard = 0,

                        // image load complete handler
                        onLoadComplete = function() {
                            if (item) {
                                if (item.img[0].complete) {
                                    item.img.off('.mfploader');

                                    if (item === mfp.currItem) {
                                        mfp._onImageHasSize(item);

                                        mfp.updateStatus('ready');
                                    }

                                    item.hasSize = true;
                                    item.loaded = true;

                                    _mfpTrigger('ImageLoadComplete');

                                } else {
                                    // if image complete check fails 200 times (20 sec), we assume that there was an error.
                                    guard++;
                                    if (guard < 200) {
                                        setTimeout(onLoadComplete, 100);
                                    } else {
                                        onLoadError();
                                    }
                                }
                            }
                        },

                        // image error handler
                        onLoadError = function() {
                            if (item) {
                                item.img.off('.mfploader');
                                if (item === mfp.currItem) {
                                    mfp._onImageHasSize(item);
                                    mfp.updateStatus('error', imgSt.tError.replace('%url%', item.src));
                                }

                                item.hasSize = true;
                                item.loaded = true;
                                item.loadError = true;
                            }
                        },
                        imgSt = mfp.st.image;


                    var el = template.find('.mfp-img');
                    if (el.length) {
                        var img = document.createElement('img');
                        img.className = 'mfp-img';
                        item.img = $(img).on('load.mfploader', onLoadComplete).on('error.mfploader', onLoadError);
                        img.src = item.src;

                        // without clone() "error" event is not firing when IMG is replaced by new IMG
                        // TODO: find a way to avoid such cloning
                        if (el.is('img')) {
                            item.img = item.img.clone();
                        }

                        img = item.img[0];
                        if (img.naturalWidth > 0) {
                            item.hasSize = true;
                        } else if (!img.width) {
                            item.hasSize = false;
                        }
                    }

                    mfp._parseMarkup(template, {
                        title: _getTitle(item),
                        img_replaceWith: item.img
                    }, item);

                    mfp.resizeImage();

                    if (item.hasSize) {
                        if (_imgInterval) clearInterval(_imgInterval);

                        if (item.loadError) {
                            template.addClass('mfp-loading');
                            mfp.updateStatus('error', imgSt.tError.replace('%url%', item.src));
                        } else {
                            template.removeClass('mfp-loading');
                            mfp.updateStatus('ready');
                        }
                        return template;
                    }

                    mfp.updateStatus('loading');
                    item.loading = true;

                    if (!item.hasSize) {
                        item.imgHidden = true;
                        template.addClass('mfp-loading');
                        mfp.findImageSize(item);
                    }

                    return template;
                }
            }
        });



        /*>>image*/

        /*>>zoom*/
        var hasMozTransform,
            getHasMozTransform = function() {
                if (hasMozTransform === undefined) {
                    hasMozTransform = document.createElement('p').style.MozTransform !== undefined;
                }
                return hasMozTransform;
            };

        $.magnificPopup.registerModule('zoom', {

            options: {
                enabled: false,
                easing: 'ease-in-out',
                duration: 300,
                opener: function(element) {
                    return element.is('img') ? element : element.find('img');
                }
            },

            proto: {

                initZoom: function() {
                    var zoomSt = mfp.st.zoom,
                        ns = '.zoom',
                        image;

                    if (!zoomSt.enabled || !mfp.supportsTransition) {
                        return;
                    }

                    var duration = zoomSt.duration,
                        getElToAnimate = function(image) {
                            var newImg = image.clone().removeAttr('style').removeAttr('class').addClass('mfp-animated-image'),
                                transition = 'all ' + (zoomSt.duration / 1000) + 's ' + zoomSt.easing,
                                cssObj = {
                                    position: 'fixed',
                                    zIndex: 9999,
                                    left: 0,
                                    top: 0,
                                    '-webkit-backface-visibility': 'hidden'
                                },
                                t = 'transition';

                            cssObj['-webkit-' + t] = cssObj['-moz-' + t] = cssObj['-o-' + t] = cssObj[t] = transition;

                            newImg.css(cssObj);
                            return newImg;
                        },
                        showMainContent = function() {
                            mfp.content.css('visibility', 'visible');
                        },
                        openTimeout,
                        animatedImg;

                    _mfpOn('BuildControls' + ns, function() {
                        if (mfp._allowZoom()) {

                            clearTimeout(openTimeout);
                            mfp.content.css('visibility', 'hidden');

                            // Basically, all code below does is clones existing image, puts in on top of the current one and animated it

                            image = mfp._getItemToZoom();

                            if (!image) {
                                showMainContent();
                                return;
                            }

                            animatedImg = getElToAnimate(image);

                            animatedImg.css(mfp._getOffset());

                            mfp.wrap.append(animatedImg);

                            openTimeout = setTimeout(function() {
                                animatedImg.css(mfp._getOffset(true));
                                openTimeout = setTimeout(function() {

                                    showMainContent();

                                    setTimeout(function() {
                                        animatedImg.remove();
                                        image = animatedImg = null;
                                        _mfpTrigger('ZoomAnimationEnded');
                                    }, 16); // avoid blink when switching images 

                                }, duration); // this timeout equals animation duration

                            }, 16); // by adding this timeout we avoid short glitch at the beginning of animation


                            // Lots of timeouts...
                        }
                    });
                    _mfpOn(BEFORE_CLOSE_EVENT + ns, function() {
                        if (mfp._allowZoom()) {

                            clearTimeout(openTimeout);

                            mfp.st.removalDelay = duration;

                            if (!image) {
                                image = mfp._getItemToZoom();
                                if (!image) {
                                    return;
                                }
                                animatedImg = getElToAnimate(image);
                            }


                            animatedImg.css(mfp._getOffset(true));
                            mfp.wrap.append(animatedImg);
                            mfp.content.css('visibility', 'hidden');

                            setTimeout(function() {
                                animatedImg.css(mfp._getOffset());
                            }, 16);
                        }

                    });

                    _mfpOn(CLOSE_EVENT + ns, function() {
                        if (mfp._allowZoom()) {
                            showMainContent();
                            if (animatedImg) {
                                animatedImg.remove();
                            }
                            image = null;
                        }
                    });
                },

                _allowZoom: function() {
                    return mfp.currItem.type === 'image';
                },

                _getItemToZoom: function() {
                    if (mfp.currItem.hasSize) {
                        return mfp.currItem.img;
                    } else {
                        return false;
                    }
                },

                // Get element postion relative to viewport
                _getOffset: function(isLarge) {
                    var el;
                    if (isLarge) {
                        el = mfp.currItem.img;
                    } else {
                        el = mfp.st.zoom.opener(mfp.currItem.el || mfp.currItem);
                    }

                    var offset = el.offset();
                    var paddingTop = parseInt(el.css('padding-top'), 10);
                    var paddingBottom = parseInt(el.css('padding-bottom'), 10);
                    offset.top -= ($(window).scrollTop() - paddingTop);


                    /*
                    
                    Animating left + top + width/height looks glitchy in Firefox, but perfect in Chrome. And vice-versa.

                     */
                    var obj = {
                        width: el.width(),
                        // fix Zepto height+padding issue
                        height: (_isJQ ? el.innerHeight() : el[0].offsetHeight) - paddingBottom - paddingTop
                    };

                    // I hate to do this, but there is no another option
                    if (getHasMozTransform()) {
                        obj['-moz-transform'] = obj['transform'] = 'translate(' + offset.left + 'px,' + offset.top + 'px)';
                    } else {
                        obj.left = offset.left;
                        obj.top = offset.top;
                    }
                    return obj;
                }

            }
        });



        /*>>zoom*/

        /*>>iframe*/

        var IFRAME_NS = 'iframe',
            _emptyPage = '//about:blank',

            _fixIframeBugs = function(isShowing) {
                if (mfp.currTemplate[IFRAME_NS]) {
                    var el = mfp.currTemplate[IFRAME_NS].find('iframe');
                    if (el.length) {
                        // reset src after the popup is closed to avoid "video keeps playing after popup is closed" bug
                        if (!isShowing) {
                            el[0].src = _emptyPage;
                        }

                        // IE8 black screen bug fix
                        if (mfp.isIE8) {
                            el.css('display', isShowing ? 'block' : 'none');
                        }
                    }
                }
            };

        $.magnificPopup.registerModule(IFRAME_NS, {

            options: {
                markup: '<div class="mfp-iframe-scaler">' +
                    '<div class="mfp-close"></div>' +
                    '<iframe class="mfp-iframe" src="//about:blank" frameborder="0" allowfullscreen></iframe>' +
                    '</div>',

                srcAction: 'iframe_src',

                // we don't care and support only one default type of URL by default
                patterns: {
                    youtube: {
                        index: 'youtube.com',
                        id: 'v=',
                        src: '//www.youtube.com/embed/%id%?autoplay=1'
                    },
                    vimeo: {
                        index: 'vimeo.com/',
                        id: '/',
                        src: '//player.vimeo.com/video/%id%?autoplay=1'
                    },
                    gmaps: {
                        index: '//maps.google.',
                        src: '%id%&output=embed'
                    }
                }
            },

            proto: {
                initIframe: function() {
                    mfp.types.push(IFRAME_NS);

                    _mfpOn('BeforeChange', function(e, prevType, newType) {
                        if (prevType !== newType) {
                            if (prevType === IFRAME_NS) {
                                _fixIframeBugs(); // iframe if removed
                            } else if (newType === IFRAME_NS) {
                                _fixIframeBugs(true); // iframe is showing
                            }
                        } // else {
                        // iframe source is switched, don't do anything
                        //}
                    });

                    _mfpOn(CLOSE_EVENT + '.' + IFRAME_NS, function() {
                        _fixIframeBugs();
                    });
                },

                getIframe: function(item, template) {
                    var embedSrc = item.src;
                    var iframeSt = mfp.st.iframe;

                    $.each(iframeSt.patterns, function() {
                        if (embedSrc.indexOf(this.index) > -1) {
                            if (this.id) {
                                if (typeof this.id === 'string') {
                                    embedSrc = embedSrc.substr(embedSrc.lastIndexOf(this.id) + this.id.length, embedSrc.length);
                                } else {
                                    embedSrc = this.id.call(this, embedSrc);
                                }
                            }
                            embedSrc = this.src.replace('%id%', embedSrc);
                            return false; // break;
                        }
                    });

                    var dataObj = {};
                    if (iframeSt.srcAction) {
                        dataObj[iframeSt.srcAction] = embedSrc;
                    }
                    mfp._parseMarkup(template, dataObj, item);

                    mfp.updateStatus('ready');

                    return template;
                }
            }
        });



        /*>>iframe*/

        /*>>gallery*/
        /**
         * Get looped index depending on number of slides
         */
        var _getLoopedId = function(index) {
            var numSlides = mfp.items.length;
            if (index > numSlides - 1) {
                return index - numSlides;
            } else if (index < 0) {
                return numSlides + index;
            }
            return index;
        },
            _replaceCurrTotal = function(text, curr, total) {
                return text.replace(/%curr%/gi, curr + 1).replace(/%total%/gi, total);
            };

        $.magnificPopup.registerModule('gallery', {

            options: {
                enabled: false,
                arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',
                preload: [0, 2],
                navigateByImgClick: true,
                arrows: true,

                tPrev: LANG_PLUGIN.Magnific[$.LANG_SITE].tPrev,
                tNext: LANG_PLUGIN.Magnific[$.LANG_SITE].tNext,
                tCounter: LANG_PLUGIN.Magnific[$.LANG_SITE].tCounter
            },

            proto: {
                initGallery: function() {

                    var gSt = mfp.st.gallery,
                        ns = '.mfp-gallery',
                        supportsFastClick = Boolean($.fn.mfpFastClick);

                    mfp.direction = true; // true - next, false - prev

                    if (!gSt || !gSt.enabled) return false;

                    _wrapClasses += ' mfp-gallery';

                    _mfpOn(OPEN_EVENT + ns, function() {

                        if (gSt.navigateByImgClick) {
                            mfp.wrap.on('click' + ns, '.mfp-img', function() {
                                if (mfp.items.length > 1) {
                                    mfp.next();
                                    return false;
                                }
                            });
                        }

                        _document.on('keydown' + ns, function(e) {
                            if (e.keyCode === 37) {
                                mfp.prev();
                            } else if (e.keyCode === 39) {
                                mfp.next();
                            }
                        });
                    });

                    _mfpOn('UpdateStatus' + ns, function(e, data) {
                        if (data.text) {
                            data.text = _replaceCurrTotal(data.text, mfp.currItem.index, mfp.items.length);
                        }
                    });

                    _mfpOn(MARKUP_PARSE_EVENT + ns, function(e, element, values, item) {
                        var l = mfp.items.length;
                        values.counter = l > 1 ? _replaceCurrTotal(gSt.tCounter, item.index, l) : '';
                    });

                    _mfpOn('BuildControls' + ns, function() {
                        if (mfp.items.length > 1 && gSt.arrows && !mfp.arrowLeft) {
                            var markup = gSt.arrowMarkup,
                                arrowLeft = mfp.arrowLeft = $(markup.replace(/%title%/gi, gSt.tPrev).replace(/%dir%/gi, 'left')).addClass(PREVENT_CLOSE_CLASS),
                                arrowRight = mfp.arrowRight = $(markup.replace(/%title%/gi, gSt.tNext).replace(/%dir%/gi, 'right')).addClass(PREVENT_CLOSE_CLASS);

                            var eName = supportsFastClick ? 'mfpFastClick' : 'click';
                            arrowLeft[eName](function() {
                                mfp.prev();
                            });
                            arrowRight[eName](function() {
                                mfp.next();
                            });

                            // Polyfill for :before and :after (adds elements with classes mfp-a and mfp-b)
                            if (mfp.isIE7) {
                                _getEl('b', arrowLeft[0], false, true);
                                _getEl('a', arrowLeft[0], false, true);
                                _getEl('b', arrowRight[0], false, true);
                                _getEl('a', arrowRight[0], false, true);
                            }

                            mfp.container.append(arrowLeft.add(arrowRight));
                        }
                    });

                    _mfpOn(CHANGE_EVENT + ns, function() {
                        if (mfp._preloadTimeout) clearTimeout(mfp._preloadTimeout);

                        mfp._preloadTimeout = setTimeout(function() {
                            mfp.preloadNearbyImages();
                            mfp._preloadTimeout = null;
                        }, 16);
                    });


                    _mfpOn(CLOSE_EVENT + ns, function() {
                        _document.off(ns);
                        mfp.wrap.off('click' + ns);

                        if (mfp.arrowLeft && supportsFastClick) {
                            mfp.arrowLeft.add(mfp.arrowRight).destroyMfpFastClick();
                        }
                        mfp.arrowRight = mfp.arrowLeft = null;
                    });

                },
                next: function() {
                    mfp.direction = true;
                    mfp.index = _getLoopedId(mfp.index + 1);
                    mfp.updateItemHTML();
                },
                prev: function() {
                    mfp.direction = false;
                    mfp.index = _getLoopedId(mfp.index - 1);
                    mfp.updateItemHTML();
                },
                goTo: function(newIndex) {
                    mfp.direction = (newIndex >= mfp.index);
                    mfp.index = newIndex;
                    mfp.updateItemHTML();
                },
                preloadNearbyImages: function() {
                    var p = mfp.st.gallery.preload,
                        preloadBefore = Math.min(p[0], mfp.items.length),
                        preloadAfter = Math.min(p[1], mfp.items.length),
                        i;

                    for (i = 1; i <= (mfp.direction ? preloadAfter : preloadBefore); i++) {
                        mfp._preloadItem(mfp.index + i);
                    }
                    for (i = 1; i <= (mfp.direction ? preloadBefore : preloadAfter); i++) {
                        mfp._preloadItem(mfp.index - i);
                    }
                },
                _preloadItem: function(index) {
                    index = _getLoopedId(index);

                    if (mfp.items[index].preloaded) {
                        return;
                    }

                    var item = mfp.items[index];
                    if (!item.parsed) {
                        item = mfp.parseEl(index);
                    }

                    _mfpTrigger('LazyLoad', item);

                    if (item.type === 'image') {
                        item.img = $('<img class="mfp-img" />').on('load.mfploader', function() {
                            item.hasSize = true;
                        }).on('error.mfploader', function() {
                            item.hasSize = true;
                            item.loadError = true;
                            _mfpTrigger('LazyLoadError', item);
                        }).attr('src', item.src);
                    }


                    item.preloaded = true;
                }
            }
        });

        /*
        Touch Support that might be implemented some day

        addSwipeGesture: function() {
            var startX,
                moved,
                multipleTouches;

                return;

            var namespace = '.mfp',
                addEventNames = function(pref, down, move, up, cancel) {
                    mfp._tStart = pref + down + namespace;
                    mfp._tMove = pref + move + namespace;
                    mfp._tEnd = pref + up + namespace;
                    mfp._tCancel = pref + cancel + namespace;
                };

            if(window.navigator.msPointerEnabled) {
                addEventNames('MSPointer', 'Down', 'Move', 'Up', 'Cancel');
            } else if('ontouchstart' in window) {
                addEventNames('touch', 'start', 'move', 'end', 'cancel');
            } else {
                return;
            }
            _window.on(mfp._tStart, function(e) {
                var oE = e.originalEvent;
                multipleTouches = moved = false;
                startX = oE.pageX || oE.changedTouches[0].pageX;
            }).on(mfp._tMove, function(e) {
                if(e.originalEvent.touches.length > 1) {
                    multipleTouches = e.originalEvent.touches.length;
                } else {
                    //e.preventDefault();
                    moved = true;
                }
            }).on(mfp._tEnd + ' ' + mfp._tCancel, function(e) {
                if(moved && !multipleTouches) {
                    var oE = e.originalEvent,
                        diff = startX - (oE.pageX || oE.changedTouches[0].pageX);

                    if(diff > 20) {
                        mfp.next();
                    } else if(diff < -20) {
                        mfp.prev();
                    }
                }
            });
        },
        */


        /*>>gallery*/

        /*>>retina*/

        var RETINA_NS = 'retina';

        $.magnificPopup.registerModule(RETINA_NS, {
            options: {
                replaceSrc: function(item) {
                    return item.src.replace(/\.\w+$/, function(m) {
                        return '@2x' + m;
                    });
                },
                ratio: 1 // Function or number.  Set to 1 to disable.
            },
            proto: {
                initRetina: function() {
                    if (window.devicePixelRatio > 1) {

                        var st = mfp.st.retina,
                            ratio = st.ratio;

                        ratio = !isNaN(ratio) ? ratio : ratio();

                        if (ratio > 1) {
                            _mfpOn('ImageHasSize' + '.' + RETINA_NS, function(e, item) {
                                item.img.css({
                                    'max-width': item.img[0].naturalWidth / ratio,
                                    'width': '100%'
                                });
                            });
                            _mfpOn('ElementParse' + '.' + RETINA_NS, function(e, item) {
                                item.src = st.replaceSrc(item, ratio);
                            });
                        }
                    }

                }
            }
        });

        /*>>retina*/

        /*>>fastclick*/
        /**
         * FastClick event implementation. (removes 300ms delay on touch devices)
         * Based on https://developers.google.com/mobile/articles/fast_buttons
         *
         * You may use it outside the Magnific Popup by calling just:
         *
         * $('.your-el').mfpFastClick(function() {
         *     console.log('Clicked!');
         * });
         *
         * To unbind:
         * $('.your-el').destroyMfpFastClick();
         *
         *
         * Note that it's a very basic and simple implementation, it blocks ghost click on the same element where it was bound.
         * If you need something more advanced, use plugin by FT Labs https://github.com/ftlabs/fastclick
         *
         */

        (function() {
            var ghostClickDelay = 1000,
                supportsTouch = 'ontouchstart' in window,
                unbindTouchMove = function() {
                    _window.off('touchmove' + ns + ' touchend' + ns);
                },
                eName = 'mfpFastClick',
                ns = '.' + eName;


            // As Zepto.js doesn't have an easy way to add custom events (like jQuery), so we implement it in this way
            $.fn.mfpFastClick = function(callback) {

                return $(this).each(function() {

                    var elem = $(this),
                        lock;

                    if (supportsTouch) {

                        var timeout,
                            startX,
                            startY,
                            pointerMoved,
                            point,
                            numPointers;

                        elem.on('touchstart' + ns, function(e) {
                            pointerMoved = false;
                            numPointers = 1;

                            point = e.originalEvent ? e.originalEvent.touches[0] : e.touches[0];
                            startX = point.clientX;
                            startY = point.clientY;

                            _window.on('touchmove' + ns, function(e) {
                                point = e.originalEvent ? e.originalEvent.touches : e.touches;
                                numPointers = point.length;
                                point = point[0];
                                if (Math.abs(point.clientX - startX) > 10 ||
                                    Math.abs(point.clientY - startY) > 10) {
                                    pointerMoved = true;
                                    unbindTouchMove();
                                }
                            }).on('touchend' + ns, function(e) {
                                unbindTouchMove();
                                if (pointerMoved || numPointers > 1) {
                                    return;
                                }
                                lock = true;
                                e.preventDefault();
                                clearTimeout(timeout);
                                timeout = setTimeout(function() {
                                    lock = false;
                                }, ghostClickDelay);
                                callback();
                            });
                        });

                    }

                    elem.on('click' + ns, function() {
                        if (!lock) {
                            callback();
                        }
                    });
                });
            };

            $.fn.destroyMfpFastClick = function() {
                $(this).off('touchstart' + ns + ' click' + ns);
                if (supportsTouch) _window.off('touchmove' + ns + ' touchend' + ns);
            };
        })();

        /*>>fastclick*/
        _checkInstance();
    })(window.jQuery || window.Zepto);
/*-------------------------------  Fred  ---------------------------------------*/
    /*
     *  jQuery carouFredSel 6.2.1
     *  Demo's and documentation:
     *  caroufredsel.dev7studios.com
     *
     *  Copyright (c) 2013 Fred Heusschen
     *  www.frebsite.nl
     *
     *  Dual licensed under the MIT and GPL licenses.
     *  http://en.wikipedia.org/wiki/MIT_License
     *  http://en.wikipedia.org/wiki/GNU_General_Public_License
     */


    (function($) {


        //  LOCAL

        if ( $.fn.carouFredSel )
        {
            return;
        }

        $.fn.caroufredsel = $.fn.carouFredSel = function(options, configs)
        {

            //  no element
            if (this.length == 0)
            {
                debug( true, 'No element found for "' + this.selector + '".' );
                return this;
            }

            //  multiple elements
            if (this.length > 1)
            {
                return this.each(function() {
                    $(this).carouFredSel(options, configs);
                });
            }


            var $cfs = this,
                $tt0 = this[0],
                starting_position = false;

            if ($cfs.data('_cfs_isCarousel'))
            {
                starting_position = $cfs.triggerHandler('_cfs_triggerEvent', 'currentPosition');
                $cfs.trigger('_cfs_triggerEvent', ['destroy', true]);
            }

            var FN = {};

            FN._init = function(o, setOrig, start)
            {
                o = go_getObject($tt0, o);

                o.items = go_getItemsObject($tt0, o.items);
                o.scroll = go_getScrollObject($tt0, o.scroll);
                o.auto = go_getAutoObject($tt0, o.auto);
                o.prev = go_getPrevNextObject($tt0, o.prev);
                o.next = go_getPrevNextObject($tt0, o.next);
                o.pagination = go_getPaginationObject($tt0, o.pagination);
                o.swipe = go_getSwipeObject($tt0, o.swipe);
                o.mousewheel = go_getMousewheelObject($tt0, o.mousewheel);

                if (setOrig)
                {
                    opts_orig = $.extend(true, {}, $.fn.carouFredSel.defaults, o);
                }

                opts = $.extend(true, {}, $.fn.carouFredSel.defaults, o);
                opts.d = cf_getDimensions(opts);

                crsl.direction = (opts.direction == 'up' || opts.direction == 'left') ? 'next' : 'prev';

                var a_itm = $cfs.children(),
                    avail_primary = ms_getParentSize($wrp, opts, 'width');

                if (is_true(opts.cookie))
                {
                    opts.cookie = 'caroufredsel_cookie_' + conf.serialNumber;
                }

                opts.maxDimension = ms_getMaxDimension(opts, avail_primary);

                //  complement items and sizes
                opts.items = in_complementItems(opts.items, opts, a_itm, start);
                opts[opts.d['width']] = in_complementPrimarySize(opts[opts.d['width']], opts, a_itm);
                opts[opts.d['height']] = in_complementSecondarySize(opts[opts.d['height']], opts, a_itm);

                //  primary size not set for a responsive carousel
                if (opts.responsive)
                {
                    if (!is_percentage(opts[opts.d['width']]))
                    {
                        opts[opts.d['width']] = '100%';
                    }
                }

                //  primary size is percentage
                if (is_percentage(opts[opts.d['width']]))
                {
                    crsl.upDateOnWindowResize = true;
                    crsl.primarySizePercentage = opts[opts.d['width']];
                    opts[opts.d['width']] = ms_getPercentage(avail_primary, crsl.primarySizePercentage);
                    if (!opts.items.visible)
                    {
                        opts.items.visibleConf.variable = true;
                    }
                }

                if (opts.responsive)
                {
                    opts.usePadding = false;
                    opts.padding = [0, 0, 0, 0];
                    opts.align = false;
                    opts.items.visibleConf.variable = false;
                }
                else
                {
                    //  visible-items not set
                    if (!opts.items.visible)
                    {
                        opts = in_complementVisibleItems(opts, avail_primary);
                    }

                    //  primary size not set -> calculate it or set to "variable"
                    if (!opts[opts.d['width']])
                    {
                        if (!opts.items.visibleConf.variable && is_number(opts.items[opts.d['width']]) && opts.items.filter == '*')
                        {
                            opts[opts.d['width']] = opts.items.visible * opts.items[opts.d['width']];
                            opts.align = false;
                        }
                        else
                        {
                            opts[opts.d['width']] = 'variable';
                        }
                    }
                    //  align not set -> set to center if primary size is number
                    if (is_undefined(opts.align))
                    {
                        opts.align = (is_number(opts[opts.d['width']]))
                            ? 'center'
                            : false;
                    }
                    //  set variabe visible-items
                    if (opts.items.visibleConf.variable)
                    {
                        opts.items.visible = gn_getVisibleItemsNext(a_itm, opts, 0);
                    }
                }

                //  set visible items by filter
                if (opts.items.filter != '*' && !opts.items.visibleConf.variable)
                {
                    opts.items.visibleConf.org = opts.items.visible;
                    opts.items.visible = gn_getVisibleItemsNextFilter(a_itm, opts, 0);
                }

                opts.items.visible = cf_getItemsAdjust(opts.items.visible, opts, opts.items.visibleConf.adjust, $tt0);
                opts.items.visibleConf.old = opts.items.visible;

                if (opts.responsive)
                {
                    if (!opts.items.visibleConf.min)
                    {
                        opts.items.visibleConf.min = opts.items.visible;
                    }
                    if (!opts.items.visibleConf.max)
                    {
                        opts.items.visibleConf.max = opts.items.visible;
                    }
                    opts = in_getResponsiveValues(opts, a_itm, avail_primary);
                }
                else
                {
                    opts.padding = cf_getPadding(opts.padding);

                    if (opts.align == 'top')
                    {
                        opts.align = 'left';
                    }
                    else if (opts.align == 'bottom')
                    {
                        opts.align = 'right';
                    }

                    switch (opts.align)
                    {
                        //  align: center, left or right
                        case 'center':
                        case 'left':
                        case 'right':
                            if (opts[opts.d['width']] != 'variable')
                            {
                                opts = in_getAlignPadding(opts, a_itm);
                                opts.usePadding = true;
                            }
                            break;

                        //  padding
                        default:
                            opts.align = false;
                            opts.usePadding = (
                                opts.padding[0] == 0 && 
                                opts.padding[1] == 0 && 
                                opts.padding[2] == 0 && 
                                opts.padding[3] == 0
                            ) ? false : true;
                            break;
                    }
                }

                if (!is_number(opts.scroll.duration))
                {
                    opts.scroll.duration = 500;
                }
                if (is_undefined(opts.scroll.items))
                {
                    opts.scroll.items = (opts.responsive || opts.items.visibleConf.variable || opts.items.filter != '*') 
                        ? 'visible'
                        : opts.items.visible;
                }

                opts.auto = $.extend(true, {}, opts.scroll, opts.auto);
                opts.prev = $.extend(true, {}, opts.scroll, opts.prev);
                opts.next = $.extend(true, {}, opts.scroll, opts.next);
                opts.pagination = $.extend(true, {}, opts.scroll, opts.pagination);
                //  swipe and mousewheel extend later on, per direction

                opts.auto = go_complementAutoObject($tt0, opts.auto);
                opts.prev = go_complementPrevNextObject($tt0, opts.prev);
                opts.next = go_complementPrevNextObject($tt0, opts.next);
                opts.pagination = go_complementPaginationObject($tt0, opts.pagination);
                opts.swipe = go_complementSwipeObject($tt0, opts.swipe);
                opts.mousewheel = go_complementMousewheelObject($tt0, opts.mousewheel);

                if (opts.synchronise)
                {
                    opts.synchronise = cf_getSynchArr(opts.synchronise);
                }


                //  DEPRECATED
                if (opts.auto.onPauseStart)
                {
                    opts.auto.onTimeoutStart = opts.auto.onPauseStart;
                    deprecated('auto.onPauseStart', 'auto.onTimeoutStart');
                }
                if (opts.auto.onPausePause)
                {
                    opts.auto.onTimeoutPause = opts.auto.onPausePause;
                    deprecated('auto.onPausePause', 'auto.onTimeoutPause');
                }
                if (opts.auto.onPauseEnd)
                {
                    opts.auto.onTimeoutEnd = opts.auto.onPauseEnd;
                    deprecated('auto.onPauseEnd', 'auto.onTimeoutEnd');
                }
                if (opts.auto.pauseDuration)
                {
                    opts.auto.timeoutDuration = opts.auto.pauseDuration;
                    deprecated('auto.pauseDuration', 'auto.timeoutDuration');
                }
                //  /DEPRECATED


            };  //  /init


            FN._build = function() {
                $cfs.data('_cfs_isCarousel', true);

                var a_itm = $cfs.children(),
                    orgCSS = in_mapCss($cfs, ['textAlign', 'float', 'position', 'top', 'right', 'bottom', 'left', 'zIndex', 'width', 'height', 'marginTop', 'marginRight', 'marginBottom', 'marginLeft']),
                    newPosition = 'relative';

                switch (orgCSS.position)
                {
                    case 'absolute':
                    case 'fixed':
                        newPosition = orgCSS.position;
                        break;
                }

                if (conf.wrapper == 'parent')
                {
                    sz_storeOrigCss($wrp);
                }
                else
                {
                    $wrp.css(orgCSS);
                }
                $wrp.css({
                    'overflow'      : 'hidden',
                    'position'      : newPosition
                });

                sz_storeOrigCss($cfs);
                $cfs.data('_cfs_origCssZindex', orgCSS.zIndex);
                $cfs.css({
                    'textAlign'     : 'left',
                    'float'         : 'none',
                    'position'      : 'absolute',
                    'top'           : 0,
                    'right'         : 'auto',
                    'bottom'        : 'auto',
                    'left'          : 0,
                    'marginTop'     : 0,
                    'marginRight'   : 0,
                    'marginBottom'  : 0,
                    'marginLeft'    : 0
                });

                sz_storeMargin(a_itm, opts);
                sz_storeOrigCss(a_itm);
                if (opts.responsive)
                {
                    sz_setResponsiveSizes(opts, a_itm);
                }

            };  //  /build


            FN._bind_events = function() {
                FN._unbind_events();


                //  stop event
                $cfs.bind(cf_e('stop', conf), function(e, imm) {
                    e.stopPropagation();

                    //  button
                    if (!crsl.isStopped)
                    {
                        if (opts.auto.button)
                        {
                            opts.auto.button.addClass(cf_c('stopped', conf));
                        }
                    }

                    //  set stopped
                    crsl.isStopped = true;

                    if (opts.auto.play)
                    {
                        opts.auto.play = false;
                        $cfs.trigger(cf_e('pause', conf), imm);
                    }
                    return true;
                });


                //  finish event
                $cfs.bind(cf_e('finish', conf), function(e) {
                    e.stopPropagation();
                    if (crsl.isScrolling)
                    {
                        sc_stopScroll(scrl);
                    }
                    return true;
                });


                //  pause event
                $cfs.bind(cf_e('pause', conf), function(e, imm, res) {
                    e.stopPropagation();
                    tmrs = sc_clearTimers(tmrs);

                    //  immediately pause
                    if (imm && crsl.isScrolling)
                    {
                        scrl.isStopped = true;
                        var nst = getTime() - scrl.startTime;
                        scrl.duration -= nst;
                        if (scrl.pre)
                        {
                            scrl.pre.duration -= nst;
                        }
                        if (scrl.post)
                        {
                            scrl.post.duration -= nst;
                        }
                        sc_stopScroll(scrl, false);
                    }

                    //  update remaining pause-time
                    if (!crsl.isPaused && !crsl.isScrolling)
                    {
                        if (res)
                        {
                            tmrs.timePassed += getTime() - tmrs.startTime;
                        }
                    }

                    //  button
                    if (!crsl.isPaused)
                    {
                        if (opts.auto.button)
                        {
                            opts.auto.button.addClass(cf_c('paused', conf));
                        }
                    }

                    //  set paused
                    crsl.isPaused = true;

                    //  pause pause callback
                    if (opts.auto.onTimeoutPause)
                    {
                        var dur1 = opts.auto.timeoutDuration - tmrs.timePassed,
                            perc = 100 - Math.ceil( dur1 * 100 / opts.auto.timeoutDuration );

                        opts.auto.onTimeoutPause.call($tt0, perc, dur1);
                    }
                    return true;
                });


                //  play event
                $cfs.bind(cf_e('play', conf), function(e, dir, del, res) {
                    e.stopPropagation();
                    tmrs = sc_clearTimers(tmrs);

                    //  sort params
                    var v = [dir, del, res],
                        t = ['string', 'number', 'boolean'],
                        a = cf_sortParams(v, t);

                    dir = a[0];
                    del = a[1];
                    res = a[2];

                    if (dir != 'prev' && dir != 'next')
                    {
                        dir = crsl.direction;
                    }
                    if (!is_number(del))
                    {
                        del = 0;
                    }
                    if (!is_boolean(res))
                    {
                        res = false;
                    }

                    //  stopped?
                    if (res)
                    {
                        crsl.isStopped = false;
                        opts.auto.play = true;
                    }
                    if (!opts.auto.play)
                    {
                        e.stopImmediatePropagation();
                        return debug(conf, 'Carousel stopped: Not scrolling.');
                    }

                    //  button
                    if (crsl.isPaused)
                    {
                        if (opts.auto.button)
                        {
                            opts.auto.button.removeClass(cf_c('stopped', conf));
                            opts.auto.button.removeClass(cf_c('paused', conf));
                        }
                    }

                    //  set playing
                    crsl.isPaused = false;
                    tmrs.startTime = getTime();

                    //  timeout the scrolling
                    var dur1 = opts.auto.timeoutDuration + del;
                        dur2 = dur1 - tmrs.timePassed;
                        perc = 100 - Math.ceil(dur2 * 100 / dur1);

                    if (opts.auto.progress)
                    {
                        tmrs.progress = setInterval(function() {
                            var pasd = getTime() - tmrs.startTime + tmrs.timePassed,
                                perc = Math.ceil(pasd * 100 / dur1);
                            opts.auto.progress.updater.call(opts.auto.progress.bar[0], perc);
                        }, opts.auto.progress.interval);
                    }

                    tmrs.auto = setTimeout(function() {
                        if (opts.auto.progress)
                        {
                            opts.auto.progress.updater.call(opts.auto.progress.bar[0], 100);
                        }
                        if (opts.auto.onTimeoutEnd)
                        {
                            opts.auto.onTimeoutEnd.call($tt0, perc, dur2);
                        }
                        if (crsl.isScrolling)
                        {
                            $cfs.trigger(cf_e('play', conf), dir);
                        }
                        else
                        {
                            $cfs.trigger(cf_e(dir, conf), opts.auto);
                        }
                    }, dur2);

                    //  pause start callback
                    if (opts.auto.onTimeoutStart)
                    {
                        opts.auto.onTimeoutStart.call($tt0, perc, dur2);
                    }

                    return true;
                });


                //  resume event
                $cfs.bind(cf_e('resume', conf), function(e) {
                    e.stopPropagation();
                    if (scrl.isStopped)
                    {
                        scrl.isStopped = false;
                        crsl.isPaused = false;
                        crsl.isScrolling = true;
                        scrl.startTime = getTime();
                        sc_startScroll(scrl, conf);
                    }
                    else
                    {
                        $cfs.trigger(cf_e('play', conf));
                    }
                    return true;
                });


                //  prev + next events
                $cfs.bind(cf_e('prev', conf)+' '+cf_e('next', conf), function(e, obj, num, clb, que) {
                    e.stopPropagation();

                    //  stopped or hidden carousel, don't scroll, don't queue
                    if (crsl.isStopped || $cfs.is(':hidden'))
                    {
                        e.stopImmediatePropagation();
                        return debug(conf, 'Carousel stopped or hidden: Not scrolling.');
                    }

                    //  not enough items
                    var minimum = (is_number(opts.items.minimum)) ? opts.items.minimum : opts.items.visible + 1;
                    if (minimum > itms.total)
                    {
                        e.stopImmediatePropagation();
                        return debug(conf, 'Not enough items ('+itms.total+' total, '+minimum+' needed): Not scrolling.');
                    }

                    //  get config
                    var v = [obj, num, clb, que],
                        t = ['object', 'number/string', 'function', 'boolean'],
                        a = cf_sortParams(v, t);

                    obj = a[0];
                    num = a[1];
                    clb = a[2];
                    que = a[3];

                    var eType = e.type.slice(conf.events.prefix.length);

                    if (!is_object(obj))
                    {
                        obj = {};
                    }
                    if (is_function(clb))
                    {
                        obj.onAfter = clb;
                    }
                    if (is_boolean(que))
                    {
                        obj.queue = que;
                    }
                    obj = $.extend(true, {}, opts[eType], obj);

                    //  test conditions callback
                    if (obj.conditions && !obj.conditions.call($tt0, eType))
                    {
                        e.stopImmediatePropagation();
                        return debug(conf, 'Callback "conditions" returned false.');
                    }

                    if (!is_number(num))
                    {
                        if (opts.items.filter != '*')
                        {
                            num = 'visible';
                        }
                        else
                        {
                            var arr = [num, obj.items, opts[eType].items];
                            for (var a = 0, l = arr.length; a < l; a++)
                            {
                                if (is_number(arr[a]) || arr[a] == 'page' || arr[a] == 'visible') {
                                    num = arr[a];
                                    break;
                                }
                            }
                        }
                        switch(num) {
                            case 'page':
                                e.stopImmediatePropagation();
                                return $cfs.triggerHandler(cf_e(eType+'Page', conf), [obj, clb]);
                                break;

                            case 'visible':
                                if (!opts.items.visibleConf.variable && opts.items.filter == '*')
                                {
                                    num = opts.items.visible;
                                }
                                break;
                        }
                    }

                    //  resume animation, add current to queue
                    if (scrl.isStopped)
                    {
                        $cfs.trigger(cf_e('resume', conf));
                        $cfs.trigger(cf_e('queue', conf), [eType, [obj, num, clb]]);
                        e.stopImmediatePropagation();
                        return debug(conf, 'Carousel resumed scrolling.');
                    }

                    //  queue if scrolling
                    if (obj.duration > 0)
                    {
                        if (crsl.isScrolling)
                        {
                            if (obj.queue)
                            {
                                if (obj.queue == 'last')
                                {
                                    queu = [];
                                }
                                if (obj.queue != 'first' || queu.length == 0)
                                {
                                    $cfs.trigger(cf_e('queue', conf), [eType, [obj, num, clb]]);
                                }
                            }
                            e.stopImmediatePropagation();
                            return debug(conf, 'Carousel currently scrolling.');
                        }
                    }

                    tmrs.timePassed = 0;
                    $cfs.trigger(cf_e('slide_'+eType, conf), [obj, num]);

                    //  synchronise
                    if (opts.synchronise)
                    {
                        var s = opts.synchronise,
                            c = [obj, num];

                        for (var j = 0, l = s.length; j < l; j++) {
                            var d = eType;
                            if (!s[j][2])
                            {
                                d = (d == 'prev') ? 'next' : 'prev';
                            }
                            if (!s[j][1])
                            {
                                c[0] = s[j][0].triggerHandler('_cfs_triggerEvent', ['configuration', d]);
                            }
                            c[1] = num + s[j][3];
                            s[j][0].trigger('_cfs_triggerEvent', ['slide_'+d, c]);
                        }
                    }
                    return true;
                });


                //  prev event
                $cfs.bind(cf_e('slide_prev', conf), function(e, sO, nI) {
                    e.stopPropagation();
                    var a_itm = $cfs.children();

                    //  non-circular at start, scroll to end
                    if (!opts.circular)
                    {
                        if (itms.first == 0)
                        {
                            if (opts.infinite)
                            {
                                $cfs.trigger(cf_e('next', conf), itms.total-1);
                            }
                            return e.stopImmediatePropagation();
                        }
                    }

                    sz_resetMargin(a_itm, opts);

                    //  find number of items to scroll
                    if (!is_number(nI))
                    {
                        if (opts.items.visibleConf.variable)
                        {
                            nI = gn_getVisibleItemsPrev(a_itm, opts, itms.total-1);
                        }
                        else if (opts.items.filter != '*')
                        {
                            var xI = (is_number(sO.items)) ? sO.items : gn_getVisibleOrg($cfs, opts);
                            nI = gn_getScrollItemsPrevFilter(a_itm, opts, itms.total-1, xI);
                        }
                        else
                        {
                            nI = opts.items.visible;
                        }
                        nI = cf_getAdjust(nI, opts, sO.items, $tt0);
                    }

                    //  prevent non-circular from scrolling to far
                    if (!opts.circular)
                    {
                        if (itms.total - nI < itms.first)
                        {
                            nI = itms.total - itms.first;
                        }
                    }

                    //  set new number of visible items
                    opts.items.visibleConf.old = opts.items.visible;
                    if (opts.items.visibleConf.variable)
                    {
                        var vI = cf_getItemsAdjust(gn_getVisibleItemsNext(a_itm, opts, itms.total-nI), opts, opts.items.visibleConf.adjust, $tt0);
                        if (opts.items.visible+nI <= vI && nI < itms.total)
                        {
                            nI++;
                            vI = cf_getItemsAdjust(gn_getVisibleItemsNext(a_itm, opts, itms.total-nI), opts, opts.items.visibleConf.adjust, $tt0);
                        }
                        opts.items.visible = vI;
                    }
                    else if (opts.items.filter != '*')
                    {
                        var vI = gn_getVisibleItemsNextFilter(a_itm, opts, itms.total-nI);
                        opts.items.visible = cf_getItemsAdjust(vI, opts, opts.items.visibleConf.adjust, $tt0);
                    }

                    sz_resetMargin(a_itm, opts, true);

                    //  scroll 0, don't scroll
                    if (nI == 0)
                    {
                        e.stopImmediatePropagation();
                        return debug(conf, '0 items to scroll: Not scrolling.');
                    }
                    debug(conf, 'Scrolling '+nI+' items backward.');


                    //  save new config
                    itms.first += nI;
                    while (itms.first >= itms.total)
                    {
                        itms.first -= itms.total;
                    }

                    //  non-circular callback
                    if (!opts.circular)
                    {
                        if (itms.first == 0 && sO.onEnd)
                        {
                            sO.onEnd.call($tt0, 'prev');
                        }
                        if (!opts.infinite)
                        {
                            nv_enableNavi(opts, itms.first, conf);
                        }
                    }

                    //  rearrange items
                    $cfs.children().slice(itms.total-nI, itms.total).prependTo($cfs);
                    if (itms.total < opts.items.visible + nI)
                    {
                        $cfs.children().slice(0, (opts.items.visible+nI)-itms.total).clone(true).appendTo($cfs);
                    }

                    //  the needed items
                    var a_itm = $cfs.children(),
                        i_old = gi_getOldItemsPrev(a_itm, opts, nI),
                        i_new = gi_getNewItemsPrev(a_itm, opts),
                        i_cur_l = a_itm.eq(nI-1),
                        i_old_l = i_old.last(),
                        i_new_l = i_new.last();

                    sz_resetMargin(a_itm, opts);

                    var pL = 0,
                        pR = 0;

                    if (opts.align)
                    {
                        var p = cf_getAlignPadding(i_new, opts);
                        pL = p[0];
                        pR = p[1];
                    }
                    var oL = (pL < 0) ? opts.padding[opts.d[3]] : 0;

                    //  hide items for fx directscroll
                    var hiddenitems = false,
                        i_skp = $();
                    if (opts.items.visible < nI)
                    {
                        i_skp = a_itm.slice(opts.items.visibleConf.old, nI);
                        if (sO.fx == 'directscroll')
                        {
                            var orgW = opts.items[opts.d['width']];
                            hiddenitems = i_skp;
                            i_cur_l = i_new_l;
                            sc_hideHiddenItems(hiddenitems);
                            opts.items[opts.d['width']] = 'variable';
                        }
                    }

                    //  save new sizes
                    var $cf2 = false,
                        i_siz = ms_getTotalSize(a_itm.slice(0, nI), opts, 'width'),
                        w_siz = cf_mapWrapperSizes(ms_getSizes(i_new, opts, true), opts, !opts.usePadding),
                        i_siz_vis = 0,
                        a_cfs = {},
                        a_wsz = {},
                        a_cur = {},
                        a_old = {},
                        a_new = {},
                        a_lef = {},
                        a_lef_vis = {},
                        a_dur = sc_getDuration(sO, opts, nI, i_siz);

                    switch(sO.fx)
                    {
                        case 'cover':
                        case 'cover-fade':
                            i_siz_vis = ms_getTotalSize(a_itm.slice(0, opts.items.visible), opts, 'width');
                            break;
                    }

                    if (hiddenitems)
                    {
                        opts.items[opts.d['width']] = orgW;
                    }

                    sz_resetMargin(a_itm, opts, true);
                    if (pR >= 0)
                    {
                        sz_resetMargin(i_old_l, opts, opts.padding[opts.d[1]]);
                    }
                    if (pL >= 0)
                    {
                        sz_resetMargin(i_cur_l, opts, opts.padding[opts.d[3]]);
                    }

                    if (opts.align)
                    {
                        opts.padding[opts.d[1]] = pR;
                        opts.padding[opts.d[3]] = pL;
                    }

                    a_lef[opts.d['left']] = -(i_siz - oL);
                    a_lef_vis[opts.d['left']] = -(i_siz_vis - oL);
                    a_wsz[opts.d['left']] = w_siz[opts.d['width']];

                    //  scrolling functions
                    var _s_wrapper = function() {},
                        _a_wrapper = function() {},
                        _s_paddingold = function() {},
                        _a_paddingold = function() {},
                        _s_paddingnew = function() {},
                        _a_paddingnew = function() {},
                        _s_paddingcur = function() {},
                        _a_paddingcur = function() {},
                        _onafter = function() {},
                        _moveitems = function() {},
                        _position = function() {};

                    //  clone carousel
                    switch(sO.fx)
                    {
                        case 'crossfade':
                        case 'cover':
                        case 'cover-fade':
                        case 'uncover':
                        case 'uncover-fade':
                            $cf2 = $cfs.clone(true).appendTo($wrp);
                            break;
                    }
                    switch(sO.fx)
                    {
                        case 'crossfade':
                        case 'uncover':
                        case 'uncover-fade':
                            $cf2.children().slice(0, nI).remove();
                            $cf2.children().slice(opts.items.visibleConf.old).remove();
                            break;

                        case 'cover':
                        case 'cover-fade':
                            $cf2.children().slice(opts.items.visible).remove();
                            $cf2.css(a_lef_vis);
                            break;
                    }

                    $cfs.css(a_lef);

                    //  reset all scrolls
                    scrl = sc_setScroll(a_dur, sO.easing, conf);

                    //  animate / set carousel
                    a_cfs[opts.d['left']] = (opts.usePadding) ? opts.padding[opts.d[3]] : 0;

                    //  animate / set wrapper
                    if (opts[opts.d['width']] == 'variable' || opts[opts.d['height']] == 'variable')
                    {
                        _s_wrapper = function() {
                            $wrp.css(w_siz);
                        };
                        _a_wrapper = function() {
                            scrl.anims.push([$wrp, w_siz]);
                        };
                    }

                    //  animate / set items
                    if (opts.usePadding)
                    {
                        if (i_new_l.not(i_cur_l).length)
                        {
                            a_cur[opts.d['marginRight']] = i_cur_l.data('_cfs_origCssMargin');

                            if (pL < 0)
                            {
                                i_cur_l.css(a_cur);
                            }
                            else
                            {
                                _s_paddingcur = function() {
                                    i_cur_l.css(a_cur);
                                };
                                _a_paddingcur = function() {
                                    scrl.anims.push([i_cur_l, a_cur]);
                                };
                            }
                        }
                        switch(sO.fx)
                        {
                            case 'cover':
                            case 'cover-fade':
                                $cf2.children().eq(nI-1).css(a_cur);
                                break;
                        }

                        if (i_new_l.not(i_old_l).length)
                        {
                            a_old[opts.d['marginRight']] = i_old_l.data('_cfs_origCssMargin');
                            _s_paddingold = function() {
                                i_old_l.css(a_old);
                            };
                            _a_paddingold = function() {
                                scrl.anims.push([i_old_l, a_old]);
                            };
                        }

                        if (pR >= 0)
                        {
                            a_new[opts.d['marginRight']] = i_new_l.data('_cfs_origCssMargin') + opts.padding[opts.d[1]];
                            _s_paddingnew = function() {
                                i_new_l.css(a_new);
                            };
                            _a_paddingnew = function() {
                                scrl.anims.push([i_new_l, a_new]);
                            };
                        }
                    }

                    //  set position
                    _position = function() {
                        $cfs.css(a_cfs);
                    };


                    var overFill = opts.items.visible+nI-itms.total;

                    //  rearrange items
                    _moveitems = function() {
                        if (overFill > 0)
                        {
                            $cfs.children().slice(itms.total).remove();
                            i_old = $( $cfs.children().slice(itms.total-(opts.items.visible-overFill)).get().concat( $cfs.children().slice(0, overFill).get() ) );
                        }
                        sc_showHiddenItems(hiddenitems);

                        if (opts.usePadding)
                        {
                            var l_itm = $cfs.children().eq(opts.items.visible+nI-1);
                            l_itm.css(opts.d['marginRight'], l_itm.data('_cfs_origCssMargin'));
                        }
                    };


                    var cb_arguments = sc_mapCallbackArguments(i_old, i_skp, i_new, nI, 'prev', a_dur, w_siz);

                    //  fire onAfter callbacks
                    _onafter = function() {
                        sc_afterScroll($cfs, $cf2, sO);
                        crsl.isScrolling = false;
                        clbk.onAfter = sc_fireCallbacks($tt0, sO, 'onAfter', cb_arguments, clbk);
                        queu = sc_fireQueue($cfs, queu, conf);

                        if (!crsl.isPaused)
                        {
                            $cfs.trigger(cf_e('play', conf));
                        }
                    };

                    //  fire onBefore callback
                    crsl.isScrolling = true;
                    tmrs = sc_clearTimers(tmrs);
                    clbk.onBefore = sc_fireCallbacks($tt0, sO, 'onBefore', cb_arguments, clbk);

                    switch(sO.fx)
                    {
                        case 'none':
                            $cfs.css(a_cfs);
                            _s_wrapper();
                            _s_paddingold();
                            _s_paddingnew();
                            _s_paddingcur();
                            _position();
                            _moveitems();
                            _onafter();
                            break;

                        case 'fade':
                            scrl.anims.push([$cfs, { 'opacity': 0 }, function() {
                                _s_wrapper();
                                _s_paddingold();
                                _s_paddingnew();
                                _s_paddingcur();
                                _position();
                                _moveitems();
                                scrl = sc_setScroll(a_dur, sO.easing, conf);
                                scrl.anims.push([$cfs, { 'opacity': 1 }, _onafter]);
                                sc_startScroll(scrl, conf);
                            }]);
                            break;

                        case 'crossfade':
                            $cfs.css({ 'opacity': 0 });
                            scrl.anims.push([$cf2, { 'opacity': 0 }]);
                            scrl.anims.push([$cfs, { 'opacity': 1 }, _onafter]);
                            _a_wrapper();
                            _s_paddingold();
                            _s_paddingnew();
                            _s_paddingcur();
                            _position();
                            _moveitems();
                            break;

                        case 'cover':
                            scrl.anims.push([$cf2, a_cfs, function() {
                                _s_paddingold();
                                _s_paddingnew();
                                _s_paddingcur();
                                _position();
                                _moveitems();
                                _onafter();
                            }]);
                            _a_wrapper();
                            break;

                        case 'cover-fade':
                            scrl.anims.push([$cfs, { 'opacity': 0 }]);
                            scrl.anims.push([$cf2, a_cfs, function() {
                                _s_paddingold();
                                _s_paddingnew();
                                _s_paddingcur();
                                _position();
                                _moveitems();
                                _onafter();
                            }]);
                            _a_wrapper();
                            break;

                        case 'uncover':
                            scrl.anims.push([$cf2, a_wsz, _onafter]);
                            _a_wrapper();
                            _s_paddingold();
                            _s_paddingnew();
                            _s_paddingcur();
                            _position();
                            _moveitems();
                            break;

                        case 'uncover-fade':
                            $cfs.css({ 'opacity': 0 });
                            scrl.anims.push([$cfs, { 'opacity': 1 }]);
                            scrl.anims.push([$cf2, a_wsz, _onafter]);
                            _a_wrapper();
                            _s_paddingold();
                            _s_paddingnew();
                            _s_paddingcur();
                            _position();
                            _moveitems();
                            break;

                        default:
                            scrl.anims.push([$cfs, a_cfs, function() {
                                _moveitems();
                                _onafter();
                            }]);
                            _a_wrapper();
                            _a_paddingold();
                            _a_paddingnew();
                            _a_paddingcur();
                            break;
                    }

                    sc_startScroll(scrl, conf);
                    cf_setCookie(opts.cookie, $cfs, conf);

                    $cfs.trigger(cf_e('updatePageStatus', conf), [false, w_siz]);

                    return true;
                });


                //  next event
                $cfs.bind(cf_e('slide_next', conf), function(e, sO, nI) {
                    e.stopPropagation();
                    var a_itm = $cfs.children();

                    //  non-circular at end, scroll to start
                    if (!opts.circular)
                    {
                        if (itms.first == opts.items.visible)
                        {
                            if (opts.infinite)
                            {
                                $cfs.trigger(cf_e('prev', conf), itms.total-1);
                            }
                            return e.stopImmediatePropagation();
                        }
                    }

                    sz_resetMargin(a_itm, opts);

                    //  find number of items to scroll
                    if (!is_number(nI))
                    {
                        if (opts.items.filter != '*')
                        {
                            var xI = (is_number(sO.items)) ? sO.items : gn_getVisibleOrg($cfs, opts);
                            nI = gn_getScrollItemsNextFilter(a_itm, opts, 0, xI);
                        }
                        else
                        {
                            nI = opts.items.visible;
                        }
                        nI = cf_getAdjust(nI, opts, sO.items, $tt0);
                    }

                    var lastItemNr = (itms.first == 0) ? itms.total : itms.first;

                    //  prevent non-circular from scrolling to far
                    if (!opts.circular)
                    {
                        if (opts.items.visibleConf.variable)
                        {
                            var vI = gn_getVisibleItemsNext(a_itm, opts, nI),
                                xI = gn_getVisibleItemsPrev(a_itm, opts, lastItemNr-1);
                        }
                        else
                        {
                            var vI = opts.items.visible,
                                xI = opts.items.visible;
                        }

                        if (nI + vI > lastItemNr)
                        {
                            nI = lastItemNr - xI;
                        }
                    }

                    //  set new number of visible items
                    opts.items.visibleConf.old = opts.items.visible;
                    if (opts.items.visibleConf.variable)
                    {
                        var vI = cf_getItemsAdjust(gn_getVisibleItemsNextTestCircular(a_itm, opts, nI, lastItemNr), opts, opts.items.visibleConf.adjust, $tt0);
                        while (opts.items.visible-nI >= vI && nI < itms.total)
                        {
                            nI++;
                            vI = cf_getItemsAdjust(gn_getVisibleItemsNextTestCircular(a_itm, opts, nI, lastItemNr), opts, opts.items.visibleConf.adjust, $tt0);
                        }
                        opts.items.visible = vI;
                    }
                    else if (opts.items.filter != '*')
                    {
                        var vI = gn_getVisibleItemsNextFilter(a_itm, opts, nI);
                        opts.items.visible = cf_getItemsAdjust(vI, opts, opts.items.visibleConf.adjust, $tt0);
                    }

                    sz_resetMargin(a_itm, opts, true);

                    //  scroll 0, don't scroll
                    if (nI == 0)
                    {
                        e.stopImmediatePropagation();
                        return debug(conf, '0 items to scroll: Not scrolling.');
                    }
                    debug(conf, 'Scrolling '+nI+' items forward.');


                    //  save new config
                    itms.first -= nI;
                    while (itms.first < 0)
                    {
                        itms.first += itms.total;
                    }

                    //  non-circular callback
                    if (!opts.circular)
                    {
                        if (itms.first == opts.items.visible && sO.onEnd)
                        {
                            sO.onEnd.call($tt0, 'next');
                        }
                        if (!opts.infinite)
                        {
                            nv_enableNavi(opts, itms.first, conf);
                        }
                    }

                    //  rearrange items
                    if (itms.total < opts.items.visible+nI)
                    {
                        $cfs.children().slice(0, (opts.items.visible+nI)-itms.total).clone(true).appendTo($cfs);
                    }

                    //  the needed items
                    var a_itm = $cfs.children(),
                        i_old = gi_getOldItemsNext(a_itm, opts),
                        i_new = gi_getNewItemsNext(a_itm, opts, nI),
                        i_cur_l = a_itm.eq(nI-1),
                        i_old_l = i_old.last(),
                        i_new_l = i_new.last();

                    sz_resetMargin(a_itm, opts);

                    var pL = 0,
                        pR = 0;

                    if (opts.align)
                    {
                        var p = cf_getAlignPadding(i_new, opts);
                        pL = p[0];
                        pR = p[1];
                    }

                    //  hide items for fx directscroll
                    var hiddenitems = false,
                        i_skp = $();
                    if (opts.items.visibleConf.old < nI)
                    {
                        i_skp = a_itm.slice(opts.items.visibleConf.old, nI);
                        if (sO.fx == 'directscroll')
                        {
                            var orgW = opts.items[opts.d['width']];
                            hiddenitems = i_skp;
                            i_cur_l = i_old_l;
                            sc_hideHiddenItems(hiddenitems);
                            opts.items[opts.d['width']] = 'variable';
                        }
                    }

                    //  save new sizes
                    var $cf2 = false,
                        i_siz = ms_getTotalSize(a_itm.slice(0, nI), opts, 'width'),
                        w_siz = cf_mapWrapperSizes(ms_getSizes(i_new, opts, true), opts, !opts.usePadding),
                        i_siz_vis = 0,
                        a_cfs = {},
                        a_cfs_vis = {},
                        a_cur = {},
                        a_old = {},
                        a_lef = {},
                        a_dur = sc_getDuration(sO, opts, nI, i_siz);

                    switch(sO.fx)
                    {
                        case 'uncover':
                        case 'uncover-fade':
                            i_siz_vis = ms_getTotalSize(a_itm.slice(0, opts.items.visibleConf.old), opts, 'width');
                            break;
                    }

                    if (hiddenitems)
                    {
                        opts.items[opts.d['width']] = orgW;
                    }

                    if (opts.align)
                    {
                        if (opts.padding[opts.d[1]] < 0)
                        {
                            opts.padding[opts.d[1]] = 0;
                        }
                    }
                    sz_resetMargin(a_itm, opts, true);
                    sz_resetMargin(i_old_l, opts, opts.padding[opts.d[1]]);

                    if (opts.align)
                    {
                        opts.padding[opts.d[1]] = pR;
                        opts.padding[opts.d[3]] = pL;
                    }

                    a_lef[opts.d['left']] = (opts.usePadding) ? opts.padding[opts.d[3]] : 0;

                    //  scrolling functions
                    var _s_wrapper = function() {},
                        _a_wrapper = function() {},
                        _s_paddingold = function() {},
                        _a_paddingold = function() {},
                        _s_paddingcur = function() {},
                        _a_paddingcur = function() {},
                        _onafter = function() {},
                        _moveitems = function() {},
                        _position = function() {};

                    //  clone carousel
                    switch(sO.fx)
                    {
                        case 'crossfade':
                        case 'cover':
                        case 'cover-fade':
                        case 'uncover':
                        case 'uncover-fade':
                            $cf2 = $cfs.clone(true).appendTo($wrp);
                            $cf2.children().slice(opts.items.visibleConf.old).remove();
                            break;
                    }
                    switch(sO.fx)
                    {
                        case 'crossfade':
                        case 'cover':
                        case 'cover-fade':
                            $cfs.css('zIndex', 1);
                            $cf2.css('zIndex', 0);
                            break;
                    }

                    //  reset all scrolls
                    scrl = sc_setScroll(a_dur, sO.easing, conf);

                    //  animate / set carousel
                    a_cfs[opts.d['left']] = -i_siz;
                    a_cfs_vis[opts.d['left']] = -i_siz_vis;

                    if (pL < 0)
                    {
                        a_cfs[opts.d['left']] += pL;
                    }

                    //  animate / set wrapper
                    if (opts[opts.d['width']] == 'variable' || opts[opts.d['height']] == 'variable')
                    {
                        _s_wrapper = function() {
                            $wrp.css(w_siz);
                        };
                        _a_wrapper = function() {
                            scrl.anims.push([$wrp, w_siz]);
                        };
                    }

                    //  animate / set items
                    if (opts.usePadding)
                    {
                        var i_new_l_m = i_new_l.data('_cfs_origCssMargin');

                        if (pR >= 0)
                        {
                            i_new_l_m += opts.padding[opts.d[1]];
                        }
                        i_new_l.css(opts.d['marginRight'], i_new_l_m);

                        if (i_cur_l.not(i_old_l).length)
                        {
                            a_old[opts.d['marginRight']] = i_old_l.data('_cfs_origCssMargin');
                        }
                        _s_paddingold = function() {
                            i_old_l.css(a_old);
                        };
                        _a_paddingold = function() {
                            scrl.anims.push([i_old_l, a_old]);
                        };

                        var i_cur_l_m = i_cur_l.data('_cfs_origCssMargin');
                        if (pL > 0)
                        {
                            i_cur_l_m += opts.padding[opts.d[3]];
                        }

                        a_cur[opts.d['marginRight']] = i_cur_l_m;

                        _s_paddingcur = function() {
                            i_cur_l.css(a_cur);
                        };
                        _a_paddingcur = function() {
                            scrl.anims.push([i_cur_l, a_cur]);
                        };
                    }

                    //  set position
                    _position = function() {
                        $cfs.css(a_lef);
                    };


                    var overFill = opts.items.visible+nI-itms.total;

                    //  rearrange items
                    _moveitems = function() {
                        if (overFill > 0)
                        {
                            $cfs.children().slice(itms.total).remove();
                        }
                        var l_itm = $cfs.children().slice(0, nI).appendTo($cfs).last();
                        if (overFill > 0)
                        {
                            i_new = gi_getCurrentItems(a_itm, opts);
                        }
                        sc_showHiddenItems(hiddenitems);

                        if (opts.usePadding)
                        {
                            if (itms.total < opts.items.visible+nI) {
                                var i_cur_l = $cfs.children().eq(opts.items.visible-1);
                                i_cur_l.css(opts.d['marginRight'], i_cur_l.data('_cfs_origCssMargin') + opts.padding[opts.d[1]]);
                            }
                            l_itm.css(opts.d['marginRight'], l_itm.data('_cfs_origCssMargin'));
                        }
                    };


                    var cb_arguments = sc_mapCallbackArguments(i_old, i_skp, i_new, nI, 'next', a_dur, w_siz);

                    //  fire onAfter callbacks
                    _onafter = function() {
                        $cfs.css('zIndex', $cfs.data('_cfs_origCssZindex'));
                        sc_afterScroll($cfs, $cf2, sO);
                        crsl.isScrolling = false;
                        clbk.onAfter = sc_fireCallbacks($tt0, sO, 'onAfter', cb_arguments, clbk);
                        queu = sc_fireQueue($cfs, queu, conf);
                        
                        if (!crsl.isPaused)
                        {
                            $cfs.trigger(cf_e('play', conf));
                        }
                    };

                    //  fire onBefore callbacks
                    crsl.isScrolling = true;
                    tmrs = sc_clearTimers(tmrs);
                    clbk.onBefore = sc_fireCallbacks($tt0, sO, 'onBefore', cb_arguments, clbk);

                    switch(sO.fx)
                    {
                        case 'none':
                            $cfs.css(a_cfs);
                            _s_wrapper();
                            _s_paddingold();
                            _s_paddingcur();
                            _position();
                            _moveitems();
                            _onafter();
                            break;

                        case 'fade':
                            scrl.anims.push([$cfs, { 'opacity': 0 }, function() {
                                _s_wrapper();
                                _s_paddingold();
                                _s_paddingcur();
                                _position();
                                _moveitems();
                                scrl = sc_setScroll(a_dur, sO.easing, conf);
                                scrl.anims.push([$cfs, { 'opacity': 1 }, _onafter]);
                                sc_startScroll(scrl, conf);
                            }]);
                            break;

                        case 'crossfade':
                            $cfs.css({ 'opacity': 0 });
                            scrl.anims.push([$cf2, { 'opacity': 0 }]);
                            scrl.anims.push([$cfs, { 'opacity': 1 }, _onafter]);
                            _a_wrapper();
                            _s_paddingold();
                            _s_paddingcur();
                            _position();
                            _moveitems();
                            break;

                        case 'cover':
                            $cfs.css(opts.d['left'], $wrp[opts.d['width']]());
                            scrl.anims.push([$cfs, a_lef, _onafter]);
                            _a_wrapper();
                            _s_paddingold();
                            _s_paddingcur();
                            _moveitems();
                            break;

                        case 'cover-fade':
                            $cfs.css(opts.d['left'], $wrp[opts.d['width']]());
                            scrl.anims.push([$cf2, { 'opacity': 0 }]);
                            scrl.anims.push([$cfs, a_lef, _onafter]);
                            _a_wrapper();
                            _s_paddingold();
                            _s_paddingcur();
                            _moveitems();
                            break;

                        case 'uncover':
                            scrl.anims.push([$cf2, a_cfs_vis, _onafter]);
                            _a_wrapper();
                            _s_paddingold();
                            _s_paddingcur();
                            _position();
                            _moveitems();
                            break;

                        case 'uncover-fade':
                            $cfs.css({ 'opacity': 0 });
                            scrl.anims.push([$cfs, { 'opacity': 1 }]);
                            scrl.anims.push([$cf2, a_cfs_vis, _onafter]);
                            _a_wrapper();
                            _s_paddingold();
                            _s_paddingcur();
                            _position();
                            _moveitems();
                            break;

                        default:
                            scrl.anims.push([$cfs, a_cfs, function() {
                                _position();
                                _moveitems();
                                _onafter();
                            }]);
                            _a_wrapper();
                            _a_paddingold();
                            _a_paddingcur();
                            break;
                    }

                    sc_startScroll(scrl, conf);
                    cf_setCookie(opts.cookie, $cfs, conf);

                    $cfs.trigger(cf_e('updatePageStatus', conf), [false, w_siz]);

                    return true;
                });


                //  slideTo event
                $cfs.bind(cf_e('slideTo', conf), function(e, num, dev, org, obj, dir, clb) {
                    e.stopPropagation();

                    var v = [num, dev, org, obj, dir, clb],
                        t = ['string/number/object', 'number', 'boolean', 'object', 'string', 'function'],
                        a = cf_sortParams(v, t);

                    obj = a[3];
                    dir = a[4];
                    clb = a[5];

                    num = gn_getItemIndex(a[0], a[1], a[2], itms, $cfs);

                    if (num == 0)
                    {
                        return false;
                    }
                    if (!is_object(obj))
                    {
                        obj = false;
                    }

                    if (dir != 'prev' && dir != 'next')
                    {
                        if (opts.circular)
                        {
                            dir = (num <= itms.total / 2) ? 'next' : 'prev';
                        }
                        else
                        {
                            dir = (itms.first == 0 || itms.first > num) ? 'next' : 'prev';
                        }
                    }

                    if (dir == 'prev')
                    {
                        num = itms.total-num;
                    }
                    $cfs.trigger(cf_e(dir, conf), [obj, num, clb]);

                    return true;
                });


                //  prevPage event
                $cfs.bind(cf_e('prevPage', conf), function(e, obj, clb) {
                    e.stopPropagation();
                    var cur = $cfs.triggerHandler(cf_e('currentPage', conf));
                    return $cfs.triggerHandler(cf_e('slideToPage', conf), [cur-1, obj, 'prev', clb]);
                });


                //  nextPage event
                $cfs.bind(cf_e('nextPage', conf), function(e, obj, clb) {
                    e.stopPropagation();
                    var cur = $cfs.triggerHandler(cf_e('currentPage', conf));
                    return $cfs.triggerHandler(cf_e('slideToPage', conf), [cur+1, obj, 'next', clb]);
                });


                //  slideToPage event
                $cfs.bind(cf_e('slideToPage', conf), function(e, pag, obj, dir, clb) {
                    e.stopPropagation();
                    if (!is_number(pag))
                    {
                        pag = $cfs.triggerHandler(cf_e('currentPage', conf));
                    }
                    var ipp = opts.pagination.items || opts.items.visible,
                        max = Math.ceil(itms.total / ipp)-1;

                    if (pag < 0)
                    {
                        pag = max;
                    }
                    if (pag > max)
                    {
                        pag = 0;
                    }
                    return $cfs.triggerHandler(cf_e('slideTo', conf), [pag*ipp, 0, true, obj, dir, clb]);
                });

                //  jumpToStart event
                $cfs.bind(cf_e('jumpToStart', conf), function(e, s) {
                    e.stopPropagation();
                    if (s)
                    {
                        s = gn_getItemIndex(s, 0, true, itms, $cfs);
                    }
                    else
                    {
                        s = 0;
                    }

                    s += itms.first;
                    if (s != 0)
                    {
                        if (itms.total > 0)
                        {
                            while (s > itms.total)
                            {
                                s -= itms.total;
                            }
                        }
                        $cfs.prepend($cfs.children().slice(s, itms.total));
                    }
                    return true;
                });


                //  synchronise event
                $cfs.bind(cf_e('synchronise', conf), function(e, s) {
                    e.stopPropagation();
                    if (s)
                    {
                        s = cf_getSynchArr(s);
                    }
                    else if (opts.synchronise)
                    {
                        s = opts.synchronise;
                    }
                    else
                    {
                        return debug(conf, 'No carousel to synchronise.');
                    }

                    var n = $cfs.triggerHandler(cf_e('currentPosition', conf)),
                        x = true;

                    for (var j = 0, l = s.length; j < l; j++)
                    {
                        if (!s[j][0].triggerHandler(cf_e('slideTo', conf), [n, s[j][3], true]))
                        {
                            x = false;
                        }
                    }
                    return x;
                });


                //  queue event
                $cfs.bind(cf_e('queue', conf), function(e, dir, opt) {
                    e.stopPropagation();
                    if (is_function(dir))
                    {
                        dir.call($tt0, queu);
                    }
                    else if (is_array(dir))
                    {
                        queu = dir;
                    }
                    else if (!is_undefined(dir))
                    {
                        queu.push([dir, opt]);
                    }
                    return queu;
                });


                //  insertItem event
                $cfs.bind(cf_e('insertItem', conf), function(e, itm, num, org, dev) {
                    e.stopPropagation();

                    var v = [itm, num, org, dev],
                        t = ['string/object', 'string/number/object', 'boolean', 'number'],
                        a = cf_sortParams(v, t);

                    itm = a[0];
                    num = a[1];
                    org = a[2];
                    dev = a[3];

                    if (is_object(itm) && !is_jquery(itm))
                    { 
                        itm = $(itm);
                    }
                    else if (is_string(itm))
                    {
                        itm = $(itm);
                    }
                    if (!is_jquery(itm) || itm.length == 0)
                    {
                        return debug(conf, 'Not a valid object.');
                    }

                    if (is_undefined(num))
                    {
                        num = 'end';
                    }

                    sz_storeMargin(itm, opts);
                    sz_storeOrigCss(itm);

                    var orgNum = num,
                        before = 'before';

                    if (num == 'end')
                    {
                        if (org)
                        {
                            if (itms.first == 0)
                            {
                                num = itms.total-1;
                                before = 'after';
                            }
                            else
                            {
                                num = itms.first;
                                itms.first += itm.length;
                            }
                            if (num < 0)
                            {
                                num = 0;
                            }
                        }
                        else
                        {
                            num = itms.total-1;
                            before = 'after';
                        }
                    }
                    else
                    {
                        num = gn_getItemIndex(num, dev, org, itms, $cfs);
                    }

                    var $cit = $cfs.children().eq(num);
                    if ($cit.length)
                    {
                        $cit[before](itm);
                    }
                    else
                    {
                        debug(conf, 'Correct insert-position not found! Appending item to the end.');
                        $cfs.append(itm);
                    }

                    if (orgNum != 'end' && !org)
                    {
                        if (num < itms.first)
                        {
                            itms.first += itm.length;
                        }
                    }
                    itms.total = $cfs.children().length;
                    if (itms.first >= itms.total)
                    {
                        itms.first -= itms.total;
                    }

                    $cfs.trigger(cf_e('updateSizes', conf));
                    $cfs.trigger(cf_e('linkAnchors', conf));

                    return true;
                });


                //  removeItem event
                $cfs.bind(cf_e('removeItem', conf), function(e, num, org, dev) {
                    e.stopPropagation();

                    var v = [num, org, dev],
                        t = ['string/number/object', 'boolean', 'number'],
                        a = cf_sortParams(v, t);

                    num = a[0];
                    org = a[1];
                    dev = a[2];

                    var removed = false;

                    if (num instanceof $ && num.length > 1)
                    {
                        $removed = $();
                        num.each(function(i, el) {
                            var $rem = $cfs.trigger(cf_e('removeItem', conf), [$(this), org, dev]);
                            if ( $rem ) 
                            {
                                $removed = $removed.add($rem);
                            }
                        });
                        return $removed;
                    }

                    if (is_undefined(num) || num == 'end')
                    {
                        $removed = $cfs.children().last();
                    }
                    else
                    {
                        num = gn_getItemIndex(num, dev, org, itms, $cfs);
                        var $removed = $cfs.children().eq(num);
                        if ( $removed.length )
                        {
                            if (num < itms.first)
                            {
                                itms.first -= $removed.length;
                            }
                        }
                    }
                    if ( $removed && $removed.length )
                    {
                        $removed.detach();
                        itms.total = $cfs.children().length;
                        $cfs.trigger(cf_e('updateSizes', conf));
                    }

                    return $removed;
                });


                //  onBefore and onAfter event
                $cfs.bind(cf_e('onBefore', conf)+' '+cf_e('onAfter', conf), function(e, fn) {
                    e.stopPropagation();
                    var eType = e.type.slice(conf.events.prefix.length);
                    if (is_array(fn))
                    {
                        clbk[eType] = fn;
                    }
                    if (is_function(fn))
                    {
                        clbk[eType].push(fn);
                    }
                    return clbk[eType];
                });


                //  currentPosition event
                $cfs.bind(cf_e('currentPosition', conf), function(e, fn) {
                    e.stopPropagation();
                    if (itms.first == 0)
                    {
                        var val = 0;
                    }
                    else
                    {
                        var val = itms.total - itms.first;
                    }
                    if (is_function(fn))
                    {
                        fn.call($tt0, val);
                    }
                    return val;
                });


                //  currentPage event
                $cfs.bind(cf_e('currentPage', conf), function(e, fn) {
                    e.stopPropagation();
                    var ipp = opts.pagination.items || opts.items.visible,
                        max = Math.ceil(itms.total/ipp-1),
                        nr;
                    if (itms.first == 0)
                    {
                        nr = 0;
                    }
                    else if (itms.first < itms.total % ipp)
                    {
                        nr = 0;
                    }
                    else if (itms.first == ipp && !opts.circular)
                    {
                        nr = max;
                    }
                    else 
                    {
                         nr = Math.round((itms.total-itms.first)/ipp);
                    }
                    if (nr < 0)
                    {
                        nr = 0;
                    }
                    if (nr > max)
                    {
                        nr = max;
                    }
                    if (is_function(fn))
                    {
                        fn.call($tt0, nr);
                    }
                    return nr;
                });


                //  currentVisible event
                $cfs.bind(cf_e('currentVisible', conf), function(e, fn) {
                    e.stopPropagation();
                    var $i = gi_getCurrentItems($cfs.children(), opts);
                    if (is_function(fn))
                    {
                        fn.call($tt0, $i);
                    }
                    return $i;
                });


                //  slice event
                $cfs.bind(cf_e('slice', conf), function(e, f, l, fn) {
                    e.stopPropagation();

                    if (itms.total == 0)
                    {
                        return false;
                    }

                    var v = [f, l, fn],
                        t = ['number', 'number', 'function'],
                        a = cf_sortParams(v, t);

                    f = (is_number(a[0])) ? a[0] : 0;
                    l = (is_number(a[1])) ? a[1] : itms.total;
                    fn = a[2];

                    f += itms.first;
                    l += itms.first;

                    if (items.total > 0)
                    {
                        while (f > itms.total)
                        {
                            f -= itms.total;
                        }
                        while (l > itms.total)
                        {
                            l -= itms.total;
                        }
                        while (f < 0)
                        {
                            f += itms.total;
                        }
                        while (l < 0)
                        {
                            l += itms.total;
                        }
                    }
                    var $iA = $cfs.children(),
                        $i;

                    if (l > f)
                    {
                        $i = $iA.slice(f, l);
                    }
                    else
                    {
                        $i = $( $iA.slice(f, itms.total).get().concat( $iA.slice(0, l).get() ) );
                    }

                    if (is_function(fn))
                    {
                        fn.call($tt0, $i);
                    }
                    return $i;
                });


                //  isPaused, isStopped and isScrolling events
                $cfs.bind(cf_e('isPaused', conf)+' '+cf_e('isStopped', conf)+' '+cf_e('isScrolling', conf), function(e, fn) {
                    e.stopPropagation();
                    var eType = e.type.slice(conf.events.prefix.length),
                        value = crsl[eType];
                    if (is_function(fn))
                    {
                        fn.call($tt0, value);
                    }
                    return value;
                });


                //  configuration event
                $cfs.bind(cf_e('configuration', conf), function(e, a, b, c) {
                    e.stopPropagation();
                    var reInit = false;

                    //  return entire configuration-object
                    if (is_function(a))
                    {
                        a.call($tt0, opts);
                    }
                    //  set multiple options via object
                    else if (is_object(a))
                    {
                        opts_orig = $.extend(true, {}, opts_orig, a);
                        if (b !== false) reInit = true;
                        else opts = $.extend(true, {}, opts, a);

                    }
                    else if (!is_undefined(a))
                    {

                        //  callback function for specific option
                        if (is_function(b))
                        {
                            var val = eval('opts.'+a);
                            if (is_undefined(val))
                            {
                                val = '';
                            }
                            b.call($tt0, val);
                        }
                        //  set individual option
                        else if (!is_undefined(b))
                        {
                            if (typeof c !== 'boolean') c = true;
                            eval('opts_orig.'+a+' = b');
                            if (c !== false) reInit = true;
                            else eval('opts.'+a+' = b');
                        }
                        //  return value for specific option
                        else
                        {
                            return eval('opts.'+a);
                        }
                    }
                    if (reInit)
                    {
                        sz_resetMargin($cfs.children(), opts);
                        FN._init(opts_orig);
                        FN._bind_buttons();
                        var sz = sz_setSizes($cfs, opts);
                        $cfs.trigger(cf_e('updatePageStatus', conf), [true, sz]);
                    }
                    return opts;
                });


                //  linkAnchors event
                $cfs.bind(cf_e('linkAnchors', conf), function(e, $con, sel) {
                    e.stopPropagation();

                    if (is_undefined($con))
                    {
                        $con = $('body');
                    }
                    else if (is_string($con))
                    {
                        $con = $($con);
                    }
                    if (!is_jquery($con) || $con.length == 0)
                    {
                        return debug(conf, 'Not a valid object.');
                    }
                    if (!is_string(sel))
                    {
                        sel = 'a.caroufredsel';
                    }

                    $con.find(sel).each(function() {
                        var h = this.hash || '';
                        if (h.length > 0 && $cfs.children().index($(h)) != -1)
                        {
                            $(this).unbind('click').click(function(e) {
                                e.preventDefault();
                                $cfs.trigger(cf_e('slideTo', conf), h);
                            });
                        }
                    });
                    return true;
                });


                //  updatePageStatus event
                $cfs.bind(cf_e('updatePageStatus', conf), function(e, build, sizes) {
                    e.stopPropagation();
                    if (!opts.pagination.container)
                    {
                        return;
                    }

                    var ipp = opts.pagination.items || opts.items.visible,
                        pgs = Math.ceil(itms.total/ipp);

                    if (build)
                    {
                        if (opts.pagination.anchorBuilder)
                        {
                            opts.pagination.container.children().remove();
                            opts.pagination.container.each(function() {
                                for (var a = 0; a < pgs; a++)
                                {
                                    var i = $cfs.children().eq( gn_getItemIndex(a*ipp, 0, true, itms, $cfs) );
                                    $(this).append(opts.pagination.anchorBuilder.call(i[0], a+1));
                                }
                            });
                        }
                        opts.pagination.container.each(function() {
                            $(this).children().unbind(opts.pagination.event).each(function(a) {
                                $(this).bind(opts.pagination.event, function(e) {
                                    e.preventDefault();
                                    $cfs.trigger(cf_e('slideTo', conf), [a*ipp, -opts.pagination.deviation, true, opts.pagination]);
                                });
                            });
                        });
                    }

                    var selected = $cfs.triggerHandler(cf_e('currentPage', conf)) + opts.pagination.deviation;
                    if (selected >= pgs)
                    {
                        selected = 0;
                    }
                    if (selected < 0)
                    {
                        selected = pgs-1;
                    }
                    opts.pagination.container.each(function() {
                        $(this).children().removeClass(cf_c('selected', conf)).eq(selected).addClass(cf_c('selected', conf));
                    });
                    return true;
                });


                //  updateSizes event
                $cfs.bind(cf_e('updateSizes', conf), function(e) {
                    var vI = opts.items.visible,
                        a_itm = $cfs.children(),
                        avail_primary = ms_getParentSize($wrp, opts, 'width');

                    itms.total = a_itm.length;

                    if (crsl.primarySizePercentage)
                    {
                        opts.maxDimension = avail_primary;
                        opts[opts.d['width']] = ms_getPercentage(avail_primary, crsl.primarySizePercentage);
                    }
                    else
                    {
                        opts.maxDimension = ms_getMaxDimension(opts, avail_primary);
                    }

                    if (opts.responsive)
                    {
                        opts.items.width = opts.items.sizesConf.width;
                        opts.items.height = opts.items.sizesConf.height;
                        opts = in_getResponsiveValues(opts, a_itm, avail_primary);
                        vI = opts.items.visible;
                        sz_setResponsiveSizes(opts, a_itm);
                    }
                    else if (opts.items.visibleConf.variable)
                    {
                        vI = gn_getVisibleItemsNext(a_itm, opts, 0);
                    }
                    else if (opts.items.filter != '*')
                    {
                        vI = gn_getVisibleItemsNextFilter(a_itm, opts, 0);
                    }

                    if (!opts.circular && itms.first != 0 && vI > itms.first) {
                        if (opts.items.visibleConf.variable)
                        {
                            var nI = gn_getVisibleItemsPrev(a_itm, opts, itms.first) - itms.first;
                        }
                        else if (opts.items.filter != '*')
                        {
                            var nI = gn_getVisibleItemsPrevFilter(a_itm, opts, itms.first) - itms.first;
                        }
                        else
                        {
                            var nI = opts.items.visible - itms.first;
                        }
                        debug(conf, 'Preventing non-circular: sliding '+nI+' items backward.');
                        $cfs.trigger(cf_e('prev', conf), nI);
                    }

                    opts.items.visible = cf_getItemsAdjust(vI, opts, opts.items.visibleConf.adjust, $tt0);
                    opts.items.visibleConf.old = opts.items.visible;
                    opts = in_getAlignPadding(opts, a_itm);

                    var sz = sz_setSizes($cfs, opts);
                    $cfs.trigger(cf_e('updatePageStatus', conf), [true, sz]);
                    nv_showNavi(opts, itms.total, conf);
                    nv_enableNavi(opts, itms.first, conf);

                    return sz;
                });


                //  destroy event
                $cfs.bind(cf_e('destroy', conf), function(e, orgOrder) {
                    e.stopPropagation();
                    tmrs = sc_clearTimers(tmrs);

                    $cfs.data('_cfs_isCarousel', false);
                    $cfs.trigger(cf_e('finish', conf));
                    if (orgOrder)
                    {
                        $cfs.trigger(cf_e('jumpToStart', conf));
                    }
                    sz_restoreOrigCss($cfs.children());
                    sz_restoreOrigCss($cfs);
                    FN._unbind_events();
                    FN._unbind_buttons();
                    if (conf.wrapper == 'parent')
                    {
                        sz_restoreOrigCss($wrp);
                    }
                    else
                    {
                        $wrp.replaceWith($cfs);
                    }

                    return true;
                });


                //  debug event
                $cfs.bind(cf_e('debug', conf), function(e) {
                    debug(conf, 'Carousel width: ' + opts.width);
                    debug(conf, 'Carousel height: ' + opts.height);
                    debug(conf, 'Item widths: ' + opts.items.width);
                    debug(conf, 'Item heights: ' + opts.items.height);
                    debug(conf, 'Number of items visible: ' + opts.items.visible);
                    if (opts.auto.play)
                    {
                        debug(conf, 'Number of items scrolled automatically: ' + opts.auto.items);
                    }
                    if (opts.prev.button)
                    {
                        debug(conf, 'Number of items scrolled backward: ' + opts.prev.items);
                    }
                    if (opts.next.button)
                    {
                        debug(conf, 'Number of items scrolled forward: ' + opts.next.items);
                    }
                    return conf.debug;
                });


                //  triggerEvent, making prefixed and namespaced events accessible from outside
                $cfs.bind('_cfs_triggerEvent', function(e, n, o) {
                    e.stopPropagation();
                    return $cfs.triggerHandler(cf_e(n, conf), o);
                });
            };  //  /bind_events


            FN._unbind_events = function() {
                $cfs.unbind(cf_e('', conf));
                $cfs.unbind(cf_e('', conf, false));
                $cfs.unbind('_cfs_triggerEvent');
            };  //  /unbind_events


            FN._bind_buttons = function() {
                FN._unbind_buttons();
                nv_showNavi(opts, itms.total, conf);
                nv_enableNavi(opts, itms.first, conf);

                if (opts.auto.pauseOnHover)
                {
                    var pC = bt_pauseOnHoverConfig(opts.auto.pauseOnHover);
                    $wrp.bind(cf_e('mouseenter', conf, false), function() { $cfs.trigger(cf_e('pause', conf), pC);  })
                        .bind(cf_e('mouseleave', conf, false), function() { $cfs.trigger(cf_e('resume', conf));     });
                }

                //  play button
                if (opts.auto.button)
                {
                    opts.auto.button.bind(cf_e(opts.auto.event, conf, false), function(e) {
                        e.preventDefault();
                        var ev = false,
                            pC = null;

                        if (crsl.isPaused)
                        {
                            ev = 'play';
                        }
                        else if (opts.auto.pauseOnEvent)
                        {
                            ev = 'pause';
                            pC = bt_pauseOnHoverConfig(opts.auto.pauseOnEvent);
                        }
                        if (ev)
                        {
                            $cfs.trigger(cf_e(ev, conf), pC);
                        }
                    });
                }

                //  prev button
                if (opts.prev.button)
                {
                    opts.prev.button.bind(cf_e(opts.prev.event, conf, false), function(e) {
                        e.preventDefault();
                        $cfs.trigger(cf_e('prev', conf));
                    });
                    if (opts.prev.pauseOnHover)
                    {
                        var pC = bt_pauseOnHoverConfig(opts.prev.pauseOnHover);
                        opts.prev.button.bind(cf_e('mouseenter', conf, false), function() { $cfs.trigger(cf_e('pause', conf), pC);  })
                                        .bind(cf_e('mouseleave', conf, false), function() { $cfs.trigger(cf_e('resume', conf));     });
                    }
                }

                //  next butotn
                if (opts.next.button)
                {
                    opts.next.button.bind(cf_e(opts.next.event, conf, false), function(e) {
                        e.preventDefault();
                        $cfs.trigger(cf_e('next', conf));
                    });
                    if (opts.next.pauseOnHover)
                    {
                        var pC = bt_pauseOnHoverConfig(opts.next.pauseOnHover);
                        opts.next.button.bind(cf_e('mouseenter', conf, false), function() { $cfs.trigger(cf_e('pause', conf), pC);  })
                                        .bind(cf_e('mouseleave', conf, false), function() { $cfs.trigger(cf_e('resume', conf));     });
                    }
                }

                //  pagination
                if (opts.pagination.container)
                {
                    if (opts.pagination.pauseOnHover)
                    {
                        var pC = bt_pauseOnHoverConfig(opts.pagination.pauseOnHover);
                        opts.pagination.container.bind(cf_e('mouseenter', conf, false), function() { $cfs.trigger(cf_e('pause', conf), pC); })
                                                 .bind(cf_e('mouseleave', conf, false), function() { $cfs.trigger(cf_e('resume', conf));    });
                    }
                }

                //  prev/next keys
                if (opts.prev.key || opts.next.key)
                {
                    $(document).bind(cf_e('keyup', conf, false, true, true), function(e) {
                        var k = e.keyCode;
                        if (k == opts.next.key)
                        {
                            e.preventDefault();
                            $cfs.trigger(cf_e('next', conf));
                        }
                        if (k == opts.prev.key)
                        {
                            e.preventDefault();
                            $cfs.trigger(cf_e('prev', conf));
                        }
                    });
                }

                //  pagination keys
                if (opts.pagination.keys)
                {
                    $(document).bind(cf_e('keyup', conf, false, true, true), function(e) {
                        var k = e.keyCode;
                        if (k >= 49 && k < 58)
                        {
                            k = (k-49) * opts.items.visible;
                            if (k <= itms.total)
                            {
                                e.preventDefault();
                                $cfs.trigger(cf_e('slideTo', conf), [k, 0, true, opts.pagination]);
                            }
                        }
                    });
                }

                //  swipe
                if ($.fn.swipe)
                {
                    var isTouch = 'ontouchstart' in window;
                    if ((isTouch && opts.swipe.onTouch) || (!isTouch && opts.swipe.onMouse))
                    {
                        var scP = $.extend(true, {}, opts.prev, opts.swipe),
                            scN = $.extend(true, {}, opts.next, opts.swipe),
                            swP = function() { $cfs.trigger(cf_e('prev', conf), [scP]) },
                            swN = function() { $cfs.trigger(cf_e('next', conf), [scN]) };

                        switch (opts.direction)
                        {
                            case 'up':
                            case 'down':
                                opts.swipe.options.swipeUp = swN;
                                opts.swipe.options.swipeDown = swP;
                                break;
                            default:
                                opts.swipe.options.swipeLeft = swN;
                                opts.swipe.options.swipeRight = swP;
                        }
                        if (crsl.swipe)
                        {
                            $cfs.swipe('destroy');
                        }
                        $wrp.swipe(opts.swipe.options);
                        $wrp.css('cursor', 'move');
                        crsl.swipe = true;
                    }
                }

                //  mousewheel
                if ($.fn.mousewheel)
                {

                    if (opts.mousewheel)
                    {
                        var mcP = $.extend(true, {}, opts.prev, opts.mousewheel),
                            mcN = $.extend(true, {}, opts.next, opts.mousewheel);

                        if (crsl.mousewheel)
                        {
                            $wrp.unbind(cf_e('mousewheel', conf, false));
                        }
                        $wrp.bind(cf_e('mousewheel', conf, false), function(e, delta) { 
                            e.preventDefault();
                            if (delta > 0)
                            {
                                $cfs.trigger(cf_e('prev', conf), [mcP]);
                            }
                            else
                            {
                                $cfs.trigger(cf_e('next', conf), [mcN]);
                            }
                        });
                        crsl.mousewheel = true;
                    }
                }

                if (opts.auto.play)
                {
                    $cfs.trigger(cf_e('play', conf), opts.auto.delay);
                }

                if (crsl.upDateOnWindowResize)
                {
                    var resizeFn = function(e) {
                        $cfs.trigger(cf_e('finish', conf));
                        if (opts.auto.pauseOnResize && !crsl.isPaused)
                        {
                            $cfs.trigger(cf_e('play', conf));
                        }
                        sz_resetMargin($cfs.children(), opts);
                        $cfs.trigger(cf_e('updateSizes', conf));
                    };

                    var $w = $(window),
                        onResize = null;

                    if ($.debounce && conf.onWindowResize == 'debounce')
                    {
                        onResize = $.debounce(200, resizeFn);
                    }
                    else if ($.throttle && conf.onWindowResize == 'throttle')
                    {
                        onResize = $.throttle(300, resizeFn);
                    }
                    else
                    {
                        var _windowWidth = 0,
                            _windowHeight = 0;

                        onResize = function() {
                            var nw = $w.width(),
                                nh = $w.height();

                            if (nw != _windowWidth || nh != _windowHeight)
                            {
                                resizeFn();
                                _windowWidth = nw;
                                _windowHeight = nh;
                            }
                        };
                    }
                    $w.bind(cf_e('resize', conf, false, true, true), onResize);
                }
            };  //  /bind_buttons


            FN._unbind_buttons = function() {
                var ns1 = cf_e('', conf),
                    ns2 = cf_e('', conf, false);
                    ns3 = cf_e('', conf, false, true, true);

                $(document).unbind(ns3);
                $(window).unbind(ns3);
                $wrp.unbind(ns2);

                if (opts.auto.button)
                {
                    opts.auto.button.unbind(ns2);
                }
                if (opts.prev.button)
                {
                    opts.prev.button.unbind(ns2);
                }
                if (opts.next.button)
                {
                    opts.next.button.unbind(ns2);
                }
                if (opts.pagination.container)
                {
                    opts.pagination.container.unbind(ns2);
                    if (opts.pagination.anchorBuilder)
                    {
                        opts.pagination.container.children().remove();
                    }
                }
                if (crsl.swipe)
                {
                    $cfs.swipe('destroy');
                    $wrp.css('cursor', 'default');
                    crsl.swipe = false;
                }
                if (crsl.mousewheel)
                {
                    crsl.mousewheel = false;
                }

                nv_showNavi(opts, 'hide', conf);
                nv_enableNavi(opts, 'removeClass', conf);

            };  //  /unbind_buttons



            //  START

            if (is_boolean(configs))
            {
                configs = {
                    'debug': configs
                };
            }

            //  set vars
            var crsl = {
                    'direction'     : 'next',
                    'isPaused'      : true,
                    'isScrolling'   : false,
                    'isStopped'     : false,
                    'mousewheel'    : false,
                    'swipe'         : false
                },
                itms = {
                    'total'         : $cfs.children().length,
                    'first'         : 0
                },
                tmrs = {
                    'auto'          : null,
                    'progress'      : null,
                    'startTime'     : getTime(),
                    'timePassed'    : 0
                },
                scrl = {
                    'isStopped'     : false,
                    'duration'      : 0,
                    'startTime'     : 0,
                    'easing'        : '',
                    'anims'         : []
                },
                clbk = {
                    'onBefore'      : [],
                    'onAfter'       : []
                },
                queu = [],
                conf = $.extend(true, {}, $.fn.carouFredSel.configs, configs),
                opts = {},
                opts_orig = $.extend(true, {}, options),
                $wrp = (conf.wrapper == 'parent')
                    ? $cfs.parent()
                    : $cfs.wrap('<'+conf.wrapper.element+' class="'+conf.wrapper.classname+'" />').parent();


            conf.selector       = $cfs.selector;
            conf.serialNumber   = $.fn.carouFredSel.serialNumber++;

            conf.transition = (conf.transition && $.fn.transition) ? 'transition' : 'animate';

            //  create carousel
            FN._init(opts_orig, true, starting_position);
            FN._build();
            FN._bind_events();
            FN._bind_buttons();

            //  find item to start
            if (is_array(opts.items.start))
            {
                var start_arr = opts.items.start;
            }
            else
            {
                var start_arr = [];
                if (opts.items.start != 0)
                {
                    start_arr.push(opts.items.start);
                }
            }
            if (opts.cookie)
            {
                start_arr.unshift(parseInt(cf_getCookie(opts.cookie), 10));
            }

            if (start_arr.length > 0)
            {
                for (var a = 0, l = start_arr.length; a < l; a++)
                {
                    var s = start_arr[a];
                    if (s == 0)
                    {
                        continue;
                    }
                    if (s === true)
                    {
                        s = window.location.hash;
                        if (s.length < 1)
                        {
                            continue;
                        }
                    }
                    else if (s === 'random')
                    {
                        s = Math.floor(Math.random()*itms.total);
                    }
                    if ($cfs.triggerHandler(cf_e('slideTo', conf), [s, 0, true, { fx: 'none' }]))
                    {
                        break;
                    }
                }
            }
            var siz = sz_setSizes($cfs, opts),
                itm = gi_getCurrentItems($cfs.children(), opts);

            if (opts.onCreate)
            {
                opts.onCreate.call($tt0, {
                    'width': siz.width,
                    'height': siz.height,
                    'items': itm
                });
            }

            $cfs.trigger(cf_e('updatePageStatus', conf), [true, siz]);
            $cfs.trigger(cf_e('linkAnchors', conf));

            if (conf.debug)
            {
                $cfs.trigger(cf_e('debug', conf));
            }

            return $cfs;
        };



        //  GLOBAL PUBLIC

        $.fn.carouFredSel.serialNumber = 1;
        $.fn.carouFredSel.defaults = {
            'synchronise'   : false,
            'infinite'      : true,
            'circular'      : true,
            'responsive'    : false,
            'direction'     : 'left',
            'items'         : {
                'start'         : 0
            },
            'scroll'        : {
                'easing'        : 'swing',
                'duration'      : 500,
                'pauseOnHover'  : false,
                'event'         : 'click',
                'queue'         : false
            }
        };
        $.fn.carouFredSel.configs = {
            'debug'         : false,
            'transition'    : false,
            'onWindowResize': 'throttle',
            'events'        : {
                'prefix'        : '',
                'namespace'     : 'cfs'
            },
            'wrapper'       : {
                'element'       : 'div',
                'classname'     : 'caroufredsel_wrapper'
            },
            'classnames'    : {}
        };
        $.fn.carouFredSel.pageAnchorBuilder = function(nr) {
            return '<a href="#"><span>'+nr+'</span></a>';
        };
        $.fn.carouFredSel.progressbarUpdater = function(perc) {
            $(this).css('width', perc+'%');
        };

        $.fn.carouFredSel.cookie = {
            get: function(n) {
                n += '=';
                var ca = document.cookie.split(';');
                for (var a = 0, l = ca.length; a < l; a++)
                {
                    var c = ca[a];
                    while (c.charAt(0) == ' ')
                    {
                        c = c.slice(1);
                    }
                    if (c.indexOf(n) == 0)
                    {
                        return c.slice(n.length);
                    }
                }
                return 0;
            },
            set: function(n, v, d) {
                var e = "";
                if (d)
                {
                    var date = new Date();
                    date.setTime(date.getTime() + (d * 24 * 60 * 60 * 1000));
                    e = "; expires=" + date.toGMTString();
                }
                document.cookie = n + '=' + v + e + '; path=/';
            },
            remove: function(n) {
                $.fn.carouFredSel.cookie.set(n, "", -1);
            }
        };


        //  GLOBAL PRIVATE

        //  scrolling functions
        function sc_setScroll(d, e, c) {
            if (c.transition == 'transition')
            {
                if (e == 'swing')
                {
                    e = 'ease';
                }
            }
            return {
                anims: [],
                duration: d,
                orgDuration: d,
                easing: e,
                startTime: getTime()
            };
        }
        function sc_startScroll(s, c) {
            for (var a = 0, l = s.anims.length; a < l; a++)
            {
                var b = s.anims[a];
                if (!b)
                {
                    continue;
                }
                b[0][c.transition](b[1], s.duration, s.easing, b[2]);
            }
        }
        function sc_stopScroll(s, finish) {
            if (!is_boolean(finish))
            {
                finish = true;
            }
            if (is_object(s.pre))
            {
                sc_stopScroll(s.pre, finish);
            }
            for (var a = 0, l = s.anims.length; a < l; a++)
            {
                var b = s.anims[a];
                b[0].stop(true);
                if (finish)
                {
                    b[0].css(b[1]);
                    if (is_function(b[2]))
                    {
                        b[2]();
                    }
                }
            }
            if (is_object(s.post))
            {
                sc_stopScroll(s.post, finish);
            }
        }
        function sc_afterScroll( $c, $c2, o ) {
            if ($c2)
            {
                $c2.remove();
            }

            switch(o.fx) {
                case 'fade':
                case 'crossfade':
                case 'cover-fade':
                case 'uncover-fade':
                    $c.css('opacity', 1);
                    $c.css('filter', '');
                    break;
            }
        }
        function sc_fireCallbacks($t, o, b, a, c) {
            if (o[b])
            {
                o[b].call($t, a);
            }
            if (c[b].length)
            {
                for (var i = 0, l = c[b].length; i < l; i++)
                {
                    c[b][i].call($t, a);
                }
            }
            return [];
        }
        function sc_fireQueue($c, q, c) {

            if (q.length)
            {
                $c.trigger(cf_e(q[0][0], c), q[0][1]);
                q.shift();
            }
            return q;
        }
        function sc_hideHiddenItems(hiddenitems) {
            hiddenitems.each(function() {
                var hi = $(this);
                hi.data('_cfs_isHidden', hi.is(':hidden')).hide();
            });
        }
        function sc_showHiddenItems(hiddenitems) {
            if (hiddenitems)
            {
                hiddenitems.each(function() {
                    var hi = $(this);
                    if (!hi.data('_cfs_isHidden'))
                    {
                        hi.show();
                    }
                });
            }
        }
        function sc_clearTimers(t) {
            if (t.auto)
            {
                clearTimeout(t.auto);
            }
            if (t.progress)
            {
                clearInterval(t.progress);
            }
            return t;
        }
        function sc_mapCallbackArguments(i_old, i_skp, i_new, s_itm, s_dir, s_dur, w_siz) {
            return {
                'width': w_siz.width,
                'height': w_siz.height,
                'items': {
                    'old': i_old,
                    'skipped': i_skp,
                    'visible': i_new
                },
                'scroll': {
                    'items': s_itm,
                    'direction': s_dir,
                    'duration': s_dur
                }
            };
        }
        function sc_getDuration( sO, o, nI, siz ) {
            var dur = sO.duration;
            if (sO.fx == 'none')
            {
                return 0;
            }
            if (dur == 'auto')
            {
                dur = o.scroll.duration / o.scroll.items * nI;
            }
            else if (dur < 10)
            {
                dur = siz / dur;
            }
            if (dur < 1)
            {
                return 0;
            }
            if (sO.fx == 'fade')
            {
                dur = dur / 2;
            }
            return Math.round(dur);
        }

        //  navigation functions
        function nv_showNavi(o, t, c) {
            var minimum = (is_number(o.items.minimum)) ? o.items.minimum : o.items.visible + 1;
            if (t == 'show' || t == 'hide')
            {
                var f = t;
            }
            else if (minimum > t)
            {
                debug(c, 'Not enough items ('+t+' total, '+minimum+' needed): Hiding navigation.');
                var f = 'hide';
            }
            else
            {
                var f = 'show';
            }
            var s = (f == 'show') ? 'removeClass' : 'addClass',
                h = cf_c('hidden', c);

            if (o.auto.button)
            {
                o.auto.button[f]()[s](h);
            }
            if (o.prev.button)
            {
                o.prev.button[f]()[s](h);
            }
            if (o.next.button)
            {
                o.next.button[f]()[s](h);
            }
            if (o.pagination.container)
            {
                o.pagination.container[f]()[s](h);
            }
        }
        function nv_enableNavi(o, f, c) {
            if (o.circular || o.infinite) return;
            var fx = (f == 'removeClass' || f == 'addClass') ? f : false,
                di = cf_c('disabled', c);

            if (o.auto.button && fx)
            {
                o.auto.button[fx](di);
            }
            if (o.prev.button)
            {
                var fn = fx || (f == 0) ? 'addClass' : 'removeClass';
                o.prev.button[fn](di);
            }
            if (o.next.button)
            {
                var fn = fx || (f == o.items.visible) ? 'addClass' : 'removeClass';
                o.next.button[fn](di);
            }
        }

        //  get object functions
        function go_getObject($tt, obj) {
            if (is_function(obj))
            {
                obj = obj.call($tt);
            }
            else if (is_undefined(obj))
            {
                obj = {};
            }
            return obj;
        }
        function go_getItemsObject($tt, obj) {
            obj = go_getObject($tt, obj);
            if (is_number(obj))
            {
                obj = {
                    'visible': obj
                };
            }
            else if (obj == 'variable')
            {
                obj = {
                    'visible': obj,
                    'width': obj, 
                    'height': obj
                };
            }
            else if (!is_object(obj))
            {
                obj = {};
            }
            return obj;
        }
        function go_getScrollObject($tt, obj) {
            obj = go_getObject($tt, obj);
            if (is_number(obj))
            {
                if (obj <= 50)
                {
                    obj = {
                        'items': obj
                    };
                }
                else
                {
                    obj = {
                        'duration': obj
                    };
                }
            }
            else if (is_string(obj))
            {
                obj = {
                    'easing': obj
                };
            }
            else if (!is_object(obj))
            {
                obj = {};
            }
            return obj;
        }
        function go_getNaviObject($tt, obj) {
            obj = go_getObject($tt, obj);
            if (is_string(obj))
            {
                var temp = cf_getKeyCode(obj);
                if (temp == -1)
                {
                    obj = $(obj);
                }
                else
                {
                    obj = temp;
                }
            }
            return obj;
        }

        function go_getAutoObject($tt, obj) {
            obj = go_getNaviObject($tt, obj);
            if (is_jquery(obj))
            {
                obj = {
                    'button': obj
                };
            }
            else if (is_boolean(obj))
            {
                obj = {
                    'play': obj
                };
            }
            else if (is_number(obj))
            {
                obj = {
                    'timeoutDuration': obj
                };
            }
            if (obj.progress)
            {
                if (is_string(obj.progress) || is_jquery(obj.progress))
                {
                    obj.progress = {
                        'bar': obj.progress
                    };
                }
            }
            return obj;
        }
        function go_complementAutoObject($tt, obj) {
            if (is_function(obj.button))
            {
                obj.button = obj.button.call($tt);
            }
            if (is_string(obj.button))
            {
                obj.button = $(obj.button);
            }
            if (!is_boolean(obj.play))
            {
                obj.play = true;
            }
            if (!is_number(obj.delay))
            {
                obj.delay = 0;
            }
            if (is_undefined(obj.pauseOnEvent))
            {
                obj.pauseOnEvent = true;
            }
            if (!is_boolean(obj.pauseOnResize))
            {
                obj.pauseOnResize = true;
            }
            if (!is_number(obj.timeoutDuration))
            {
                obj.timeoutDuration = (obj.duration < 10)
                    ? 2500
                    : obj.duration * 5;
            }
            if (obj.progress)
            {
                if (is_function(obj.progress.bar))
                {
                    obj.progress.bar = obj.progress.bar.call($tt);
                }
                if (is_string(obj.progress.bar))
                {
                    obj.progress.bar = $(obj.progress.bar);
                }
                if (obj.progress.bar)
                {
                    if (!is_function(obj.progress.updater))
                    {
                        obj.progress.updater = $.fn.carouFredSel.progressbarUpdater;
                    }
                    if (!is_number(obj.progress.interval))
                    {
                        obj.progress.interval = 50;
                    }
                }
                else
                {
                    obj.progress = false;
                }
            }
            return obj;
        }

        function go_getPrevNextObject($tt, obj) {
            obj = go_getNaviObject($tt, obj);
            if (is_jquery(obj))
            {
                obj = {
                    'button': obj
                };
            }
            else if (is_number(obj))
            {
                obj = {
                    'key': obj
                };
            }
            return obj;
        }
        function go_complementPrevNextObject($tt, obj) {
            if (is_function(obj.button))
            {
                obj.button = obj.button.call($tt);
            }
            if (is_string(obj.button))
            {
                obj.button = $(obj.button);
            }
            if (is_string(obj.key))
            {
                obj.key = cf_getKeyCode(obj.key);
            }
            return obj;
        }

        function go_getPaginationObject($tt, obj) {
            obj = go_getNaviObject($tt, obj);
            if (is_jquery(obj))
            {
                obj = {
                    'container': obj
                };
            }
            else if (is_boolean(obj))
            {
                obj = {
                    'keys': obj
                };
            }
            return obj;
        }
        function go_complementPaginationObject($tt, obj) {
            if (is_function(obj.container))
            {
                obj.container = obj.container.call($tt);
            }
            if (is_string(obj.container))
            {
                obj.container = $(obj.container);
            }
            if (!is_number(obj.items))
            {
                obj.items = false;
            }
            if (!is_boolean(obj.keys))
            {
                obj.keys = false;
            }
            if (!is_function(obj.anchorBuilder) && !is_false(obj.anchorBuilder))
            {
                obj.anchorBuilder = $.fn.carouFredSel.pageAnchorBuilder;
            }
            if (!is_number(obj.deviation))
            {
                obj.deviation = 0;
            }
            return obj;
        }

        function go_getSwipeObject($tt, obj) {
            if (is_function(obj))
            {
                obj = obj.call($tt);
            }
            if (is_undefined(obj))
            {
                obj = {
                    'onTouch': false
                };
            }
            if (is_true(obj))
            {
                obj = {
                    'onTouch': obj
                };
            }
            else if (is_number(obj))
            {
                obj = {
                    'items': obj
                };
            }
            return obj;
        }
        function go_complementSwipeObject($tt, obj) {
            if (!is_boolean(obj.onTouch))
            {
                obj.onTouch = true;
            }
            if (!is_boolean(obj.onMouse))
            {
                obj.onMouse = false;
            }
            if (!is_object(obj.options))
            {
                obj.options = {};
            }
            if (!is_boolean(obj.options.triggerOnTouchEnd))
            {
                obj.options.triggerOnTouchEnd = false;
            }
            return obj;
        }
        function go_getMousewheelObject($tt, obj) {
            if (is_function(obj))
            {
                obj = obj.call($tt);
            }
            if (is_true(obj))
            {
                obj = {};
            }
            else if (is_number(obj))
            {
                obj = {
                    'items': obj
                };
            }
            else if (is_undefined(obj))
            {
                obj = false;
            }
            return obj;
        }
        function go_complementMousewheelObject($tt, obj) {
            return obj;
        }

        //  get number functions
        function gn_getItemIndex(num, dev, org, items, $cfs) {
            if (is_string(num))
            {
                num = $(num, $cfs);
            }

            if (is_object(num))
            {
                num = $(num, $cfs);
            }
            if (is_jquery(num))
            {
                num = $cfs.children().index(num);
                if (!is_boolean(org))
                {
                    org = false;
                }
            }
            else
            {
                if (!is_boolean(org))
                {
                    org = true;
                }
            }
            if (!is_number(num))
            {
                num = 0;
            }
            if (!is_number(dev))
            {
                dev = 0;
            }

            if (org)
            {
                num += items.first;
            }
            num += dev;
            if (items.total > 0)
            {
                while (num >= items.total)
                {
                    num -= items.total;
                }
                while (num < 0)
                {
                    num += items.total;
                }
            }
            return num;
        }

        //  items prev
        function gn_getVisibleItemsPrev(i, o, s) {
            var t = 0,
                x = 0;

            for (var a = s; a >= 0; a--)
            {
                var j = i.eq(a);
                t += (j.is(':visible')) ? j[o.d['outerWidth']](true) : 0;
                if (t > o.maxDimension)
                {
                    return x;
                }
                if (a == 0)
                {
                    a = i.length;
                }
                x++;
            }
        }
        function gn_getVisibleItemsPrevFilter(i, o, s) {
            return gn_getItemsPrevFilter(i, o.items.filter, o.items.visibleConf.org, s);
        }
        function gn_getScrollItemsPrevFilter(i, o, s, m) {
            return gn_getItemsPrevFilter(i, o.items.filter, m, s);
        }
        function gn_getItemsPrevFilter(i, f, m, s) {
            var t = 0,
                x = 0;

            for (var a = s, l = i.length; a >= 0; a--)
            {
                x++;
                if (x == l)
                {
                    return x;
                }

                var j = i.eq(a);
                if (j.is(f))
                {
                    t++;
                    if (t == m)
                    {
                        return x;
                    }
                }
                if (a == 0)
                {
                    a = l;
                }
            }
        }

        function gn_getVisibleOrg($c, o) {
            return o.items.visibleConf.org || $c.children().slice(0, o.items.visible).filter(o.items.filter).length;
        }

        //  items next
        function gn_getVisibleItemsNext(i, o, s) {
            var t = 0,
                x = 0;

            for (var a = s, l = i.length-1; a <= l; a++)
            {
                var j = i.eq(a);

                t += (j.is(':visible')) ? j[o.d['outerWidth']](true) : 0;
                if (t > o.maxDimension)
                {
                    return x;
                }

                x++;
                if (x == l+1)
                {
                    return x;
                }
                if (a == l)
                {
                    a = -1;
                }
            }
        }
        function gn_getVisibleItemsNextTestCircular(i, o, s, l) {
            var v = gn_getVisibleItemsNext(i, o, s);
            if (!o.circular)
            {
                if (s + v > l)
                {
                    v = l - s;
                }
            }
            return v;
        }
        function gn_getVisibleItemsNextFilter(i, o, s) {
            return gn_getItemsNextFilter(i, o.items.filter, o.items.visibleConf.org, s, o.circular);
        }
        function gn_getScrollItemsNextFilter(i, o, s, m) {
            return gn_getItemsNextFilter(i, o.items.filter, m+1, s, o.circular) - 1;
        }
        function gn_getItemsNextFilter(i, f, m, s, c) {
            var t = 0,
                x = 0;

            for (var a = s, l = i.length-1; a <= l; a++)
            {
                x++;
                if (x >= l)
                {
                    return x;
                }

                var j = i.eq(a);
                if (j.is(f))
                {
                    t++;
                    if (t == m)
                    {
                        return x;
                    }
                }
                if (a == l)
                {
                    a = -1;
                }
            }
        }

        //  get items functions
        function gi_getCurrentItems(i, o) {
            return i.slice(0, o.items.visible);
        }
        function gi_getOldItemsPrev(i, o, n) {
            return i.slice(n, o.items.visibleConf.old+n);
        }
        function gi_getNewItemsPrev(i, o) {
            return i.slice(0, o.items.visible);
        }
        function gi_getOldItemsNext(i, o) {
            return i.slice(0, o.items.visibleConf.old);
        }
        function gi_getNewItemsNext(i, o, n) {
            return i.slice(n, o.items.visible+n);
        }

        //  sizes functions
        function sz_storeMargin(i, o, d) {
            if (o.usePadding)
            {
                if (!is_string(d))
                {
                    d = '_cfs_origCssMargin';
                }
                i.each(function() {
                    var j = $(this),
                        m = parseInt(j.css(o.d['marginRight']), 10);
                    if (!is_number(m)) 
                    {
                        m = 0;
                    }
                    j.data(d, m);
                });
            }
        }
        function sz_resetMargin(i, o, m) {
            if (o.usePadding)
            {
                var x = (is_boolean(m)) ? m : false;
                if (!is_number(m))
                {
                    m = 0;
                }
                sz_storeMargin(i, o, '_cfs_tempCssMargin');
                i.each(function() {
                    var j = $(this);
                    j.css(o.d['marginRight'], ((x) ? j.data('_cfs_tempCssMargin') : m + j.data('_cfs_origCssMargin')));
                });
            }
        }
        function sz_storeOrigCss(i) {
            i.each(function() {
                var j = $(this);
                j.data('_cfs_origCss', j.attr('style') || '');
            });
        }
        function sz_restoreOrigCss(i) {
            i.each(function() {
                var j = $(this);
                j.attr('style', j.data('_cfs_origCss') || '');
            });
        }
        function sz_setResponsiveSizes(o, all) {
            var visb = o.items.visible,
                newS = o.items[o.d['width']],
                seco = o[o.d['height']],
                secp = is_percentage(seco);

            all.each(function() {
                var $t = $(this),
                    nw = newS - ms_getPaddingBorderMargin($t, o, 'Width');

                $t[o.d['width']](nw);
                if (secp)
                {
                    $t[o.d['height']](ms_getPercentage(nw, seco));
                }
            });
        }
        function sz_setSizes($c, o) {
            var $w = $c.parent(),
                $i = $c.children(),
                $v = gi_getCurrentItems($i, o),
                sz = cf_mapWrapperSizes(ms_getSizes($v, o, true), o, false);

            $w.css(sz);

            if (o.usePadding)
            {
                var p = o.padding,
                    r = p[o.d[1]];

                if (o.align && r < 0)
                {
                    r = 0;
                }
                var $l = $v.last();
                $l.css(o.d['marginRight'], $l.data('_cfs_origCssMargin') + r);
                $c.css(o.d['top'], p[o.d[0]]);
                $c.css(o.d['left'], p[o.d[3]]);
            }

            $c.css(o.d['width'], sz[o.d['width']]+(ms_getTotalSize($i, o, 'width')*2));
            $c.css(o.d['height'], ms_getLargestSize($i, o, 'height'));
            return sz;
        }

        //  measuring functions
        function ms_getSizes(i, o, wrapper) {
            return [ms_getTotalSize(i, o, 'width', wrapper), ms_getLargestSize(i, o, 'height', wrapper)];
        }
        function ms_getLargestSize(i, o, dim, wrapper) {
            if (!is_boolean(wrapper))
            {
                wrapper = false;
            }
            if (is_number(o[o.d[dim]]) && wrapper)
            {
                return o[o.d[dim]];
            }
            if (is_number(o.items[o.d[dim]]))
            {
                return o.items[o.d[dim]];
            }
            dim = (dim.toLowerCase().indexOf('width') > -1) ? 'outerWidth' : 'outerHeight';
            return ms_getTrueLargestSize(i, o, dim);
        }
        function ms_getTrueLargestSize(i, o, dim) {
            var s = 0;

            for (var a = 0, l = i.length; a < l; a++)
            {
                var j = i.eq(a);

                var m = (j.is(':visible')) ? j[o.d[dim]](true) : 0;
                if (s < m)
                {
                    s = m;
                }
            }
            return s;
        }

        function ms_getTotalSize(i, o, dim, wrapper) {
            if (!is_boolean(wrapper))
            {
                wrapper = false;
            }
            if (is_number(o[o.d[dim]]) && wrapper)
            {
                return o[o.d[dim]];
            }
            if (is_number(o.items[o.d[dim]]))
            {
                return o.items[o.d[dim]] * i.length;
            }

            var d = (dim.toLowerCase().indexOf('width') > -1) ? 'outerWidth' : 'outerHeight',
                s = 0;

            for (var a = 0, l = i.length; a < l; a++)
            {
                var j = i.eq(a);
                s += (j.is(':visible')) ? j[o.d[d]](true) : 0;
            }
            return s;
        }
        function ms_getParentSize($w, o, d) {
            var isVisible = $w.is(':visible');
            if (isVisible)
            {
                $w.hide();
            }
            var s = $w.parent()[o.d[d]]();
            if (isVisible)
            {
                $w.show();
            }
            return s;
        }
        function ms_getMaxDimension(o, a) {
            return (is_number(o[o.d['width']])) ? o[o.d['width']] : a;
        }
        function ms_hasVariableSizes(i, o, dim) {
            var s = false,
                v = false;

            for (var a = 0, l = i.length; a < l; a++)
            {
                var j = i.eq(a);

                var c = (j.is(':visible')) ? j[o.d[dim]](true) : 0;
                if (s === false)
                {
                    s = c;
                }
                else if (s != c)
                {
                    v = true;
                }
                if (s == 0)
                {
                    v = true;
                }
            }
            return v;
        }
        function ms_getPaddingBorderMargin(i, o, d) {
            return i[o.d['outer'+d]](true) - i[o.d[d.toLowerCase()]]();
        }
        function ms_getPercentage(s, o) {
            if (is_percentage(o))
            {
                o = parseInt( o.slice(0, -1), 10 );
                if (!is_number(o))
                {
                    return s;
                }
                s *= o/100;
            }
            return s;
        }

        //  config functions
        function cf_e(n, c, pf, ns, rd) {
            if (!is_boolean(pf))
            {
                pf = true;
            }
            if (!is_boolean(ns))
            {
                ns = true;
            }
            if (!is_boolean(rd))
            {
                rd = false;
            }

            if (pf)
            {
                n = c.events.prefix + n;
            }
            if (ns)
            {
                n = n +'.'+ c.events.namespace;
            }
            if (ns && rd)
            {
                n += c.serialNumber;
            }

            return n;
        }
        function cf_c(n, c) {
            return (is_string(c.classnames[n])) ? c.classnames[n] : n;
        }
        function cf_mapWrapperSizes(ws, o, p) {
            if (!is_boolean(p))
            {
                p = true;
            }
            var pad = (o.usePadding && p) ? o.padding : [0, 0, 0, 0];
            var wra = {};

            wra[o.d['width']] = ws[0] + pad[1] + pad[3];
            wra[o.d['height']] = ws[1] + pad[0] + pad[2];

            return wra;
        }
        function cf_sortParams(vals, typs) {
            var arr = [];
            for (var a = 0, l1 = vals.length; a < l1; a++)
            {
                for (var b = 0, l2 = typs.length; b < l2; b++)
                {
                    if (typs[b].indexOf(typeof vals[a]) > -1 && is_undefined(arr[b]))
                    {
                        arr[b] = vals[a];
                        break;
                    }
                }
            }
            return arr;
        }
        function cf_getPadding(p) {
            if (is_undefined(p))
            {
                return [0, 0, 0, 0];
            }
            if (is_number(p))
            {
                return [p, p, p, p];
            }
            if (is_string(p))
            {
                p = p.split('px').join('').split('em').join('').split(' ');
            }

            if (!is_array(p))
            {
                return [0, 0, 0, 0];
            }
            for (var i = 0; i < 4; i++)
            {
                p[i] = parseInt(p[i], 10);
            }
            switch (p.length)
            {
                case 0:
                    return [0, 0, 0, 0];
                case 1:
                    return [p[0], p[0], p[0], p[0]];
                case 2:
                    return [p[0], p[1], p[0], p[1]];
                case 3:
                    return [p[0], p[1], p[2], p[1]];
                default:
                    return [p[0], p[1], p[2], p[3]];
            }
        }
        function cf_getAlignPadding(itm, o) {
            var x = (is_number(o[o.d['width']])) ? Math.ceil(o[o.d['width']] - ms_getTotalSize(itm, o, 'width')) : 0;
            switch (o.align)
            {
                case 'left': 
                    return [0, x];
                case 'right':
                    return [x, 0];
                case 'center':
                default:
                    return [Math.ceil(x/2), Math.floor(x/2)];
            }
        }
        function cf_getDimensions(o) {
            var dm = [
                    ['width'    , 'innerWidth'  , 'outerWidth'  , 'height'  , 'innerHeight' , 'outerHeight' , 'left', 'top' , 'marginRight' , 0, 1, 2, 3],
                    ['height'   , 'innerHeight' , 'outerHeight' , 'width'   , 'innerWidth'  , 'outerWidth'  , 'top' , 'left', 'marginBottom', 3, 2, 1, 0]
                ];

            var dl = dm[0].length,
                dx = (o.direction == 'right' || o.direction == 'left') ? 0 : 1;

            var dimensions = {};
            for (var d = 0; d < dl; d++)
            {
                dimensions[dm[0][d]] = dm[dx][d];
            }
            return dimensions;
        }
        function cf_getAdjust(x, o, a, $t) {
            var v = x;
            if (is_function(a))
            {
                v = a.call($t, v);

            }
            else if (is_string(a))
            {
                var p = a.split('+'),
                    m = a.split('-');

                if (m.length > p.length)
                {
                    var neg = true,
                        sta = m[0],
                        adj = m[1];
                }
                else
                {
                    var neg = false,
                        sta = p[0],
                        adj = p[1];
                }

                switch(sta)
                {
                    case 'even':
                        v = (x % 2 == 1) ? x-1 : x;
                        break;
                    case 'odd':
                        v = (x % 2 == 0) ? x-1 : x;
                        break;
                    default:
                        v = x;
                        break;
                }
                adj = parseInt(adj, 10);
                if (is_number(adj))
                {
                    if (neg)
                    {
                        adj = -adj;
                    }
                    v += adj;
                }
            }
            if (!is_number(v) || v < 1)
            {
                v = 1;
            }
            return v;
        }
        function cf_getItemsAdjust(x, o, a, $t) {
            return cf_getItemAdjustMinMax(cf_getAdjust(x, o, a, $t), o.items.visibleConf);
        }
        function cf_getItemAdjustMinMax(v, i) {
            if (is_number(i.min) && v < i.min)
            {
                v = i.min;
            }
            if (is_number(i.max) && v > i.max)
            {
                v = i.max;
            }
            if (v < 1)
            {
                v = 1;
            }
            return v;
        }
        function cf_getSynchArr(s) {
            if (!is_array(s))
            {
                s = [[s]];
            }
            if (!is_array(s[0]))
            {
                s = [s];
            }
            for (var j = 0, l = s.length; j < l; j++)
            {
                if (is_string(s[j][0]))
                {
                    s[j][0] = $(s[j][0]);
                }
                if (!is_boolean(s[j][1]))
                {
                    s[j][1] = true;
                }
                if (!is_boolean(s[j][2]))
                {
                    s[j][2] = true;
                }
                if (!is_number(s[j][3]))
                {
                    s[j][3] = 0;
                }
            }
            return s;
        }
        function cf_getKeyCode(k) {
            if (k == 'right')
            {
                return 39;
            }
            if (k == 'left')
            {
                return 37;
            }
            if (k == 'up')
            {
                return 38;
            }
            if (k == 'down')
            {
                return 40;
            }
            return -1;
        }
        function cf_setCookie(n, $c, c) {
            if (n)
            {
                var v = $c.triggerHandler(cf_e('currentPosition', c));
                $.fn.carouFredSel.cookie.set(n, v);
            }
        }
        function cf_getCookie(n) {
            var c = $.fn.carouFredSel.cookie.get(n);
            return (c == '') ? 0 : c;
        }

        //  init function
        function in_mapCss($elem, props) {
            var css = {};
            for (var p = 0, l = props.length; p < l; p++)
            {
                css[props[p]] = $elem.css(props[p]);
            }
            return css;
        }
        function in_complementItems(obj, opt, itm, sta) {
            if (!is_object(obj.visibleConf))
            {
                obj.visibleConf = {};
            }
            if (!is_object(obj.sizesConf))
            {
                obj.sizesConf = {};
            }

            if (obj.start == 0 && is_number(sta))
            {
                obj.start = sta;
            }

            //  visible items
            if (is_object(obj.visible))
            {
                obj.visibleConf.min = obj.visible.min;
                obj.visibleConf.max = obj.visible.max;
                obj.visible = false;
            }
            else if (is_string(obj.visible))
            {
                //  variable visible items
                if (obj.visible == 'variable')
                {
                    obj.visibleConf.variable = true;
                }
                //  adjust string visible items
                else
                {
                    obj.visibleConf.adjust = obj.visible;
                }
                obj.visible = false;
            }
            else if (is_function(obj.visible))
            {
                obj.visibleConf.adjust = obj.visible;
                obj.visible = false;
            }

            //  set items filter
            if (!is_string(obj.filter))
            {
                obj.filter = (itm.filter(':hidden').length > 0) ? ':visible' : '*';
            }

            //  primary item-size not set
            if (!obj[opt.d['width']])
            {
                //  responsive carousel -> set to largest
                if (opt.responsive)
                {
                    debug(true, 'Set a '+opt.d['width']+' for the items!');
                    obj[opt.d['width']] = ms_getTrueLargestSize(itm, opt, 'outerWidth');
                }
                //   non-responsive -> measure it or set to "variable"
                else
                {
                    obj[opt.d['width']] = (ms_hasVariableSizes(itm, opt, 'outerWidth')) 
                        ? 'variable' 
                        : itm[opt.d['outerWidth']](true);
                }
            }

            //  secondary item-size not set -> measure it or set to "variable"
            if (!obj[opt.d['height']])
            {
                obj[opt.d['height']] = (ms_hasVariableSizes(itm, opt, 'outerHeight')) 
                    ? 'variable' 
                    : itm[opt.d['outerHeight']](true);
            }

            obj.sizesConf.width = obj.width;
            obj.sizesConf.height = obj.height;
            return obj;
        }
        function in_complementVisibleItems(opt, avl) {
            //  primary item-size variable -> set visible items variable
            if (opt.items[opt.d['width']] == 'variable')
            {
                opt.items.visibleConf.variable = true;
            }
            if (!opt.items.visibleConf.variable) {
                //  primary size is number -> calculate visible-items
                if (is_number(opt[opt.d['width']]))
                {
                    opt.items.visible = Math.floor(opt[opt.d['width']] / opt.items[opt.d['width']]);
                }
                //  measure and calculate primary size and visible-items
                else
                {
                    opt.items.visible = Math.floor(avl / opt.items[opt.d['width']]);
                    opt[opt.d['width']] = opt.items.visible * opt.items[opt.d['width']];
                    if (!opt.items.visibleConf.adjust)
                    {
                        opt.align = false;
                    }
                }
                if (opt.items.visible == 'Infinity' || opt.items.visible < 1)
                {
                    debug(true, 'Not a valid number of visible items: Set to "variable".');
                    opt.items.visibleConf.variable = true;
                }
            }
            return opt;
        }
        function in_complementPrimarySize(obj, opt, all) {
            //  primary size set to auto -> measure largest item-size and set it
            if (obj == 'auto')
            {
                obj = ms_getTrueLargestSize(all, opt, 'outerWidth');
            }
            return obj;
        }
        function in_complementSecondarySize(obj, opt, all) {
            //  secondary size set to auto -> measure largest item-size and set it
            if (obj == 'auto')
            {
                obj = ms_getTrueLargestSize(all, opt, 'outerHeight');
            }
            //  secondary size not set -> set to secondary item-size
            if (!obj)
            {
                obj = opt.items[opt.d['height']];
            }
            return obj;
        }
        function in_getAlignPadding(o, all) {
            var p = cf_getAlignPadding(gi_getCurrentItems(all, o), o);
            o.padding[o.d[1]] = p[1];
            o.padding[o.d[3]] = p[0];
            return o;
        }
        function in_getResponsiveValues(o, all, avl) {

            var visb = cf_getItemAdjustMinMax(Math.ceil(o[o.d['width']] / o.items[o.d['width']]), o.items.visibleConf);
            if (visb > all.length)
            {
                visb = all.length;
            }

            var newS = Math.floor(o[o.d['width']]/visb);

            o.items.visible = visb;
            o.items[o.d['width']] = newS;
            o[o.d['width']] = visb * newS;
            return o;
        }


        //  buttons functions
        function bt_pauseOnHoverConfig(p) {
            if (is_string(p))
            {
                var i = (p.indexOf('immediate') > -1) ? true : false,
                    r = (p.indexOf('resume')    > -1) ? true : false;
            }
            else
            {
                var i = r = false;
            }
            return [i, r];
        }
        function bt_mousesheelNumber(mw) {
            return (is_number(mw)) ? mw : null
        }

        //  helper functions
        function is_null(a) {
            return (a === null);
        }
        function is_undefined(a) {
            return (is_null(a) || typeof a == 'undefined' || a === '' || a === 'undefined');
        }
        function is_array(a) {
            return (a instanceof Array);
        }
        function is_jquery(a) {
            return (a instanceof jQuery);
        }
        function is_object(a) {
            return ((a instanceof Object || typeof a == 'object') && !is_null(a) && !is_jquery(a) && !is_array(a) && !is_function(a));
        }
        function is_number(a) {
            return ((a instanceof Number || typeof a == 'number') && !isNaN(a));
        }
        function is_string(a) {
            return ((a instanceof String || typeof a == 'string') && !is_undefined(a) && !is_true(a) && !is_false(a));
        }
        function is_function(a) {
            return (a instanceof Function || typeof a == 'function');
        }
        function is_boolean(a) {
            return (a instanceof Boolean || typeof a == 'boolean' || is_true(a) || is_false(a));
        }
        function is_true(a) {
            return (a === true || a === 'true');
        }
        function is_false(a) {
            return (a === false || a === 'false');
        }
        function is_percentage(x) {
            return (is_string(x) && x.slice(-1) == '%');
        }


        function getTime() {
            return new Date().getTime();
        }

        function deprecated( o, n ) {
            debug(true, o+' is DEPRECATED, support for it will be removed. Use '+n+' instead.');
        }
        function debug(d, m) {
            if (!is_undefined(window.console) && !is_undefined(window.console.log))
            {
                if (is_object(d))
                {
                    var s = ' ('+d.selector+')';
                    d = d.debug;
                }
                else
                {
                    var s = '';
                }
                if (!d)
                {
                    return false;
                }
        
                if (is_string(m))
                {
                    m = 'carouFredSel'+s+': ' + m;
                }
                else
                {
                    m = ['carouFredSel'+s+':', m];
                }
                window.console.log(m);
            }
            return false;
        }



        //  EASING FUNCTIONS
        $.extend($.easing, {
            'quadratic': function(t) {
                var t2 = t * t;
                return t * (-t2 * t + 4 * t2 - 6 * t + 4);
            },
            'cubic': function(t) {
                return t * (4 * t * t - 9 * t + 6);
            },
            'elastic': function(t) {
                var t2 = t * t;
                return t * (33 * t2 * t2 - 106 * t2 * t + 126 * t2 - 67 * t + 15);
            }
        });


    })(jQuery);
/*-------------------------------  Fred_plug  -------------------------------------*/   
    /*!
     * jQuery Transit - CSS3 transitions and transformations
     * (c) 2011-2012 Rico Sta. Cruz <rico@ricostacruz.com>
     * MIT Licensed.
     *
     * http://ricostacruz.com/jquery.transit
     * http://github.com/rstacruz/jquery.transit
     */
        (function(d){function m(a){if(a in j.style)return a;var b=["Moz","Webkit","O","ms"],c=a.charAt(0).toUpperCase()+a.substr(1);if(a in j.style)return a;for(a=0;a<b.length;++a){var d=b[a]+c;if(d in j.style)return d}}function l(a){"string"===typeof a&&this.parse(a);return this}function q(a,b,c,e){var h=[];d.each(a,function(a){a=d.camelCase(a);a=d.transit.propertyMap[a]||d.cssProps[a]||a;a=a.replace(/([A-Z])/g,function(a){return"-"+a.toLowerCase()});-1===d.inArray(a,h)&&h.push(a)});d.cssEase[c]&&(c=d.cssEase[c]);
        var f=""+n(b)+" "+c;0<parseInt(e,10)&&(f+=" "+n(e));var g=[];d.each(h,function(a,b){g.push(b+" "+f)});return g.join(", ")}function f(a,b){b||(d.cssNumber[a]=!0);d.transit.propertyMap[a]=e.transform;d.cssHooks[a]={get:function(b){return d(b).css("transit:transform").get(a)},set:function(b,e){var h=d(b).css("transit:transform");h.setFromString(a,e);d(b).css({"transit:transform":h})}}}function g(a,b){return"string"===typeof a&&!a.match(/^[\-0-9\.]+$/)?a:""+a+b}function n(a){d.fx.speeds[a]&&(a=d.fx.speeds[a]);
        return g(a,"ms")}d.transit={version:"0.9.9",propertyMap:{marginLeft:"margin",marginRight:"margin",marginBottom:"margin",marginTop:"margin",paddingLeft:"padding",paddingRight:"padding",paddingBottom:"padding",paddingTop:"padding"},enabled:!0,useTransitionEnd:!1};var j=document.createElement("div"),e={},r=-1<navigator.userAgent.toLowerCase().indexOf("chrome");e.transition=m("transition");e.transitionDelay=m("transitionDelay");e.transform=m("transform");e.transformOrigin=m("transformOrigin");j.style[e.transform]=
        "";j.style[e.transform]="rotateY(90deg)";e.transform3d=""!==j.style[e.transform];var p=e.transitionEnd={transition:"transitionEnd",MozTransition:"transitionend",OTransition:"oTransitionEnd",WebkitTransition:"webkitTransitionEnd",msTransition:"MSTransitionEnd"}[e.transition]||null,k;for(k in e)e.hasOwnProperty(k)&&"undefined"===typeof d.support[k]&&(d.support[k]=e[k]);j=null;d.cssEase={_default:"ease","in":"ease-in",out:"ease-out","in-out":"ease-in-out",snap:"cubic-bezier(0,1,.5,1)",easeOutCubic:"cubic-bezier(.215,.61,.355,1)",
        easeInOutCubic:"cubic-bezier(.645,.045,.355,1)",easeInCirc:"cubic-bezier(.6,.04,.98,.335)",easeOutCirc:"cubic-bezier(.075,.82,.165,1)",easeInOutCirc:"cubic-bezier(.785,.135,.15,.86)",easeInExpo:"cubic-bezier(.95,.05,.795,.035)",easeOutExpo:"cubic-bezier(.19,1,.22,1)",easeInOutExpo:"cubic-bezier(1,0,0,1)",easeInQuad:"cubic-bezier(.55,.085,.68,.53)",easeOutQuad:"cubic-bezier(.25,.46,.45,.94)",easeInOutQuad:"cubic-bezier(.455,.03,.515,.955)",easeInQuart:"cubic-bezier(.895,.03,.685,.22)",easeOutQuart:"cubic-bezier(.165,.84,.44,1)",
        easeInOutQuart:"cubic-bezier(.77,0,.175,1)",easeInQuint:"cubic-bezier(.755,.05,.855,.06)",easeOutQuint:"cubic-bezier(.23,1,.32,1)",easeInOutQuint:"cubic-bezier(.86,0,.07,1)",easeInSine:"cubic-bezier(.47,0,.745,.715)",easeOutSine:"cubic-bezier(.39,.575,.565,1)",easeInOutSine:"cubic-bezier(.445,.05,.55,.95)",easeInBack:"cubic-bezier(.6,-.28,.735,.045)",easeOutBack:"cubic-bezier(.175, .885,.32,1.275)",easeInOutBack:"cubic-bezier(.68,-.55,.265,1.55)"};d.cssHooks["transit:transform"]={get:function(a){return d(a).data("transform")||
        new l},set:function(a,b){var c=b;c instanceof l||(c=new l(c));a.style[e.transform]="WebkitTransform"===e.transform&&!r?c.toString(!0):c.toString();d(a).data("transform",c)}};d.cssHooks.transform={set:d.cssHooks["transit:transform"].set};"1.8">d.fn.jquery&&(d.cssHooks.transformOrigin={get:function(a){return a.style[e.transformOrigin]},set:function(a,b){a.style[e.transformOrigin]=b}},d.cssHooks.transition={get:function(a){return a.style[e.transition]},set:function(a,b){a.style[e.transition]=b}});f("scale");
        f("translate");f("rotate");f("rotateX");f("rotateY");f("rotate3d");f("perspective");f("skewX");f("skewY");f("x",!0);f("y",!0);l.prototype={setFromString:function(a,b){var c="string"===typeof b?b.split(","):b.constructor===Array?b:[b];c.unshift(a);l.prototype.set.apply(this,c)},set:function(a){var b=Array.prototype.slice.apply(arguments,[1]);this.setter[a]?this.setter[a].apply(this,b):this[a]=b.join(",")},get:function(a){return this.getter[a]?this.getter[a].apply(this):this[a]||0},setter:{rotate:function(a){this.rotate=
        g(a,"deg")},rotateX:function(a){this.rotateX=g(a,"deg")},rotateY:function(a){this.rotateY=g(a,"deg")},scale:function(a,b){void 0===b&&(b=a);this.scale=a+","+b},skewX:function(a){this.skewX=g(a,"deg")},skewY:function(a){this.skewY=g(a,"deg")},perspective:function(a){this.perspective=g(a,"px")},x:function(a){this.set("translate",a,null)},y:function(a){this.set("translate",null,a)},translate:function(a,b){void 0===this._translateX&&(this._translateX=0);void 0===this._translateY&&(this._translateY=0);
        null!==a&&void 0!==a&&(this._translateX=g(a,"px"));null!==b&&void 0!==b&&(this._translateY=g(b,"px"));this.translate=this._translateX+","+this._translateY}},getter:{x:function(){return this._translateX||0},y:function(){return this._translateY||0},scale:function(){var a=(this.scale||"1,1").split(",");a[0]&&(a[0]=parseFloat(a[0]));a[1]&&(a[1]=parseFloat(a[1]));return a[0]===a[1]?a[0]:a},rotate3d:function(){for(var a=(this.rotate3d||"0,0,0,0deg").split(","),b=0;3>=b;++b)a[b]&&(a[b]=parseFloat(a[b]));
        a[3]&&(a[3]=g(a[3],"deg"));return a}},parse:function(a){var b=this;a.replace(/([a-zA-Z0-9]+)\((.*?)\)/g,function(a,d,e){b.setFromString(d,e)})},toString:function(a){var b=[],c;for(c in this)if(this.hasOwnProperty(c)&&(e.transform3d||!("rotateX"===c||"rotateY"===c||"perspective"===c||"transformOrigin"===c)))"_"!==c[0]&&(a&&"scale"===c?b.push(c+"3d("+this[c]+",1)"):a&&"translate"===c?b.push(c+"3d("+this[c]+",0)"):b.push(c+"("+this[c]+")"));return b.join(" ")}};d.fn.transition=d.fn.transit=function(a,
        b,c,f){var h=this,g=0,j=!0;"function"===typeof b&&(f=b,b=void 0);"function"===typeof c&&(f=c,c=void 0);"undefined"!==typeof a.easing&&(c=a.easing,delete a.easing);"undefined"!==typeof a.duration&&(b=a.duration,delete a.duration);"undefined"!==typeof a.complete&&(f=a.complete,delete a.complete);"undefined"!==typeof a.queue&&(j=a.queue,delete a.queue);"undefined"!==typeof a.delay&&(g=a.delay,delete a.delay);"undefined"===typeof b&&(b=d.fx.speeds._default);"undefined"===typeof c&&(c=d.cssEase._default);
        b=n(b);var l=q(a,b,c,g),k=d.transit.enabled&&e.transition?parseInt(b,10)+parseInt(g,10):0;if(0===k)return b=j,c=function(b){h.css(a);f&&f.apply(h);b&&b()},!0===b?h.queue(c):b?h.queue(b,c):c(),h;var m={};b=j;c=function(b){this.offsetWidth;var c=!1,g=function(){c&&h.unbind(p,g);0<k&&h.each(function(){this.style[e.transition]=m[this]||null});"function"===typeof f&&f.apply(h);"function"===typeof b&&b()};0<k&&p&&d.transit.useTransitionEnd?(c=!0,h.bind(p,g)):window.setTimeout(g,k);h.each(function(){0<k&&
        (this.style[e.transition]=l);d(this).css(a)})};!0===b?h.queue(c):b?h.queue(b,c):c();return this};d.transit.getTransitionValue=q})(jQuery);

    /*
    * touchSwipe - jQuery Plugin
    * https://github.com/mattbryson/TouchSwipe-Jquery-Plugin
    * http://labs.skinkers.com/touchSwipe/
    * http://plugins.jquery.com/project/touchSwipe
    *
    * Copyright (c) 2010 Matt Bryson (www.skinkers.com)
    * Dual licensed under the MIT or GPL Version 2 licenses.
    *
    * $version: 1.3.3
    */

        (function(g){function P(c){if(c&&void 0===c.allowPageScroll&&(void 0!==c.swipe||void 0!==c.swipeStatus))c.allowPageScroll=G;c||(c={});c=g.extend({},g.fn.swipe.defaults,c);return this.each(function(){var b=g(this),f=b.data(w);f||(f=new W(this,c),b.data(w,f))})}function W(c,b){var f,p,r,s;function H(a){var a=a.originalEvent,c,Q=n?a.touches[0]:a;d=R;n?h=a.touches.length:a.preventDefault();i=0;j=null;k=0;!n||h===b.fingers||b.fingers===x?(r=f=Q.pageX,s=p=Q.pageY,y=(new Date).getTime(),b.swipeStatus&&(c= l(a,d))):t(a);if(!1===c)return d=m,l(a,d),c;e.bind(I,J);e.bind(K,L)}function J(a){a=a.originalEvent;if(!(d===q||d===m)){var c,e=n?a.touches[0]:a;f=e.pageX;p=e.pageY;u=(new Date).getTime();j=S();n&&(h=a.touches.length);d=z;var e=a,g=j;if(b.allowPageScroll===G)e.preventDefault();else{var o=b.allowPageScroll===T;switch(g){case v:(b.swipeLeft&&o||!o&&b.allowPageScroll!=M)&&e.preventDefault();break;case A:(b.swipeRight&&o||!o&&b.allowPageScroll!=M)&&e.preventDefault();break;case B:(b.swipeUp&&o||!o&&b.allowPageScroll!= N)&&e.preventDefault();break;case C:(b.swipeDown&&o||!o&&b.allowPageScroll!=N)&&e.preventDefault()}}h===b.fingers||b.fingers===x||!n?(i=U(),k=u-y,b.swipeStatus&&(c=l(a,d,j,i,k)),b.triggerOnTouchEnd||(e=!(b.maxTimeThreshold?!(k>=b.maxTimeThreshold):1),!0===D()?(d=q,c=l(a,d)):e&&(d=m,l(a,d)))):(d=m,l(a,d));!1===c&&(d=m,l(a,d))}}function L(a){a=a.originalEvent;u=(new Date).getTime();i=U();j=S();k=u-y;if(b.triggerOnTouchEnd||!1===b.triggerOnTouchEnd&&d===z)if(d=q,(h===b.fingers||b.fingers=== x||!n)&&0!==f){var c=!(b.maxTimeThreshold?!(k>=b.maxTimeThreshold):1);if((!0===D()||null===D())&&!c)l(a,d);else if(c||!1===D())d=m,l(a,d)}else d=m,l(a,d);else d===z&&(d=m,l(a,d));e.unbind(I,J,!1);e.unbind(K,L,!1)}function t(){y=u=p=f=s=r=h=0}function l(a,c){var d=void 0;b.swipeStatus&&(d=b.swipeStatus.call(e,a,c,j||null,i||0,k||0,h));if(c===m&&b.click&&(1===h||!n)&&(isNaN(i)||0===i))d=b.click.call(e,a,a.target);if(c==q)switch(b.swipe&&(d=b.swipe.call(e,a,j,i,k,h)),j){case v:b.swipeLeft&&(d=b.swipeLeft.call(e, a,j,i,k,h));break;case A:b.swipeRight&&(d=b.swipeRight.call(e,a,j,i,k,h));break;case B:b.swipeUp&&(d=b.swipeUp.call(e,a,j,i,k,h));break;case C:b.swipeDown&&(d=b.swipeDown.call(e,a,j,i,k,h))}(c===m||c===q)&&t(a);return d}function D(){return null!==b.threshold?i>=b.threshold:null}function U(){return Math.round(Math.sqrt(Math.pow(f-r,2)+Math.pow(p-s,2)))}function S(){var a;a=Math.atan2(p-s,r-f);a=Math.round(180*a/Math.PI);0>a&&(a=360-Math.abs(a));return 45>=a&&0<=a?v:360>=a&&315<=a?v:135<=a&&225>=a? A:45<a&&135>a?C:B}function V(){e.unbind(E,H);e.unbind(F,t);e.unbind(I,J);e.unbind(K,L)}var O=n||!b.fallbackToMouseEvents,E=O?"touchstart":"mousedown",I=O?"touchmove":"mousemove",K=O?"touchend":"mouseup",F="touchcancel",i=0,j=null,k=0,e=g(c),d="start",h=0,y=p=f=s=r=0,u=0;try{e.bind(E,H),e.bind(F,t)}catch(P){g.error("events not supported "+E+","+F+" on jQuery.swipe")}this.enable=function(){e.bind(E,H);e.bind(F,t);return e};this.disable=function(){V();return e};this.destroy=function(){V();e.data(w,null); return e}}var v="left",A="right",B="up",C="down",G="none",T="auto",M="horizontal",N="vertical",x="all",R="start",z="move",q="end",m="cancel",n="ontouchstart"in window,w="TouchSwipe";g.fn.swipe=function(c){var b=g(this),f=b.data(w);if(f&&"string"===typeof c){if(f[c])return f[c].apply(this,Array.prototype.slice.call(arguments,1));g.error("Method "+c+" does not exist on jQuery.swipe")}else if(!f&&("object"===typeof c||!c))return P.apply(this,arguments);return b};g.fn.swipe.defaults={fingers:1,threshold:75, maxTimeThreshold:null,swipe:null,swipeLeft:null,swipeRight:null,swipeUp:null,swipeDown:null,swipeStatus:null,click:null,triggerOnTouchEnd:!0,allowPageScroll:"auto",fallbackToMouseEvents:!0};g.fn.swipe.phases={PHASE_START:R,PHASE_MOVE:z,PHASE_END:q,PHASE_CANCEL:m};g.fn.swipe.directions={LEFT:v,RIGHT:A,UP:B,DOWN:C};g.fn.swipe.pageScroll={NONE:G,HORIZONTAL:M,VERTICAL:N,AUTO:T};g.fn.swipe.fingers={ONE:1,TWO:2,THREE:3,ALL:x}})(jQuery);
/*-------------------------------  SOC_LIKE  -------------------------------*/
    /*! Social Likes v3.0.14 by Artem Sapegin - http://sapegin.github.com/social-likes - Licensed MIT */
    !function(a){"function"==typeof define&&define.amd?define(["jquery"],a):a(jQuery)}(function(a,b){"use strict";function c(a,b){this.container=a,this.options=b,this.init()}function d(b,c){this.widget=b,this.options=a.extend({},c),this.detectService(),this.service&&this.init()}function e(a){function b(a,b){return b.toUpper()}var c={},d=a.data();for(var e in d){var f=d[e];"yes"===f?f=!0:"no"===f&&(f=!1),c[e.replace(/-(\w)/g,b)]=f}return c}function f(a,b){return g(a,b,encodeURIComponent)}function g(a,b,c){return a.replace(/\{([^\}]+)\}/g,function(a,d){return d in b?c?c(b[d]):b[d]:a})}function h(a,b){var c=l+a;return c+" "+c+"_"+b}function i(b,c){function d(g){"keydown"===g.type&&27!==g.which||a(g.target).closest(b).length||(b.removeClass(m),e.off(f,d),a.isFunction(c)&&c())}var e=a(document),f="click touchstart keydown";e.on(f,d)}function j(a){var b=10;if(document.documentElement.getBoundingClientRect){var c=parseInt(a.css("left"),10),d=parseInt(a.css("top"),10),e=a[0].getBoundingClientRect();e.left<b?a.css("left",b-e.left+c):e.right>window.innerWidth-b&&a.css("left",window.innerWidth-e.right-b+c),e.top<b?a.css("top",b-e.top+d):e.bottom>window.innerHeight-b&&a.css("top",window.innerHeight-e.bottom-b+d)}a.addClass(m)}var k="social-likes",l=k+"__",m=k+"_opened",n="https:"===location.protocol?"https:":"http:",o="https:"===n,p={facebook:{counterUrl:"https://graph.facebook.com/fql?q=SELECT+total_count+FROM+link_stat+WHERE+url%3D%22{url}%22&callback=?",convertNumber:function(a){return a.data[0].total_count},popupUrl:"https://www.facebook.com/sharer/sharer.php?u={url}",popupWidth:600,popupHeight:500},twitter:{counterUrl:"https://cdn.api.twitter.com/1/urls/count.json?url={url}&callback=?",convertNumber:function(a){return a.count},popupUrl:"https://twitter.com/intent/tweet?url={url}&text={title}",popupWidth:600,popupHeight:450,click:function(){return/[\.\?:\-–—]\s*$/.test(this.options.title)||(this.options.title+=":"),!0}},mailru:{counterUrl:n+"//connect.mail.ru/share_count?url_list={url}&callback=1&func=?",convertNumber:function(a){for(var b in a)if(a.hasOwnProperty(b))return a[b].shares},popupUrl:n+"//connect.mail.ru/share?share_url={url}&title={title}",popupWidth:550,popupHeight:360},vkontakte:{counterUrl:"https://vk.com/share.php?act=count&url={url}&index={index}",counter:function(b,c){var d=p.vkontakte;d._||(d._=[],window.VK||(window.VK={}),window.VK.Share={count:function(a,b){d._[a].resolve(b)}});var e=d._.length;d._.push(c),a.getScript(f(b,{index:e})).fail(c.reject)},popupUrl:n+"//vk.com/share.php?url={url}&title={title}",popupWidth:550,popupHeight:330},odnoklassniki:{counterUrl:o?b:"http://connect.ok.ru/dk?st.cmd=extLike&ref={url}&uid={index}",counter:function(b,c){var d=p.odnoklassniki;d._||(d._=[],window.ODKL||(window.ODKL={}),window.ODKL.updateCount=function(a,b){d._[a].resolve(b)});var e=d._.length;d._.push(c),a.getScript(f(b,{index:e})).fail(c.reject)},popupUrl:"http://connect.ok.ru/dk?st.cmd=WidgetSharePreview&service=odnoklassniki&st.shareUrl={url}",popupWidth:550,popupHeight:360},plusone:{counterUrl:o?b:"http://share.yandex.ru/gpp.xml?url={url}",counter:function(b,c){var d=p.plusone;return d._?void c.reject():(window.services||(window.services={}),window.services.gplus={cb:function(a){"string"==typeof a&&(a=a.replace(/\D/g,"")),d._.resolve(parseInt(a,10))}},d._=c,void a.getScript(f(b)).fail(c.reject))},popupUrl:"https://plus.google.com/share?url={url}",popupWidth:700,popupHeight:500},pinterest:{counterUrl:n+"//api.pinterest.com/v1/urls/count.json?url={url}&callback=?",convertNumber:function(a){return a.count},popupUrl:n+"//pinterest.com/pin/create/button/?url={url}&description={title}",popupWidth:630,popupHeight:270}},q={promises:{},fetch:function(b,c,d){q.promises[b]||(q.promises[b]={});var e=q.promises[b];if(!d.forceUpdate&&e[c])return e[c];var g=a.extend({},p[b],d),h=a.Deferred(),i=g.counterUrl&&f(g.counterUrl,{url:c});return i&&a.isFunction(g.counter)?g.counter(i,h):g.counterUrl?a.getJSON(i).done(function(b){try{var c=b;a.isFunction(g.convertNumber)&&(c=g.convertNumber(b)),h.resolve(c)}catch(d){h.reject()}}).fail(h.reject):h.reject(),e[c]=h.promise(),e[c]}};a.fn.socialLikes=function(b){return this.each(function(){var d=a(this),f=d.data(k);f?a.isPlainObject(b)&&f.update(b):(f=new c(d,a.extend({},a.fn.socialLikes.defaults,b,e(d))),d.data(k,f))})},a.fn.socialLikes.defaults={url:window.location.href.replace(window.location.hash,""),title:document.title,counters:!0,zeroes:!1,wait:500,timeout:1e4,popupCheckInterval:500,singleTitle:"Share"},c.prototype={init:function(){this.container.addClass(k),this.single=this.container.hasClass(k+"_single"),this.initUserButtons(),this.countersLeft=0,this.number=0,this.container.on("counter."+k,a.proxy(this.updateCounter,this));var b=this.container.children();this.makeSingleButton(),this.buttons=[],b.each(a.proxy(function(b,c){var e=new d(a(c),this.options);this.buttons.push(e),e.options.counterUrl&&this.countersLeft++},this)),this.options.counters?(this.timer=setTimeout(a.proxy(this.appear,this),this.options.wait),this.timeout=setTimeout(a.proxy(this.ready,this,!0),this.options.timeout)):this.appear()},initUserButtons:function(){!this.userButtonInited&&window.socialLikesButtons&&a.extend(!0,p,socialLikesButtons),this.userButtonInited=!0},makeSingleButton:function(){if(this.single){var b=this.container;b.addClass(k+"_vertical"),b.wrap(a("<div>",{"class":k+"_single-w"})),b.wrapInner(a("<div>",{"class":k+"__single-container"}));var c=b.parent(),d=a("<div>",{"class":h("widget","single")}),e=a(g('<div class="{buttonCls}"><span class="{iconCls}"></span>{title}</div>',{buttonCls:h("button","single"),iconCls:h("icon","single"),title:this.options.singleTitle}));d.append(e),c.append(d),d.on("click",function(){var a=k+"__widget_active";return d.toggleClass(a),d.hasClass(a)?(b.css({left:-(b.width()-d.width())/2,top:-b.height()}),j(b),i(b,function(){d.removeClass(a)})):b.removeClass(m),!1}),this.widget=d}},update:function(b){if(b.forceUpdate||b.url!==this.options.url){this.number=0,this.countersLeft=this.buttons.length,this.widget&&this.widget.find("."+k+"__counter").remove(),a.extend(this.options,b);for(var c=0;c<this.buttons.length;c++)this.buttons[c].update(b)}},updateCounter:function(a,b,c){c&&(this.number+=c,this.single&&this.getCounterElem().text(this.number)),this.countersLeft--,0===this.countersLeft&&(this.appear(),this.ready())},appear:function(){this.container.addClass(k+"_visible")},ready:function(a){this.timeout&&clearTimeout(this.timeout),this.container.addClass(k+"_ready"),a||this.container.trigger("ready."+k,this.number)},getCounterElem:function(){var b=this.widget.find("."+l+"counter_single");return b.length||(b=a("<span>",{"class":h("counter","single")}),this.widget.append(b)),b}},d.prototype={init:function(){this.detectParams(),this.initHtml(),setTimeout(a.proxy(this.initCounter,this),0)},update:function(b){a.extend(this.options,{forceUpdate:!1},b),this.widget.find("."+k+"__counter").remove(),this.initCounter()},detectService:function(){var b=this.widget.data("service");if(!b){for(var c=this.widget[0],d=c.classList||c.className.split(" "),e=0;e<d.length;e++){var f=d[e];if(p[f]){b=f;break}}if(!b)return}this.service=b,a.extend(this.options,p[b])},detectParams:function(){var a=this.widget.data();if(a.counter){var b=parseInt(a.counter,10);isNaN(b)?this.options.counterUrl=a.counter:this.options.counterNumber=b}a.title&&(this.options.title=a.title),a.url&&(this.options.url=a.url)},initHtml:function(){var b=this.options,c=this.widget,d=c.find("a");d.length&&this.cloneDataAttrs(d,c);var e=a("<span>",{"class":this.getElementClassNames("button"),text:c.text()});if(b.clickUrl){var g=f(b.clickUrl,{url:b.url,title:b.title}),h=a("<a>",{href:g});this.cloneDataAttrs(c,h),c.replaceWith(h),this.widget=c=h}else c.on("click",a.proxy(this.click,this));c.removeClass(this.service),c.addClass(this.getElementClassNames("widget")),e.prepend(a("<span>",{"class":this.getElementClassNames("icon")})),c.empty().append(e),this.button=e},initCounter:function(){if(this.options.counters)if(this.options.counterNumber)this.updateCounter(this.options.counterNumber);else{var b={counterUrl:this.options.counterUrl,forceUpdate:this.options.forceUpdate};q.fetch(this.service,this.options.url,b).always(a.proxy(this.updateCounter,this))}},cloneDataAttrs:function(a,b){var c=a.data();for(var d in c)c.hasOwnProperty(d)&&b.data(d,c[d])},getElementClassNames:function(a){return h(a,this.service)},updateCounter:function(b){b=parseInt(b,10)||0;var c={"class":this.getElementClassNames("counter"),text:b};b||this.options.zeroes||(c["class"]+=" "+k+"__counter_empty",c.text="");var d=a("<span>",c);this.widget.append(d),this.widget.trigger("counter."+k,[this.service,b])},click:function(b){var c=this.options,d=!0;if(a.isFunction(c.click)&&(d=c.click.call(this,b)),d){var e=f(c.popupUrl,{url:c.url,title:c.title});e=this.addAdditionalParamsToUrl(e),this.openPopup(e,{width:c.popupWidth,height:c.popupHeight})}return!1},addAdditionalParamsToUrl:function(b){var c=a.param(a.extend(this.widget.data(),this.options.data));if(a.isEmptyObject(c))return b;var d=-1===b.indexOf("?")?"?":"&";return b+d+c},openPopup:function(b,c){var d=Math.round(screen.width/2-c.width/2),e=0;screen.height>c.height&&(e=Math.round(screen.height/3-c.height/2));var f=window.open(b,"sl_"+this.service,"left="+d+",top="+e+",width="+c.width+",height="+c.height+",personalbar=0,toolbar=0,scrollbars=1,resizable=1");if(f){f.focus(),this.widget.trigger("popup_opened."+k,[this.service,f]);var g=setInterval(a.proxy(function(){f.closed&&(clearInterval(g),this.widget.trigger("popup_closed."+k,this.service))},this),this.options.popupCheckInterval)}else location.href=b}},a(function(){a("."+k).socialLikes()})});
