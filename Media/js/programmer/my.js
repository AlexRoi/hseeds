$.wNoty.configGlobal({
    theme: 'default',
    position: 'bottom right',
    stack: 7
});
var generate = function( message, type, time ) {
    if(type == 'success') {
        type = 'succes';
    }
    if(time && time != 'undefined') {
        $('.demo_default').wNoty({
            msg: message,
            status: type,
            livetime: time
        });
    } else {
        $('.demo_default').wNoty({
            msg: message,
            status: type,
            immortal: true
        });
    }
};

$(function(){
    /// CART START
    var setTopCartCount = function(count){
        $('#topCartCount').text(count);
    };
    $('.addToCart').on('click', function(e){
        e.preventDefault();
        var it = $(this);
        var id = it.data('id');
        $.ajax({
            url: '/ajax/addToCart',
            type: 'POST',
            dataType: 'JSON',
            data: {
                id: id
            },
            success: function(data){
                if( data.success ) {
                    var html = '';
                    var item;
                    var count = 0;
                    var amt;
                    var amount = 0;
                    for( var i = 0; i < data.cart.length; i++ ) {
                        item = data.cart[i];
                        amt = parseInt( item.count ) * parseInt( item.cost );
                        html += '<li class="wb_item" data-id="'+item.id+'" data-count="'+item.count+'" data-price="'+item.cost+'">';
                        html += '<div class="wb_li">';
                        if ( item.image ) {
                            html += '<div class="wb_side"><div class="wb_img">';
                            html += '<a href="/'+item.alias+'/p'+item.id+'" class="wbLeave"><img src="'+item.image+'" /></a>';
                            html += '</div></div>';
                        }
                        html += '<div class="wb_content">';
                        html += '<div class="wb_row">';
                        html += '<div class="wb_del"><span title="Удалить товар">Удалить товар</span></div>';
                        html += '<div class="wb_ttl"><a href="/'+item.alias+'/p'+item.id+'" class="wbLeave">'+item.name+'</a></div>';
                        html += '</div>';
                        html += '<div class="wb_cntrl">';
                        html += '<div class="wb_price_one"><p><span>'+item.cost+'</span> грн.</p></div>';
                        html += '<div class="wb_amount_wrapp">';
                        html += '<div class="wb_amount">';
                        html += '<input type="text" value="'+item.count+'">';
                        html += '<span data-spin="plus"></span>';
                        html += '<span data-spin="minus"></span>';
                        html += '</div>';
                        html += '</div>';
                        html += '<div class="wb_price_totl"><p><span>'+amt+'</span> грн.</p></div>';
                        html += '</div>';
                        html += '</div>';
                        html += '</div>';
                        html += '</li>';
                        amount += amt;
                        count += parseInt( item.count );
                    }
                    $('#topCartList').html(html);
                    $('#topCartAmount').html(amount);
                    setTopCartCount(count);
                }
                $('.wb_edit_init').click();
            }
        });
    });
    /// CART END

    /// Remove social netqork from pc
    $('.deleteConnection').on('click', function(){
        var id = $(this).data('id');
        $.ajax({
            url: '/ajax/removeConnection',
            type: 'POST',
            dataType: 'JSON',
            data: {
                id: id
            },
            complete: function(data){
                window.location.reload();
            }
        });
    });
});
