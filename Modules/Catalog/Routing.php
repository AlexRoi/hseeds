<?php
    
    return array(
        // Catalog groups routing
        '<lang:ru|en>/products' => 'catalog/catalog/index',
        /*'<lang:ru|en>/products/page/<page:[0-9]*>' => 'catalog/catalog/index',
        '<lang:ru|en>/products/<alias>' => 'catalog/catalog/groups',
        '<lang:ru|en>/products/<alias>/page/<page:[0-9]*>' => 'catalog/catalog/groups',*/
        // Products routing
        '<lang:ru|en>/<alias>/p<id:[0-9]*>' => 'catalog/product/index',
    );