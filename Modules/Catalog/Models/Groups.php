<?php
    namespace Modules\Catalog\Models;

    use Core\QB\DB;

    class Groups extends \Core\CommonI18n {

        public static $table = 'catalog_tree';

        public static function getInnerGroups($parent_id) {
            static::$tableI18n = static::$table.'_i18n';
            $result = DB::select(
                static::$tableI18n.'.*',
                static::$table.'.*'
            )
                ->from(static::$table)
                ->join(static::$tableI18n, 'LEFT')->on(static::$tableI18n.'.row_id', '=', static::$table.'.id')
                ->where(static::$tableI18n.'.language', '=', \I18n::$lang)
                ->where(static::$table.'.parent_id', '=', $parent_id)
                ->where(static::$table.'.status', '=', 1);
            $result->order_by(static::$table.'.sort', 'ASC');
            $result->order_by(static::$table.'.id', 'DESC');
            return $result->find_all();
        }


        public static function countInnerGroups($parent_id) {
            $result = DB::select(array(DB::expr('COUNT('.static::$table.'.id)'), 'count'))
                ->from(static::$table)
                ->where(static::$table.'.parent_id', '=', $parent_id)
                ->where(static::$table.'.status', '=', 1);
            return $result->count_all();
        }

    }