<?php
    namespace Modules\Catalog\Models;

    use Core\Config;
    use Core\Cookie;
    use Core\QB\DB;

    class Items extends \Core\CommonI18n {

        public static $table = 'catalog';
        public static $tableI18n = 'catalog_i18n';
        public static $tableImages = 'catalog_images';

        public static function searchRows($query, $sort = NULL, $type = NULL, $limit = NULL, $offset = NULL) {
            $result = DB::select(
                static::$tableI18n.'.*',
                static::$table.'.*'
            )
                ->from(static::$table)
                ->join(static::$tableI18n, 'LEFT')
                    ->on(static::$tableI18n.'.row_id', '=', static::$table.'.id')
                ->join('catalog_tree', 'LEFT')
                    ->on('catalog_tree.id', '=', static::$table.'.parent_id')
                ->join('catalog_tree_i18n', 'LEFT')
                    ->on('catalog_tree_i18n.row_id', '=', static::$table.'.parent_id')
                ->join(['catalog_tree_i18n', 'over_parent'], 'LEFT')
                    ->on('over_parent.row_id', '=', 'catalog_tree.parent_id')
                ->or_where_open()
                    ->or_where(static::$tableI18n.'.name', 'LIKE', '%'.$query.'%')
                    ->or_where(static::$tableI18n.'.text', 'LIKE', '%'.$query.'%')
                    ->or_where('catalog_tree_i18n.name', 'LIKE', '%'.$query.'%')
                    ->or_where('over_parent.name', 'LIKE', '%'.$query.'%')
                ->or_where_close()
                ->where('catalog_tree_i18n.language', '=', \I18n::$lang)
                ->where('over_parent.language', '=', \I18n::$lang)
                ->where(static::$tableI18n.'.language', '=', \I18n::$lang)
                ->where(static::$table.'.status', '=', 1);
            if( $sort !== NULL ) {
                if( $type !== NULL ) {
                    $result->order_by(static::$table.'.'.$sort, $type);
                } else {
                    $result->order_by(static::$table.'.'.$sort);
                }
            }
            if( $limit !== NULL ) {
                $result->limit($limit);
                if( $offset !== NULL ) {
                    $result->offset($offset);
                }
            }
            return $result->find_all();
        }

        public static function countSearchRows($query) {
            $result = DB::select(array(DB::expr('COUNT('.static::$table.'.id)'), 'count'))
                ->from(static::$table)
                ->join(static::$tableI18n, 'LEFT')
                ->on(static::$tableI18n.'.row_id', '=', static::$table.'.id')
                ->or_where_open()
                    ->or_where(static::$tableI18n.'.name', 'LIKE', '%'.$query.'%')
                    ->or_where(static::$tableI18n.'.text', 'LIKE', '%'.$query.'%')
                ->or_where_close()
                ->where(static::$tableI18n.'.language', '=', \I18n::$lang)
                ->where(static::$table.'.status', '=', 1);
            return $result->count_all();
        }

        public static function getBrandItems($brand_alias, $sort = NULL, $type = NULL, $limit = NULL, $offset = NULL) {
            $result = DB::select(static::$table.'.*')
                ->from(static::$table)
                ->where(static::$table.'.brand_alias', '=', $brand_alias)
                ->where(static::$table.'.status', '=', 1);
            if( $sort !== NULL ) {
                if( $type !== NULL ) {
                    $result->order_by(static::$table.'.'.$sort, $type);
                } else {
                    $result->order_by(static::$table.'.'.$sort);
                }
            }
            if( $limit !== NULL ) {
                $result->limit($limit);
                if( $offset !== NULL ) {
                    $result->offset($offset);
                }
            }
            return $result->find_all();
        }


        public static function countBrandItems($brand_alias) {
            $result = DB::select(array(DB::expr('COUNT('.static::$table.'.id)'), 'count'))
                ->from(static::$table)
                ->where(static::$table.'.brand_alias', '=', $brand_alias)
                ->where(static::$table.'.status', '=', 1);
            return $result->count_all();
        }


        public static function getItemsByFlag($flag, $sort = NULL, $type = NULL, $limit = NULL, $offset = NULL) {
            $result = DB::select(static::$table.'.*')
                ->from(static::$table)
                ->where(static::$table.'.'.$flag, '=', 1)
                ->where(static::$table.'.status', '=', 1);
            if( $sort !== NULL ) {
                if( $type !== NULL ) {
                    $result->order_by(static::$table.'.'.$sort, $type);
                } else {
                    $result->order_by(static::$table.'.'.$sort);
                }
            }
            if( $limit !== NULL ) {
                $result->limit($limit);
                if( $offset !== NULL ) {
                    $result->offset($offset);
                }
            }
            return $result->find_all();
        }


        public static function countItemsByFlag($flag) {
            $result = DB::select(array(DB::expr('COUNT('.static::$table.'.id)'), 'count'))
                ->from(static::$table)
                ->where(static::$table.'.'.$flag, '=', 1)
                ->where(static::$table.'.status', '=', 1);
            return $result->count_all();
        }


        public static function addViewed( $id ) {
            $ids = static::getViewedIDs();
            if( !in_array($id, $ids) ) {
                $ids[] = $id;
                Cookie::setArray('viewed', $ids, 60*60*24*30);
            }
            return;
        }


        public static function getViewedIDs() {
            $ids = Cookie::getArray('viewed', array());
            return $ids;
        }


        public static function getViewedItems($sort = NULL, $type = NULL, $limit = NULL, $offset = NULL) {
            $ids = Items::getViewedIDs();
            if( !$ids ) {
                return array();
            }
            $result = DB::select(static::$table.'.*')
                ->from(static::$table)
                ->where(static::$table.'.id', 'IN', $ids)
                ->where(static::$table.'.status', '=', 1);
            if( $sort !== NULL ) {
                if( $type !== NULL ) {
                    $result->order_by(static::$table.'.'.$sort, $type);
                } else {
                    $result->order_by(static::$table.'.'.$sort);
                }
            }
            if( $limit !== NULL ) {
                $result->limit($limit);
                if( $offset !== NULL ) {
                    $result->offset($offset);
                }
            }
            return $result->find_all();
        }


        public static function countViewedItems() {
            $ids = Items::getViewedIDs();
            if( !$ids ) {
                return 0;
            }
            $result = DB::select(array(DB::expr('COUNT('.static::$table.'.id)'), 'count'))
                ->from(static::$table)
                ->where(static::$table.'.id', 'IN', $ids)
                ->where(static::$table.'.status', '=', 1);
            return $result->count_all();
        }


        public static function getRow($id) {
            $result = DB::select(
                static::$tableI18n.'.*',
                static::$table.'.*',
                array('catalog_tree_i18n.name', 'parent_name')
            )
                ->from(static::$table)
                ->join(static::$tableI18n, 'LEFT')
                    ->on(static::$tableI18n.'.row_id', '=', static::$table.'.id')
                ->join('catalog_tree_i18n', 'LEFT')
                    ->on(static::$table.'.parent_id', '=', 'catalog_tree_i18n.row_id')
                ->where(static::$table.'.status', '=', 1)
                ->where(static::$tableI18n.'.language', '=', \I18n::$lang)
                ->where('catalog_tree_i18n.language', '=', \I18n::$lang)
                ->where(static::$table.'.id', '=', $id);
            return $result->find();
        }

        public static function getItemImages($item_id) {
            $result = DB::select('image')
                ->from(static::$tableImages)
                ->where(static::$tableImages.'.catalog_id', '=', $item_id)
                ->order_by(static::$tableImages.'.sort');
            return $result->find_all();
        }

    }