<?php
    namespace Modules\News\Controllers;

    use Core\HTML;
    use Core\QB\DB;
    use Core\Route;
    use Core\View;
    use Core\Config;
    use Core\Pager\Pager;

    use Core\Widgets;
    use Modules\News\Models\News AS Model;
    use Modules\Content\Models\Control;
    
    class News extends \Modules\Base {

        public $current;
        public $page = 1;
        public $limit;
        public $offset;
        public $model;

        public function before() {
            parent::before();
            $this->current = Control::getRowSimple(Route::controller(), 'alias', 1);
            if( !$this->current ) {
                return Config::error();
            }
            $this->setBreadcrumbs( $this->current->name, $this->current->alias );

            $this->page = !(int) Route::param('page') ? 1 : (int) Route::param('page');
            $this->limit = (int) Config::get('basic.limit-news');
            $this->offset = ($this->page - 1) * $this->limit;
        }

        public function indexAction() {
            $this->_template = 'Text';
            if(Config::get('error')) {
                return false;
            }
            // Seo
            $this->_content_class = 'wNews';
            $this->_page_name = $this->current->h1 ? $this->current->h1 : $this->current->name;
            $this->_seo['h1'] = $this->current->h1;
            $this->_seo['title'] = $this->current->title ? $this->current->title : $this->current->name;
            $this->_seo['keywords'] = $this->current->keywords;
            $this->_seo['description'] = $this->current->description;
            $this->_seo['seo_text'] = $this->current->text;
            // Get Rows
            $result = Model::getRows(1, 'date', 'DESC', $this->limit, $this->offset);
            // Get full count of rows
            $count = Model::countRows(1);
            // Generate pagination
            $pager = Pager::factory($this->page, $count, $this->limit)->create();
            // Render template
            $this->_content = View::tpl(array(
                'page' => $this->current,
                'result' => $result,
                'pager' => $pager
            ), 'News/List' );

        }

        public function innerAction() {
            $this->_template = 'Text';
            if( Config::get('error') ) {
                return false;
            }
            // Check for existence
            $obj = Model::getRowSimple(Route::param('alias'), 'alias', 1);
            if( !$obj ) { return Config::error(); }
            // Seo
            $this->_content_class = 'wNew';
            $this->_page_name = $obj->h1 ? $obj->h1 : $obj->name;
            $this->_seo['h1'] = $obj->h1;
            $this->_seo['title'] = $obj->title ? $obj->title : $obj->name;
            $this->_seo['keywords'] = $obj->keywords;
            $this->_seo['description'] = $obj->description;
            $this->setBreadcrumbs($obj->name);
            // Add plus one to views
            $obj = Model::addView($obj);
            // Render template

            $similar_news = Model::getRows(1, 'date', 'DESC', 3);

            $this->_content = View::tpl(array(
                'obj' => $obj,
                'similar_news' => $similar_news
            ), 'News/Inner' );

        }
    }