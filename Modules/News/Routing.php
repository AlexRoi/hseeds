<?php
    
    return array(
        '<lang:ru|en>/news' => 'news/news/index',
        '<lang:ru|en>/news/page/<page:[0-9]*>' => 'news/news/index',
        '<lang:ru|en>/news/<alias>' => 'news/news/inner',
    );