<?php
    namespace Modules\Search\Controllers;

    use Core\Route;
    use Core\View;
    use Core\Arr;
    use Core\Config;
    use Core\Pager\Pager;

    use Modules\Content\Models\Control;
    use Modules\Catalog\Models\Items;
    
    class Search extends \Modules\Base {

        public $current;
        public $page = 1;
        public $sort;
        public $type;
        public $limit;
        public $offset;
        public $model;

        public function before() {
            parent::before();
            $this->current = Control::getRowSimple(Route::controller(), 'alias', 1);
            if( !$this->current ) {
                return Config::error();
            }
            $this->setBreadcrumbs( $this->current->name, $this->current->alias );
            $this->_template = 'Text';
            $this->page = !(int) Route::param('page') ? 1 : (int) Route::param('page');
            $this->limit = (int) Config::get('basic.limit-search');;//(int) Arr::get($_GET, 'per_page') ? (int) Arr::get($_GET, 'per_page') : Config::get('basic.limit');
            $this->offset = ($this->page - 1) * $this->limit;
        }

        // Search list
        public function indexAction() {
            if( Config::get('error') ) {
                return false;
            }
            // Seo
            $this->_content_class = 'wResult';
            $this->_page_name = '«'.__('search_res_string1').' <span class="wResultFind">'.$this->escapeQuery().'</span> '.__('search_res_string2').':»';
            $this->_seo['h1'] = $this->current->h1;
            $this->_seo['title'] = $this->current->title ? $this->current->title : $this->current->name;
            $this->_seo['keywords'] = $this->current->keywords;
            $this->_seo['description'] = $this->current->description;

            // Check query
            $query = $this->escapeQuery();
            if(!$query) {
                $result = array();
            } else {
                // Get items list
                $result = Items::searchRows($query, $this->sort, $this->type, $this->limit, $this->offset);

                // Count of parent groups
                $count = Items::countSearchRows($query);

                // Generate pagination
                $pager = Pager::factory($this->page, $count, $this->limit)->create();
            }

            // Render page
            $this->_content = View::tpl(array(
                'page' => $this->current,
                'result' => $result,
                'pager' => $pager
            ), 'Catalog/SearchList' );
        }


        // This we will show when no results
        public function noResults() {
            return '<p>По Вашему запросу ничего не найдено!</p>';
        }


        public function escapeQuery() {
            $query = Arr::get($_GET, 'query');
            $query = urldecode($query);
            $query = strip_tags($query);
            $query = addslashes($query);
            return trim($query);
        }

    }