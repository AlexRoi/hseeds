<?php
    namespace Modules\Reviews\Controllers;

    use Core\QB\DB;
    use Core\Route;
    use Core\View;
    use Core\Config;
    use Core\Pager\Pager;

    use Core\Widgets;
    use Modules\Reviews\Models\Reviews AS Model;
    use Modules\Content\Models\Control;
    
    class Reviews extends \Modules\Base {

        public $current;
        public $page = 1;
        public $limit;
        public $offset;
        public $model;

        public function before() {
            parent::before();
            $this->current = Control::getRowSimple(Route::controller(), 'alias', 1);
            if( !$this->current ) {
                return Config::error();
            }
            $this->setBreadcrumbs( $this->current->name, $this->current->alias );

            $this->page = !(int) Route::param('page') ? 1 : (int) Route::param('page');
            $this->limit = (int) Config::get('basic.limit-reviews');
            $this->offset = ($this->page - 1) * $this->limit;
        }

        public function indexAction() {
            if( Config::get('error') ) {
                return false;
            }
            // Seo
            $this->_seo['h1'] = $this->current->h1;
            $this->_seo['title'] = $this->current->title;
            $this->_seo['keywords'] = $this->current->keywords;
            $this->_seo['description'] = $this->current->description;
            $this->_seo['seo_text'] = $this->current->text;
            // Get Rows
            $result = Model::getRows(1, 'date', 'DESC', $this->limit, $this->offset);
            // Get full count of rows
            $count = Model::countRows(1);
            // Generate pagination
            $pager = Pager::factory($this->page, $count, $this->limit)->create();
            // Render template
            $this->_content = View::tpl( array( 'result' => $result, 'pager' => $pager ), 'Reviews/Index' );

            $this->_page_name = $this->current->name;
        }

    }