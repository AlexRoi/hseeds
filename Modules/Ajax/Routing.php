<?php

    return array(
        'ajax/<action>' => 'ajax/general/<action>',
        'form/<action>' => 'ajax/form/<action>',
        'popup/<action>' => 'ajax/popup/<action>',
		'<lang:ru|en>/ajax/<action>' => 'ajax/general/<action>',
        '<lang:ru|en>/form/<action>' => 'ajax/form/<action>',
        '<lang:ru|en>/popup/<action>' => 'ajax/popup/<action>',
    );