<?php
    namespace Modules\Ajax\Controllers;

    use Core\User;
    use Core\Widgets;

    class Popup extends \Modules\Ajax {

        public function authAction() {
            if(User::info()) {
                echo Widgets::get('Popup/Message', array('msg' => __('Вы уже авторизованы!')));
            } else {
                echo Widgets::get('Popup/Auth');
            }
        }

        public function after() {
            die;
        }

    }