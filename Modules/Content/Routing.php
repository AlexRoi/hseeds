<?php
    
    return array(
        '' => 'content/index/index',
        '<lang:ru|en>' => 'content/index/index',
        '<lang:ru|en>/contacts' => 'content/contact/index',
        '<lang:ru|en>/<alias>' => 'content/content/index',
    );