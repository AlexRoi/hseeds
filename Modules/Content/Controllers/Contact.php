<?php
namespace Modules\Content\Controllers;

use Core\View;
use Core\Config;
use Core\HTML;
use Core\QB\DB;

use Modules\Content\Models\Control;

class Contact extends \Modules\Base {

    public $current;

    public function before() {
        parent::before();
        $this->current = Control::getRowSimple('contacts', 'alias', 1);
        if( !$this->current ) {
            return Config::error();
        }
        $this->setBreadcrumbs( $this->current->name, $this->current->alias );
    }

    public function indexAction() {
        $this->_template = 'Text';
        if(Config::get('error')){
            return false;
        }
        // Seo
        $this->_content_class = 'wContacts';
        $this->_page_name = $this->current->h1 ? $this->current->h1 : $this->current->name;
        $this->_seo['h1'] = $this->current->h1;
        $this->_seo['title'] = $this->current->title ? $this->current->title : $this->current->name;
        $this->_seo['keywords'] = $this->current->keywords;
        $this->_seo['description'] = $this->current->description;
        $this->_seo['seo_text'] = $this->current->text;
        // Render template
        $this->_content = View::tpl(array(
                'obj' => $this->current,
        ), 'Contact/Index');

        $images = array();
        if($this->current->gallery_id){
            $res_images = DB::select('gallery_images.*')
                ->from('gallery_images')
                ->join('gallery', 'LEFT')
                    ->on('gallery.id', '=', 'gallery_images.gallery_id')
                ->where('gallery.id', '=', $this->current->gallery_id)
                ->where('gallery.status', '=', 1)
                ->where('gallery_images.gallery_id', '=', $this->current->gallery_id)
                ->order_by('gallery_images.sort')
                ->find_all();
            if(sizeof($res_images)){
                foreach ($res_images as $image) {
                    if(is_file(HOST.HTML::media('images/gallery_images/small/'.$image->image))) {
                        $images[] = $image;
                    }
                }
            }
        }
        $this->_content_slider = View::tpl(array(
            'images' => $images,
        ), 'Content/Slider');

    }
}