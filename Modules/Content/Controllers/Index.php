<?php
namespace Modules\Content\Controllers;

use Core\Route;
use Core\Config;

use Modules\Content\Models\Control;

class Index extends \Modules\Base {

    public $current;

    public function before() {
        parent::before();
        $this->current = Control::getRowSimple(Route::controller(), 'alias', 1);
        if( !$this->current ) {
            return Config::error();
        }
    }

    public function indexAction() {
        $this->_template = 'Main';

        // Check for existence
        if(Config::get('error')){
            return false;
        }
        // Seo
        $this->_seo['h1'] = $this->current->h1;
        $this->_seo['title'] = $this->current->title ? $this->current->title : $this->current->name;
        $this->_seo['keywords'] = $this->current->keywords;
        $this->_seo['description'] = $this->current->description;
        // Render template
        $this->_seo['seo_text'] = $this->current->text;

    }

}