<?php
    namespace Modules\Content\Controllers;

    use Core\Route;
    use Core\View;
    use Core\HTML;
    use Core\QB\DB;
    use Core\Config;

    use Modules\Content\Models\Content AS Model;

    class Content extends \Modules\Base {

        public function indexAction() {
            $this->_template = 'Text';
            // Check for existance
            $page = Model::getRowSimple(Route::param('alias'), 'alias', 1);
            if(!$page){
                return Config::error();
            }
            // Seo
            $this->_page_name = $page->h1 ? $page->h1 : $page->name;
            $this->_seo['h1'] = $page->h1;
            $this->_seo['title'] = $page->title ? $page->title : $page->name;
            $this->_seo['keywords'] = $page->keywords;
            $this->_seo['description'] = $page->description;
            $this->generateParentBreadcrumbs($page->parent_id, 'content', 'parent_id');
            $this->setBreadcrumbs($page->name);
            // Add plus one to views
            $page = Model::addView($page);
            // Get content page children
            $kids = Model::getKids($page->id);
            // Render template
            $this->_content = View::tpl(array(
                'obj' => $page,
                'kids' => $kids
            ), 'Content/Page');

            $images = array();
            if($page->gallery_id){
                $res_images = DB::select('gallery_images.*')
                    ->from('gallery_images')
                    ->join('gallery', 'LEFT')
                    ->on('gallery.id', '=', 'gallery_images.gallery_id')
                    ->where('gallery.id', '=', $page->gallery_id)
                    ->where('gallery.status', '=', 1)
                    ->where('gallery_images.gallery_id', '=', $page->gallery_id)
                    ->order_by('gallery_images.sort')
                    ->find_all();
                if(sizeof($res_images)){
                    foreach ($res_images as $image) {
                        if(is_file(HOST.HTML::media('images/gallery_images/small/'.$image->image))) {
                            $images[] = $image;
                        }
                    }
                }
            }
            $this->_content_slider = View::tpl(array(
                'images' => $images,
            ), 'Content/Slider');

        }

    }