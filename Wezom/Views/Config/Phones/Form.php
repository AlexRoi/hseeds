<form id="myForm" class="rowSection validat" method="post" action="">
    <div class="form-actions" style="display: none;">
        <input class="submit btn btn-primary pull-right" type="submit" value="Отправить">
    </div>
    <div class="col-md-12">
        <div class="widget box">
            <div class="widgetHeader">
                <div class="widgetTitle">
                    <i class="fa-reorder"></i>
                    Основные данные
                </div>
            </div>
            <div class="widgetContent">
                <div class="form-vertical row-border">
                    <div class="form-group">
                        <label class="control-label">Опубликовано</label>
                        <div class="">
                            <label class="checkerWrap-inline">
                                <input name="status" value="0" type="radio" <?php echo (!$obj->status AND $obj) ? 'checked' : ''; ?>>
                                Нет
                            </label>
                            <label class="checkerWrap-inline">
                                <input name="status" value="1" type="radio" <?php echo ($obj->status OR !$obj) ? 'checked' : ''; ?>>
                                Да
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="f_group">Группа</label>
                        <div class="">
                            <select class="form-control valid" name="FORM[group]" id="f_group">
                                <?php foreach(\Core\Config::get('contacts.groups') AS $group_id => $group_name): ?>
                                    <option value="<?php echo $group_id; ?>" <?php echo $group_id == $obj->group ? 'selected' : NULL; ?>><?php echo $group_name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="f_name">Номер телефона</label>
                        <div class="">
                            <input id="f_name" class="form-control valid" type="text" name="FORM[name]" value="<?php echo $obj->name; ?>" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>