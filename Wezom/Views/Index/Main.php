<div class="rowSection clearFix row-bg">
    <?php if( \Core\User::get_access_for_controller('comments') != 'no' || \Core\User::god() ): ?>
        <div class="col-sm-6 col-md-3">
            <div class="statbox widget box box-shadow">
                <div class="widgetContent">
                    <div class="visual green">
                        <i class="fa-comments-o"></i>
                    </div>
                    <div class="title">
                        Новости
                    </div>
                    <div class="value">
                        <?php echo $count_news; ?>
                    </div>
                    <a href="/wezom/news/index" class="more">Подробнее <i class="pull-right fa-angle-right"></i></a>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php if( \Core\User::get_access_for_controller('items') != 'no' || \Core\User::god() ): ?>
        <div class="col-sm-6 col-md-3">
            <div class="statbox widget box box-shadow">
                <div class="widgetContent">
                    <div class="visual yellow">
                        <i class="pull-right fa-fixed-width">&#xf11b;</i>
                    </div>
                    <div class="title">
                        Товары
                    </div>
                    <div class="value">
                        <?php echo $count_catalog; ?>
                    </div>
                    <a href="/wezom/items/index" class="more">Подробнее <i class="pull-right fa-angle-right"></i></a>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>

<?php if(\Core\User::god()): ?>
    <div class="rowSection clearFix">
        <div class="col-md-6">
            <div class="widget">
                <?php echo \Core\Widgets::get('Index_Log'); ?>
            </div>
        </div>
        <?php /*<div class="col-md-6">
            <div class="widget">
                <?php echo \Core\Widgets::get('Index_News'); ?>
            </div>
        </div>*/ ?>
    </div>
<?php else: ?>
    <?php /*<div class="rowSection clearFix">
        <div class="col-md-6">
            <div class="widget">
                <?php echo \Core\Widgets::get('Index_News'); ?>
            </div>
        </div>
    </div>*/ ?>
<?php endif; ?>
