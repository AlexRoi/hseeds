<?php if($_SERVER['REQUEST_METHOD'] == 'POST'): ?>
    <?php $langs = array(); ?>
    <?php foreach($languages AS $key => $lang): ?>
        <?php $langs[$key] = $obj->$key; ?>
        <?php unset($obj->$key); ?>
    <?php endforeach; ?>
<?php else: ?>
    <?php $langs = \Core\Arr::get($obj, 'langs', array()); ?>
    <?php $obj = \Core\Arr::get($obj, 'obj', array()); ?>
<?php endif; ?>
<form id="myForm" class="rowSection validat" method="post" action="" enctype="multipart/form-data">
    <div class="form-actions" style="display: none;">
        <input class="submit btn btn-primary pull-right" type="submit" value="Отправить">
    </div>
    <div class="col-md-6">
        <div class="widget box">
            <div class="widgetHeader">
                <div class="widgetTitle">
                    <i class="fa-reorder"></i>
                    Основные данные
                </div>
            </div>
            <div class="widgetContent">
                <div class="form-group">
                    <div class="rowSection">
                        <div class="col-md-4">
                            <label class="control-label">Опубликовано</label>
                            <div class="">
                                <label class="checkerWrap-inline">
                                    <input name="status" value="0" type="radio" <?php echo (!$obj->status AND $obj) ? 'checked' : ''; ?>>
                                    Нет
                                </label>
                                <label class="checkerWrap-inline">
                                    <input name="status" value="1" type="radio" <?php echo ($obj->status OR !$obj) ? 'checked' : ''; ?>>
                                    Да
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <ul class="liTabs t_wrap">
                    <?php foreach( $languages AS $key => $lang ): ?>
                        <?php $public = \Core\Arr::get($langs, $key, array()); ?>
                        <li class="t_item">
                            <a class="t_link" href="#"><?php echo $lang['name']; ?></a>
                            <div class="t_content">
                                <div class="form-group">
                                    <label class="control-label" for="f_name">Название</label>
                                    <div class="">
                                        <input id="f_name" class="form-control <?php echo $key == 'en' ? 'translitSource' : ''; ?> valid" name="FORM[<?php echo $key; ?>][name]" type="text" value="<?php echo $public->name; ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Описание</label>
                                    <div class="">
                                        <textarea style="height: 350px;0" class="tinymceEditor form-control" rows="20" name="FORM[<?php echo $key; ?>][text]"><?php echo $public->text; ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="f_h1">
                                        Заголовок страницы (h1)
                                        <i class="fa-info-circle text-info bs-tooltip nav-hint liTipLink" title="Рекомендуется, чтобы тег h1 содержал ключевую фразу, которая частично или полностью совпадает с title" style="white-space: nowrap;"></i>
                                    </label>
                                    <div class="">
                                        <input id="f_h1" class="form-control" name="FORM[<?php echo $key; ?>][h1]" type="text" value="<?php echo $public->h1; ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="f_title">
                                        title
                                        <i class="fa-info-circle text-info bs-tooltip nav-hint liTipLink" title="<p>Значимая для продвижения часть заголовка должна быть не более 12 слов</p><p>Самые популярные ключевые слова должны идти в самом начале заголовка и уместиться в первых 50 символов, чтобы сохранить привлекательный вид в поисковой выдаче.</p><p>Старайтесь не использовать в заголовке следующие знаки препинания – . ! ? – </p>" style="white-space: nowrap;"></i>
                                    </label>
                                    <div class="">
                                        <input id="f_title" class="form-control" type="text" name="FORM[<?php echo $key; ?>][title]" value="<?php echo $public->title; ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="f_keywords">Ключевые слова (keywords)</label>
                                    <div class="">
                                        <textarea id="f_keywords" class="form-control" name="FORM[<?php echo $key; ?>][keywords]" rows="5"><?php echo $public->keywords; ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="f_description">Описание (description)</label>
                                    <div class="">
                                        <textarea id="f_description" class="form-control" name="FORM[<?php echo $key; ?>][description]" rows="5"><?php echo $public->description; ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="widget">
            <div class="widgetHeader myWidgetHeader">
                <div class="widgetTitle">
                    <i class="fa-reorder"></i>
                    Доп. параметры
                </div>
            </div>
            <div class="widgetContent">
                <div class="form-group">
                    <div class="rowSection">
                        <div class="col-md-6 form-group">
                            <label class="control-label" for="parent_id">
                                Группа
                            </label>
                            <div class="">
                                <div class="controls">
                                    <select class="form-control valid" id="parent_id" name="FORM[parent_id]">
                                        <option value="">Не выбрано</option>
                                        <?php echo $tree; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--<div class="col-md-6 form-group">
                            <label class="control-label" for="f_sort">
                                Позиция
                                <i class="fa-info-circle text-info bs-tooltip nav-hint" title="<b>Поле определяет позицию товара среди других в списках</b>"></i>
                            </label>
                            <div class="">
                                <input class="form-control" id="f_sort" name="FORM[sort]" type="text" value="<?php echo $obj->sort; ?>" />
                            </div>
                        </div>-->
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label" for="f_alias">
                        Алиас
                        <i class="fa-info-circle text-info bs-tooltip nav-hint" title="<b>Алиас (англ. alias - псевдоним)</b><br>Алиасы используются для короткого именования страниц. <br>Предположим, имеется страница с псевдонимом «<b>about</b>». Тогда для вывода этой страницы можно использовать или полную форму: <br><b>http://domain/?go=frontend&page=about</b><br>или сокращенную: <br><b>http://domain/about.html</b>"></i>
                    </label>
                    <div class="">
                        <div class="input-group">
                            <input class="form-control translitConteiner valid" id="f_alias" name="FORM[alias]" type="text" value="<?php echo $obj->alias; ?>" />
                            <span class="input-group-btn">
                                <button class="btn translitAction" type="button">Заполнить автоматически</button>
                            </span>
                        </div>
                    </div>
                </div>

                <ul class="liTabs t_wrap">
                    <?php foreach( $languages AS $key => $lang ): ?>
                        <?php $public = \Core\Arr::get($langs, $key, array()); ?>
                        <li class="t_item">
                            <a class="t_link" href="#"><?php echo $lang['name']; ?></a>
                            <div class="t_content">
                                <div class="form-group">
                                    <label class="control-label" for="f_maturity">Maturity</label>
                                    <div class="">
                                        <input id="f_maturity" class="form-control" name="FORM[<?php echo $key; ?>][maturity]" type="text" value="<?php echo $public->maturity; ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="f_heat_low">Heat Low</label>
                                    <div class="">
                                        <input id="f_heat_low" class="form-control" name="FORM[<?php echo $key; ?>][heat_low]" type="text" value="<?php echo $public->heat_low; ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="f_harvest">Harvest</label>
                                    <div class="">
                                        <input id="f_harvest" class="form-control" name="FORM[<?php echo $key; ?>][harvest]" type="text" value="<?php echo $public->harvest; ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="f_plant_size">Plant Size</label>
                                    <div class="">
                                        <input id="f_plant_size" class="form-control" name="FORM[<?php echo $key; ?>][plant_size]" type="text" value="<?php echo $public->plant_size; ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="f_color">Color</label>
                                    <div class="">
                                        <input id="f_color" class="form-control" name="FORM[<?php echo $key; ?>][color]" type="text" value="<?php echo $public->color; ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="f_size_large">Size Large</label>
                                    <div class="">
                                        <input id="f_size_large" class="form-control" name="FORM[<?php echo $key; ?>][size_large]" type="text" value="<?php echo $public->size_large; ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="f_resistances">Resistances</label>
                                    <div class="">
                                        <input id="f_resistances" class="form-control" name="FORM[<?php echo $key; ?>][resistances]" type="text" value="<?php echo $public->resistances; ?>" />
                                    </div>
                                </div>

                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>

            </div>
        </div>
    </div>
</form>

<?php echo $uploader; ?>
