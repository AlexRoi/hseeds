<?php if($_SERVER['REQUEST_METHOD'] == 'POST'): ?>
    <?php $langs = array(); ?>
    <?php foreach($languages AS $key => $lang): ?>
        <?php $langs[$key] = $obj->$key; ?>
        <?php unset($obj->$key); ?>
    <?php endforeach; ?>
<?php else: ?>
    <?php $langs = \Core\Arr::get($obj, 'langs', array()); ?>
    <?php $obj = \Core\Arr::get($obj, 'obj', array()); ?>
<?php endif; ?>
<form id="myForm" class="rowSection validat" method="post" action="" enctype="multipart/form-data">
    <div class="form-actions" style="display: none;">
        <input class="submit btn btn-primary pull-right" type="submit" value="Отправить">
    </div>
    <div class="col-md-7">
        <div class="widget box">
            <div class="widgetHeader">
                <div class="widgetTitle">
                    <i class="fa-reorder"></i>
                    Основные данные
                </div>
            </div>
            <div class="widgetContent">
                <div class="form-vertical row-border">
                    <div class="widgetContent">
                        <ul class="liTabs t_wrap">
                            <?php foreach( $languages AS $key => $lang ): ?>
                            <?php $public = \Core\Arr::get($langs, $key, array()); ?>
                            <li class="t_item">
                                <a class="t_link" href="#"><?php echo $lang['name']; ?></a>
                                <div class="t_content">
                                    <div class="form-group">
                                        <label class="control-label" for="f_name">Название</label>
                                        <div class="">
                                            <input id="f_name" class="form-control valid" name="FORM[<?php echo $key; ?>][name]" type="text" value="<?php echo $public->name; ?>" />
                                        </div>
                                    </div>
                                    <?php if($obj->id == 1 || $obj->id == 5 || $obj->id == 6){} else { ?>
                                        <div class="form-group">
                                            <label class="control-label">Контент</label>
                                            <div class="">
                                                <textarea style="height: 350px;0" class="tinymceEditor form-control" rows="20" name="FORM[<?php echo $key; ?>][text]"><?php echo $public->text; ?></textarea>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if($obj->id == 1 || $obj->id == 6){} else { ?>
                                        <div class="form-group">
                                            <label class="control-label" for="f_h1">
                                                Заголовок страницы (h1)
                                                <i class="fa-info-circle text-info bs-tooltip nav-hint liTipLink" title="Рекомендуется, чтобы тег h1 содержал ключевую фразу, которая частично или полностью совпадает с title" style="white-space: nowrap;"></i>
                                            </label>
                                            <div class="">
                                                <input id="f_h1" class="form-control" name="FORM[<?php echo $key; ?>][h1]" type="text" value="<?php echo $public->h1; ?>" />
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <div class="form-group">
                                        <label class="control-label" for="f_title">
                                            title
                                            <i class="fa-info-circle text-info bs-tooltip nav-hint liTipLink" title="<p>Значимая для продвижения часть заголовка должна быть не более 12 слов</p><p>Самые популярные ключевые слова должны идти в самом начале заголовка и уместиться в первых 50 символов, чтобы сохранить привлекательный вид в поисковой выдаче.</p><p>Старайтесь не использовать в заголовке следующие знаки препинания – . ! ? – </p>" style="white-space: nowrap;"></i>
                                        </label>
                                        <div class="">
                                            <input id="f_title" class="form-control" type="text" name="FORM[<?php echo $key; ?>][title]" value="<?php echo $public->title; ?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="f_keywords">Ключевые слова (keywords)</label>
                                        <div class="">
                                            <textarea id="f_keywords" class="form-control" name="FORM[<?php echo $key; ?>][keywords]" rows="5"><?php echo $public->keywords; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="f_description">Описание (description)</label>
                                        <div class="">
                                            <textarea id="f_description" class="form-control" name="FORM[<?php echo $key; ?>][description]" rows="5"><?php echo $public->description; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="widget">
            <!--<div class="widgetContent">
                <div class="form-vertical row-border">
                    <div class="form-group">
                        <label class="control-label">Галерея</label>
                        <div class="">
                            <select id="f_gallery" class="form-control" name="FORM[gallery_id]">
                                <option value=""> - </option>
                                <?php if(count($galleries)){
                                    foreach($galleries as $gallery){ ?>
                                        <option value="<?php echo $gallery->id; ?>" <?php if($obj->gallery_id == $gallery->id){ echo 'selected'; } ?>><?php echo $gallery->name; ?></option>
                                    <?php }
                                } ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>-->
            <?php if($obj->id == 1 || $obj->id == 5 || $obj->id == 6 || $obj->id == 9){} else { ?>
                <div class="widgetHeader myWidgetHeader">
                    <div class="widgetTitle">
                        <i class="fa-reorder"></i>
                        Изображение
                    </div>
                </div>
                <div class="widgetContent">
                    <div class="form-vertical row-border">
                        <div class="form-group">
                            <label class="control-label">Изображение</label>
                            <div class="">
                                <?php if (is_file( HOST . Core\HTML::media('images/control/original/'.$obj->image) )): ?>
                                    <a href="<?php echo Core\HTML::media('images/control/original/'.$obj->image); ?>" rel="lightbox">
                                        <img src="<?php echo Core\HTML::media('images/control/big/'.$obj->image); ?>" style="max-height: 100px;" />
                                    </a>
                                    <br />
                                    <a href="/wezom/<?php echo Core\Route::controller(); ?>/delete_image/<?php echo $obj->id; ?>">Удалить изображение</a>
                                    <br />
                                    <a href="<?php echo \Core\General::crop('control', 'big', $obj->image); ?>">Редактировать</a>
                                <?php else: ?>
                                    <input type="file" name="file" />
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <?php echo $items; ?>
    </div>
</form>