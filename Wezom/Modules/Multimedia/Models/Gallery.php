<?php
namespace Wezom\Modules\Multimedia\Models;

use Core\Arr;
use Core\Message;
use Core\QB\DB;

class Gallery extends \Core\Common {

    public static $table = 'gallery';
    public static $image = 'gallery';

    public static $rules = array(
        'name' => array(
            array(
                'error' => 'Название фотоальбома не может быть пустым!',
                'key' => 'not_empty',
            ),
        ),
        /*'alias' => array(
            array(
                'error' => 'Алиас не может быть пустым!',
                'key' => 'not_empty',
            ),
            array(
                'error' => 'Алиас должен содержать только латинские буквы в нижнем регистре, цифры, "-" или "_"!',
                'key' => 'regex',
                'value' => '/^[a-z0-9\-_]*$/',
            ),
        ),*/
    );

    public static function getGalleriesForSelect(){
        $result = DB::select()->from('gallery')->order_by('name')->find_all();

        return $result;
    }

}