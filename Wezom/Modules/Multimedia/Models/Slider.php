<?php
namespace Wezom\Modules\Multimedia\Models;

use Core\Arr;
use Core\Message;

class Slider extends \Core\CommonI18n {

    public static $table = 'slider';
    public static $image = 'slider';
    public static $rulesI18n = array(
        'name' => array(
            array(
                'error' => 'Название не может быть пустым! (:lang)',
                'key' => 'not_empty',
            ),
        ),
    );
    public static $rules = array(
        'url' => array(
            array(
                'error' => 'Ссылка не может быть пустой!',
                'key' => 'not_empty',
            ),
            array(
                'error' => 'Ссылка должна содержать только латинские буквы в нижнем регистре, цифры, "/", "?", "&", "=", "-" или "_"!',
                'key' => 'regex',
                'value' => '/^[a-z0-9\-_\/\?\=\&]*$/',
            ),
        ),
    );

}