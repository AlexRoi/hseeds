<?php
    namespace Wezom\Modules\Dict\Models;

    class SizesGroups extends \Core\CommonI18n {

        public static $table = 'sizes_groups';
        public static $rulesI18n = array(
            'name' => array(
                array(
                    'error' => 'Название группы не может быть пустым! (:lang)',
                    'key' => 'not_empty',
                ),
            ),
        );

    }