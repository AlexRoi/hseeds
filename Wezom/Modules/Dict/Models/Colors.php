<?php
    namespace Wezom\Modules\Dict\Models;

    class Colors extends \Core\CommonI18n {

        public static $table = 'colors';
        public static $image = 'colors';
        public static $rulesI18n = array(
            'name' => array(
                array(
                    'error' => 'Название цвета не может быть пустым! (:lang)',
                    'key' => 'not_empty',
                ),
            ),
        );
        public static $rules = array(
            'alias' => array(
                array(
                    'error' => 'Алиас не может быть пустым!',
                    'key' => 'not_empty',
                ),
                array(
                    'error' => 'Алиас должен содержать только латинские буквы в нижнем регистре, цифры, "-" или "_"!',
                    'key' => 'regex',
                    'value' => '/^[a-z0-9\-_]*$/',
                ),
                array(
                    'error' => 'Алиас должен быть уникален!',
                    'key' => 'unique',
                    'value' => 'colors',
                ),
            ),
        );
    }