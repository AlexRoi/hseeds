<?php

    return array(
        // Groups
        'wezom/sizesGroups/index' => 'dict/sizesGroups/index',
        'wezom/sizesGroups/index/page/<page:[0-9]*>' => 'dict/sizesGroups/index',
        'wezom/sizesGroups/edit/<id:[0-9]*>' => 'dict/sizesGroups/edit',
        'wezom/sizesGroups/delete/<id:[0-9]*>' => 'dict/sizesGroups/delete',
        'wezom/sizesGroups/add' => 'dict/sizesGroups/add',
        // Items
        'wezom/sizes/index' => 'dict/sizes/index',
        'wezom/sizes/index/page/<page:[0-9]*>' => 'dict/sizes/index',
        'wezom/sizes/edit/<id:[0-9]*>' => 'dict/sizes/edit',
        'wezom/sizes/delete/<id:[0-9]*>' => 'dict/sizes/delete',
        'wezom/sizes/add' => 'dict/sizes/add',
        // Colors
        'wezom/colors/index' => 'dict/colors/index',
        'wezom/colors/index/page/<page:[0-9]*>' => 'dict/colors/index',
        'wezom/colors/edit/<id:[0-9]*>' => 'dict/colors/edit',
        'wezom/colors/delete/<id:[0-9]*>' => 'dict/colors/delete',
        'wezom/colors/delete_image/<id:[0-9]*>' => 'dict/colors/deleteImage',
        'wezom/colors/add' => 'dict/colors/add',
    );