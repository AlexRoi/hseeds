<?php
    namespace Wezom\Modules\Content\Models;

    class Content extends \Core\CommonI18n {

        public static $table = 'content';
        public static $image = 'content';
        public static $rules = array(
            'en/name' => array(
                array(
                    'error' => 'Название страницы на английском не может быть пустым!',
                    'key' => 'not_empty',
                ),
            ),
            'ru/name' => array(
                array(
                    'error' => 'Название страницы на русском не может быть пустым!',
                    'key' => 'not_empty',
                ),
            ),
            'alias' => array(
                array(
                    'error' => 'Алиас не может быть пустым!',
                    'key' => 'not_empty',
                ),
                array(
                    'error' => 'Алиас должен содержать только латинские буквы в нижнем регистре, цифры, "-" или "_"!',
                    'key' => 'regex',
                    'value' => '/^[a-z0-9\-_]*$/',
                ),
            ),
        );

        public static $rulesI18n = array(
            /*'h1' => array(
                array(
                    'error' => 'H1 не может быть пустым! (:lang)',
                    'key' => 'not_empty',
                ),
            ),*/
        );

    }