<?php
    namespace Wezom\Modules\Content\Models;

    use Core\QB\DB;

    class News extends \Core\CommonI18n {

        public static $table = 'news';
        public static $tableI18n = 'news_i18n';
        public static $image = 'news';
        public static $filters = array(
            'name' => array(
                'table' => 'news_i18n',
                'action' => 'LIKE',
            ),
        );
        public static $rulesI18n = array(
            'name' => array(
                array(
                    'error' => 'Название не может быть пустым! (:lang)',
                    'key' => 'not_empty',
                ),
            ),
        );
        public static $rules = array(
            'alias' => array(
                array(
                    'error' => 'Алиас не может быть пустым!',
                    'key' => 'not_empty',
                ),
                array(
                    'error' => 'Алиас должен содержать только латинские буквы в нижнем регистре, цифры, "-" или "_"!',
                    'key' => 'regex',
                    'value' => '/^[a-z0-9\-_]*$/',
                ),
                array(
                    'error' => 'Алиас должен быть уникальным!',
                    'key' => 'unique',
                    'value' => 'news',
                ),
            ),
            'date' => array(
                array(
                    'error' => 'Дата не может быть пустой!',
                    'key' => 'not_empty',
                ),
                array(
                    'error' => 'Укажите правильную дату!',
                    'key' => 'date',
                ),
            ),
        );


        public static function getRows($status = NULL, $date_s = NULL, $date_po = NULL, $sort = NULL, $type = NULL, $limit = NULL, $offset = NULL, $filter = true) {
            if(!static::$tableI18n) {
                static::$tableI18n = static::$table.'_i18n';
            }
            $result = DB::select(
                static::$tableI18n.'.*',
                static::$table.'.*'
            )
                ->from(static::$table)
                ->join(static::$tableI18n, 'LEFT')->on(static::$tableI18n.'.row_id', '=', static::$table.'.id')
                ->where(static::$tableI18n.'.language', '=', \I18n::$lang);
            if( $date_s ) {
                $result->where(static::$table . '.date', '>=', $date_s);
            }
            if( $date_po ) {
                $result->where(static::$table.'.date', '<=', $date_po + 24 * 60 * 60 - 1);
            }
            if( $filter ) {
                $result = static::setFilter($result);
            }
            if( $status <> NULL ) {
                $result->where(static::$table.'.status', '=', $status);
            }
            $result->order_by(static::$table.'.id', 'DESC');
            if( $sort <> NULL ) {
                if( $type <> NULL ) {
                    $result->order_by(static::$table.'.'.$sort, $type);
                } else {
                    $result->order_by(static::$table.'.'.$sort);
                }
            }
            if( $limit <> NULL ) {
                $result->limit($limit);
            }
            if( $offset <> NULL ) {
                $result->offset($offset);
            }
            return $result->find_all();
        }


        public static function countRows($status = NULL, $date_s = NULL, $date_po = NULL, $filter=true) {
            if(!static::$tableI18n) {
                static::$tableI18n = static::$table.'_i18n';
            }
            $result = DB::select(array(DB::expr('COUNT('.static::$table.'.id)'), 'count'))
                ->from(static::$table)
                ->join(static::$tableI18n, 'LEFT')->on(static::$tableI18n.'.row_id', '=', static::$table.'.id')
                ->where(static::$tableI18n.'.language', '=', \I18n::$lang);
            if( $status !== NULL ) {
                $result->where(static::$table.'.status', '=', $status);
            }
            if( $date_s ) {
                $result->where(static::$table . '.date', '>=', $date_s);
            }
            if( $date_po ) {
                $result->where(static::$table.'.date', '<=', $date_po + 24 * 60 * 60 - 1);
            }
			
			if( $filter ) {
                $result = static::setFilter($result);
            }

            return $result->count_all();
        }

    }