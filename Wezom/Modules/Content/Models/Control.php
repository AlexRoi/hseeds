<?php
    namespace Wezom\Modules\Content\Models;

    class Control extends \Core\CommonI18n {

        public static $table = 'control';
        public static $image = 'control';
        public static $rulesI18n = array(
            'name' => array(
                array(
                    'error' => 'Название не может быть пустым! (:lang)',
                    'key' => 'not_empty',
                ),
            ),
        );

    }