<?php
namespace Wezom\Modules\Index\Controllers;

use Wezom\Modules\Catalog\Models\Items;
use Wezom\Modules\Content\Models\News;
use Core\View;

class Index extends \Wezom\Modules\Base {

    function indexAction () {
        $this->_seo['h1'] = 'Панель управления';
        $this->_seo['title'] = 'Панель управления';

        $count_catalog = Items::countRows();
        $count_news = News::countRows();

        $this->_content = View::tpl( array(
            'count_catalog' => $count_catalog,
            'count_news' => $count_news,
        ), 'Index/Main');
    }

}