<?php
    namespace Wezom\Modules\Catalog\Models;

    class Items extends \Core\CommonI18n {

        public static $table = 'catalog';
        public static $filters = array(
            'name' => array(
                'table' => 'catalog_i18n',
                'action' => 'LIKE',
            ),
            'parent_id' => array(
                'table' => NULL,
                'action' => '=',
            ),
        );
        public static $rulesI18n = array(
            'name' => array(
                array(
                    'error' => 'Название товара не может быть пустым! (:lang)',
                    'key' => 'not_empty',
                ),
            ),
        );
        public static $rules = array(
            'alias' => array(
                array(
                    'error' => 'Алиас не может быть пустым!',
                    'key' => 'not_empty',
                ),
                array(
                    'error' => 'Алиас должен содержать только латинские буквы в нижнем регистре, цифры, "-" или "_"!',
                    'key' => 'regex',
                    'value' => '/^[a-z0-9\-_]*$/',
                ),
                array(
                    'error' => 'Алиас должен быть уникальным!',
                    'key' => 'unique',
                    'value' => 'catalog',
                ),
            ),
            'parent_id' => array(
                array(
                    'error' => 'Выберите группу из списка!',
                    'key' => 'pos_numeric',
                ),
            ),
        );

    }