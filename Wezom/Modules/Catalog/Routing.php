<?php   
    
    return array(
        // Groups
        'wezom/groups/index' => 'catalog/groups/index',
        'wezom/groups/index/page/<page:[0-9]*>' => 'catalog/groups/index',
        'wezom/groups/edit/<id:[0-9]*>' => 'catalog/groups/edit',
        'wezom/groups/delete/<id:[0-9]*>' => 'catalog/groups/delete',
        'wezom/groups/delete_image/<id:[0-9]*>' => 'catalog/groups/deleteImage',
        'wezom/groups/delete_image_menu/<id:[0-9]*>' => 'catalog/groups/deleteImageMenu',
        'wezom/groups/add' => 'catalog/groups/add',
        // Items
        'wezom/items/index' => 'catalog/items/index',
        'wezom/items/index/page/<page:[0-9]*>' => 'catalog/items/index',
        'wezom/items/edit/<id:[0-9]*>' => 'catalog/items/edit',
        'wezom/items/delete/<id:[0-9]*>' => 'catalog/items/delete',
        'wezom/items/add' => 'catalog/items/add',
        // Brands
        'wezom/brands/index' => 'catalog/brands/index',
        'wezom/brands/index/page/<page:[0-9]*>' => 'catalog/brands/index',
        'wezom/brands/edit/<id:[0-9]*>' => 'catalog/brands/edit',
        'wezom/brands/delete/<id:[0-9]*>' => 'catalog/brands/delete',
        'wezom/brands/delete_image/<id:[0-9]*>' => 'catalog/brands/deleteImage',
        'wezom/brands/add' => 'catalog/brands/add',
        // Certificates
        'wezom/cert/index' => 'catalog/cert/index',
        'wezom/cert/index/page/<page:[0-9]*>' => 'catalog/cert/index',
        'wezom/cert/edit/<id:[0-9]*>' => 'catalog/cert/edit',
        'wezom/cert/delete/<id:[0-9]*>' => 'catalog/cert/delete',
        'wezom/cert/delete_image/<id:[0-9]*>' => 'catalog/cert/deleteImage',
        'wezom/cert/add' => 'catalog/cert/add',
    );