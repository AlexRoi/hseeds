<?php
namespace Core;

use Core\QB\DB;

use Modules\Catalog\Models\Groups;
use Modules\News\Models\News;

/**
 *  Class that helps with widgets on the site
 */
class Widgets {

    static $_instance; // Constant that consists self class

    public static $_menu = array();
    public static $_emails = array();
    public static $_phones = array();

    // Instance method
    static function factory() {
        if(self::$_instance == NULL) { self::$_instance = new self(); }
        return self::$_instance;
    }

    /**
     *  Get widget
     *  @param  string $name  [Name of template file]
     *  @param  array  $array [Array with data -> go to template]
     *  @return string        [Widget HTML]
     */
    public static function get( $name, $array = array(), $save = true, $cache = false ) {
        $arr = explode('_', $name);
        $viewpath = implode('/', $arr);

//            $token = Profiler::start('Profiler', 'Widget '.$name);
        if( APPLICATION && !Config::get('error') ) {
            $w = WidgetsBackend::factory();
        } else {
            $w = Widgets::factory();
        }

        $_cache = Cache::instance();
        if($cache) {
            if (!$_cache->get($name)) {
                $data = NULL;
                if ($save && isset($w->_data[$name])) {
                    $data = $w->_data[$name];
                } else {
                    if( $save && isset( $w->_data[ $name ] ) ) {
                        $data = $w->_data[ $name ];
                    } else if( method_exists( $w, $name ) ) {
                        $result = $w->$name($array);
                        if( $result !== NULL && $result !== FALSE ) {
                            $array = array_merge($array, $result);
                            $data = View::widget( $array, $viewpath);
                        } else {
                            $data = NULL;
                        }
                    } else {
                        $data = $w->common( $viewpath, $array );
                    }
                }
                $_cache->set($name, HTML::compress($data, true));
                return $w->_data[$name] = $data;
            } else {
                return $_cache->get($name);
            }
        }
        if($_cache->get($name)) {
            $_cache->delete($name);
        }
        if( $save && isset( $w->_data[ $name ] ) ) {
            return $w->_data[ $name ];
        }
        if( method_exists( $w, $name ) ) {
            $result = $w->$name($array);
            if( $result !== NULL && $result !== FALSE ) {
                if(is_array($result)) {
                    $array = array_merge($array, $result);
                }
                return $w->_data[$name] = View::widget( $array, $viewpath);
            } else {
                return $w->_data[$name] = NULL;
            }
        }
//            Profiler::stop($token);
        return $w->_data[$name] = $w->common( $viewpath, $array );
    }

    /**
     *  Common widget method. Uses when we have no widgets called $name
     *  @param  string $viewpath  [Name of template file]
     *  @param  array  $array     [Array with data -> go to template]
     *  @return string            [Widget HTML or NULL if template doesn't exist]
     */
    public function common( $viewpath, $array )
    {
        if (file_exists(HOST.'/Views/Widgets/'.$viewpath.'.php')) {
            return View::widget($array, $viewpath);
        }
        return NULL;
    }


    public function Head() {
        $styles = array(
            HTML::media('css/plugin.css'),
            HTML::media('css/style.css'),
            HTML::media('css/smoothDivScroll.css'),
            HTML::media('css/wnoty/jquery.wnoty-2.0.css'),
            HTML::media('css/wnoty/jquery.wnoty-theme-default.css'),
            HTML::media('css/programmer/my.css'),
        );

        $scripts = array(
            HTML::media('js/modernizr.js'),
            HTML::media('js/jquery-1.11.0.min.js'),
            HTML::media('js/plugins.js'),
            HTML::media('js/jquery-ui-1.10.3.custom.js'),
            HTML::media('js/jquery.mousewheel.js'),
            HTML::media('js/jquery.kinetic.js'),
            HTML::media('js/jquery.smoothDivScroll-1.3.js'),
            HTML::media('js/init.js'),
            HTML::media('js/wnoty/jquery.wnoty-2.0.js'),
            HTML::media('js/programmer/my.js'),
        );

        $scripts_no_minify = array();

        return array('scripts' => $scripts, 'styles' => $styles, 'scripts_no_minify' => $scripts_no_minify);
    }

    public function Header() {
        $languages = DB::select()->from("i18n")->find_all();

        if(!static::$_menu) {
            $result = CommonI18n::factory('sitemenu')->getRows(1, 'sort', 'ASC');
            foreach($result AS $key => $value) {
                static::$_menu[$value->group][] = $value;
            }
        }
        return array('menu' => static::$_menu, 'languages' => $languages);
    }

    public function Footer($params = array()) {

        if(!static::$_menu) {
            $result = CommonI18n::factory('sitemenu')->getRows(1, 'sort', 'ASC');
            foreach($result AS $key => $value) {
                static::$_menu[$value->group][] = $value;
            }
        }
        return array('menu' => static::$_menu);
    }

    public function Index_Gallery() {
        $result = DB::select()->from('gallery_images')->where('gallery_id', '=', 7)->order_by('sort')->find_all();
        $gallery = array();
        foreach($result AS $key => $value){
            if(is_file(HOST.HTML::media('images/gallery_images/small/'.$value->image))){
                $gallery[] = $value;
            }
        }

        if(!sizeof($gallery)) {
            return NULL;
        }
        if(count($gallery) > 3){
            shuffle($gallery);
        }
        return array('gallery' => $gallery);
    }

    public function Index_Slider() {
        $result = CommonI18n::factory('slider')->getRows(1, 'sort', 'ASC');
        $slider = array();
        foreach($result AS $key => $value) {
            if(is_file(HOST.HTML::media('images/slider/big/'.$value->image))) {
                $slider[] = $value;
            }
        }
        if(!sizeof($slider)) {
            return NULL;
        }
        return array('result' => $slider);
    }


    public function Catalog_CatMenu() {

        $active_cat = Route::param('group');
        $active_parent = null;

        $result_cats = DB::select(
            'catalog_tree.id',
            'catalog_tree.alias',
            'catalog_tree.parent_id',
            'catalog_tree_i18n.name'
        )
            ->from('catalog_tree')
            ->join('catalog_tree_i18n')
                ->on('catalog_tree_i18n.row_id', '=', 'catalog_tree.id')
            ->where('catalog_tree_i18n.language', '=', \I18n::$lang)
            ->where('catalog_tree.status', '=', 1)
            ->order_by('catalog_tree.sort')
            ->find_all();
        if(sizeof($result_cats)){
            $categories = array();
            foreach($result_cats as $cat){
                $categories[$cat->parent_id][] = $cat;
                if($cat->id == $active_cat){
                    $active_parent = $cat->parent_id;
                }
            }
        }

        $result_prods = DB::select(
            'catalog.id',
            'catalog.alias',
            'catalog.parent_id',
            'catalog_i18n.name'
        )
            ->from('catalog')
            ->join('catalog_i18n')
                ->on('catalog_i18n.row_id', '=', 'catalog.id')
            ->where('catalog_i18n.language', '=', \I18n::$lang)
            ->where('catalog.status', '=', 1)
            ->order_by('catalog_i18n.name')
            ->find_all();
        if(sizeof($result_prods)){
            $products = array();
            foreach($result_prods as $prod){
                $products[$prod->parent_id][] = $prod;
            }
        }

        if(!count($categories) || !count($products)) {
            return NULL;
        }
        return array(
            'groups' => $categories,
            'products' => $products,
            'active_parent' => $active_parent,
            'active_cat' => $active_cat,
        );
    }

    public function Catalog_Cat1Menu() {
        $result_cats = CommonI18n::factory('catalog_tree')->getRows(1, 'catalog_tree.sort', 'ASC');
        $active_cat = Route::param('group');
        $active_parent = null;
        if(sizeof($result_cats)){
            $categories = array();
            foreach($result_cats as $cat){
                $categories[$cat->parent_id][] = $cat;
                if($cat->id = $active_cat){
                    $active_parent = $cat->parent_id;
                }
            }
        }

        $result_prods = CommonI18n::factory('catalog')->getRows(1, 'catalog_i18n.name', 'ASC');
        if(sizeof($result_prods)){
            $products = array();
            foreach($result_prods as $prod){
                $products[$prod->parent_id][] = $prod;
            }
        }

        if(!count($categories) || !count($products)) {
            return NULL;
        }
        return array(
            'groups' => $categories,
            'products' => $products,
            'active_parent' => $active_parent,
            'active_cat' => $active_cat,
        );
    }




    public function Contacts_Block() {
        if(!static::$_emails) {
            $result = Common::factory('contacts_emails')->getRows(1, 'sort', 'ASC');
            foreach($result AS $key => $value) {
                static::$_emails[$value->group][] = $value;
            }
        }
        if(!static::$_phones) {
            $result = Common::factory('contacts_phones')->getRows(1, 'sort', 'ASC');
            foreach($result AS $key => $value) {
                static::$_phones[$value->group][] = $value;
            }
        }
        return array('emails' => static::$_emails, 'phones' => static::$_phones);
    }


    public function News_Slider($input) {
        $result = News::getSlider(Arr::get($input, 'id'));
        $slider = array();
        foreach($result AS $key => $value) {
            if(is_file(HOST.HTML::media('images/news-slider/big/'.$value->image))) {
                $slider[] = $value;
            }
        }
        if(!count($slider)) {
            return NULL;
        }
        return array('slider' => $slider);
    }


    public function News_Items($input) {
        $result = News::getItems(Arr::get($input, 'id'));
        if(!sizeof($result)) {
            return NULL;
        }
        return array('result' => $result);
    }

}