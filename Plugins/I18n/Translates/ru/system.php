<?php
	return array(
		'js_disabled_string_1' => 'В Вашем браузере <b>отключен JavaScript!</b> Для корректной работы с сайтом необходима поддержка Javascript.',
		'js_disabled_string_2' => 'Мы рекомендуем Вам включить использование JavaScript в настройках вашего браузера.',
		'homepage_breadcrumbs' => 'Главная',
		'prev_pagination' => 'Назад',
		'next_pagination' => 'Вперед',
		'news_date' => 'Дата',
		'similar_news' => 'Похожие новости',
		'404_return_to' => 'Страница не найдена. Вернуться на',
		'404_homepage' => 'Главную',
		'404_title' => 'Ошибка 404! Страница не найдена',
		'search_res_string1' => 'По запросу',
		'search_res_string2' => 'найдены следующие товары',
		'no_result_found' => 'По вашему запросу ничего не найдено',
	);