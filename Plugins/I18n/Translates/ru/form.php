<?php
	return array(
		'site_search_placeholder' => 'Поиск по сайту',
		'contact_form' => 'форма обратной связи',
		'name' => 'Имя',
		'el_mail' => 'E-mail',
		'enter_your_message' => 'Введите сообщение...',
		'send' => 'отправить',
		'send_me_a_copy' => 'Отправить копию письма мне на почту',
	);