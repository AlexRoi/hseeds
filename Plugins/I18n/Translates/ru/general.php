<?php
	return array(
		'site_copyrights' => '© 2015 ООО «Трейдагрогруп»',
		'close' => 'Закрыть',
		'site_creation' => 'Разработка сайта',
		'readmore' => 'Подробнее',
		'our_address' => 'Наш адрес',
		'product_print' => 'Печать',
		'product_description' => 'Описание',
		'product_features' => 'Характеристики',
		'products' => 'Продукты',
		'product_f_type' => 'Разновидность ',
		'product_f_name' => 'Название',
		'product_f_maturity' => 'Созревание',
		'product_f_heat' => 'Теплостойкость',
		'product_f_harvest' => 'Урожай',
		'product_f_plant_size' => 'Размер растения',
		'product_f_color' => 'Цвет',
		'product_f_size' => 'Размер',
		'product_f_resistances' => 'Сопротивляемость',
	);