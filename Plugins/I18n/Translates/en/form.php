<?php
	return array(
		'site_search_placeholder' => 'Search',
		'contact_form' => 'contact form',
		'name' => 'Name',
		'el_mail' => 'E-mail',
		'enter_your_message' => 'Type your message',
		'send' => 'send',
		'send_me_a_copy' => 'Send a letter to my e-mail',
	);