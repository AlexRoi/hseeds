<?php
	return array(
		'js_disabled_string_1' => '<b>JavaScript! is switched off</b> in your browser.  For a proper site working JavaScript is required.',
		'js_disabled_string_2' => 'We recommend you to switch on the  JavaScript in your browser\'s settings',
		'homepage_breadcrumbs' => 'Main page',
		'prev_pagination' => 'Back',
		'next_pagination' => 'Forward',
		'news_date' => 'Date',
		'similar_news' => 'Similar news',
		'404_return_to' => 'Page has not been found. Back to',
		'404_homepage' => 'Main page',
		'404_title' => 'Error 404! Page hasn\'t been found',
		'search_res_string1' => 'On the request',
		'search_res_string2' => 'the following items have been found',
		'no_result_found' => 'No results have been found',
	);