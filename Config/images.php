<?php
    // Settings of images on the site
    return array(
        'watermark' => \Core\HTML::media('images/watermark.png'),
        // Image types
        'types' => array(
            'jpg', 'jpeg', 'png', 'gif',
        ),
        // Slider images
        'slider' => array(
            array(
                'path' => 'big',
                'width' => 700,
                'height' => 500,
                'resize' => 1,
                'crop' => 1,
            ),
            array(
                'path' => 'original',
                'resize' => 0,
                'crop' => 0,
            ),
        ),
        // News images
        'news' => array(
            array(
                'path' => 'small',
                'width' => 200,
                'height' => 140,
                'resize' => 1,
            ),
            array(
                'path' => 'medium',
                'width' => 330,
                'height' => 240,
                'resize' => 1,
            ),
            array(
                'path' => 'original',
                'resize' => 0,
                'crop' => 0,
            ),
        ),
        // Products images
        'catalog' => array(
            array(
                'path' => 'medium',
                'width' => 230,
                'height' => 230,
                'resize' => 1,
                'crop' => 1,
            ),
            array(
                'path' => 'big',
                'width' => 700,
                'height' => 400,
                'resize' => 1,
                'crop' => 1,
            ),
            array(
                'path' => 'original',
                'resize' => 0,
                'crop' => 0,
            ),
        ),
        // Products images
        'gallery' => array(
            array(
                'path' => 'small',
                'width' => 250,
                'height' => 170,
                'resize' => 1,
                'crop' => 1,
            ),
            array(
                'path' => 'original',
                'resize' => 0,
                'crop' => 0,
            ),
        ),
        'gallery_images' => array(
            array(
                'path' => 'small',
                'width' => 250,
                'height' => 170,
                'resize' => 1,
                'crop' => 1,
            ),
            array(
                'path' => 'original',
                'resize' => 0,
                'crop' => 0,
            ),
        ),
        'content' => array(
            array(
                'path' => 'big',
                'width' => 960,
                'height' => 200,
                'resize' => 1,
                'crop' => 1,
            ),
            array(
                'path' => 'original',
                'resize' => 0,
                'crop' => 0,
            ),
        ),
        'control' => array(
            array(
                'path' => 'big',
                'width' => 960,
                'height' => 200,
                'resize' => 1,
                'crop' => 1,
            ),
            array(
                'path' => 'original',
                'resize' => 0,
                'crop' => 0,
            ),
        ),
    );