<!DOCTYPE html>
<html lang="<?php echo I18n::$lang;?>" dir="ltr">
<head>
    <?php echo Core\Widgets::get('Head');
    foreach($_seo['scripts']['head'] as $script){
        echo $script;
    }
    echo $GLOBAL_MESSAGE; ?>
</head>
<body>
    <?php foreach($_seo['scripts']['body'] as $script){ echo $script; } ?>
    <div class="wWrapper">
        <?php if($_seo_text){ ?><div class="seoTxt" id="seoTxt"><div class="wSize"><div class="wTxt"><?php echo $_seo['seo_text']; ?></div></div></div><?php } ?>
        <?php echo Core\Widgets::get('Header'); ?>
        <div class="wConteiner">
            <div class="wSize">
                <div class="wLeft">
                    <?php echo Core\Widgets::get('Index_Gallery'); ?>
                </div>
                <!-- .wLeft -->
                <div class="wMiddle">
                    <?php echo Core\Widgets::get('Index_Slider'); ?>
                </div>
                <!-- .wMiddle -->
                <?php if($_seo_text){ ?><div class="page-section"><div id="clonSeo"></div></div><?php } ?>
            </div>
            <!-- .wSize -->
        </div>
        <!-- .wConteiner -->
    </div>
    <?php echo Core\Widgets::get('Footer', array('counters' => $_seo['scripts']['counter'])); ?>
    <?php echo Core\Widgets::get('HiddenData'); ?>
</body>
</html>