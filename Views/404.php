<!DOCTYPE html>
<html lang="<?php echo I18n::$lang;?>" dir="ltr">
<head>
    <?php echo Core\Widgets::get('Head');
    foreach($_seo['scripts']['head'] as $script){
        echo $script;
    }
    echo $GLOBAL_MESSAGE; ?>
</head>
<body>
    <?php foreach($_seo['scripts']['body'] as $script){ echo $script; } ?>
    <div class="page_404">
        <a href="<?php echo \Core\HTML::link(''); ?>" class="logo"><img src="<?php echo \Core\HTML::media('pic/logo.png'); ?>"></a>
        <div class="title_404">404</div>
        <p class="text_404"><?php echo __('404_return_to'); ?> <a href="<?php echo \Core\HTML::link(''); ?>"><?php echo __('404_homepage'); ?></a></p>
    </div>
    <?php echo Core\Widgets::get('Footer', array('counters' => $_seo['scripts']['counter'])); ?>
    <?php echo Core\Widgets::get('HiddenData'); ?>
</body>
</html>