<ul class="wResultList">
    <?php if(count($result)){
        foreach($result as $obj){ ?>
            <li class="wResultItem">
                <a class="wResultLink" href="<?php echo Core\HTML::link($obj->alias.'/p'.$obj->id); ?>"><?php echo $obj->name; ?></a>
                <p><?php echo Core\Text::limit_words(strip_tags($obj->text), 20, '...'); ?></p>
                <a class="wResultLinkBot" href="<?php echo Core\HTML::link($obj->alias.'/p'.$obj->id); ?>"><?php echo __('readmore'); ?></a>
            </li>
        <?php }
    } else { ?>
        <li class="wResultItem">
            <p><?php echo __('no_result_found'); ?></p>
        </li>
    <?php } ?>
</ul>
<?php echo $pager; ?>