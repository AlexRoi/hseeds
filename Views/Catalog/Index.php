<div class="wTxt">
    <h1><?php echo $obj->h1 ? $obj->h1 : $obj->name; ?></h1>
    <?php echo $obj->text; ?>
</div>
<?php if(count($products)){ ?>
    <div class="wCatList">
        <div class="fll wCatImg">
            <?php foreach($products as $product) {
                if(is_file(HOST.\Core\HTML::media('images/catalog/medium/'.$product->image))){ ?>
                    <img src="<?php echo \Core\HTML::media('images/catalog/medium/'.$product->image); ?>">
                <?php } else { ?>
                    <img src="<?php echo \Core\HTML::media('pic/no-image-product.png'); ?>">
                <?php }
                break;
            } ?>
        </div>
        <div class="flr wCatListBlock">
            <ul class="wCatListMain">
                <?php foreach($products as $product){ ?>
                    <li class="wCatItem">
                        <?php if(is_file(HOST.\Core\HTML::media('images/catalog/medium/'.$product->image))){
                            $product->data_img = \Core\HTML::media('images/catalog/medium/'.$product->image);
                        } else {
                            $product->data_img = \Core\HTML::media('pic/no-image-product.png');
                        } ?>
                        <a href="<?php echo Core\HTML::link($product->alias.'/p'.$product->id); ?>" class="wCatLink" data-img="<?php echo $product->data_img; ?>"><?php echo $product->name; ?></a>
                    </li>
                <?php } ?>
            </ul>
        </div>
        <div class="clear"></div>
    </div>
<?php } ?>