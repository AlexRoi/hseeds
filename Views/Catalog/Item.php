<div class="wTxt fll">
    <h1><?php echo $obj->h1 ? $obj->h1 : $obj->name; ?></h1>
</div>
<div class="flr wPrint">
    <a href="#" onclick="window.print();">
        <svg class="svg_print"> <use xlink:href="#print" /></svg>
        <p><?php echo __('product_print'); ?></p>
    </a>
</div>
<div class="clear"></div>
<?php $count_images = 0;
foreach($images as $im){
    if(is_file(HOST.Core\HTML::media('images/catalog/big/'.$im->image))){ $count_images++; }
}
if($count_images){ ?>
    <div class="wItemSlider">
        <ul class="wItemSliderList">
            <?php foreach($images as $im){ ?>
                <?php if(is_file(HOST.Core\HTML::media('images/catalog/big/'.$im->image))){ ?>
                    <li class="wItemSliderItem"><img src="<?php echo Core\HTML::media('images/catalog/big/'.$im->image); ?>"></li>
                <?php } ?>
            <?php } ?>
        </ul>
        <div class="prev">
            <svg class="svg_contr"> <use xlink:href="#prev" /></svg>
        </div>
        <div class="next">
            <svg class="svg_contr"> <use xlink:href="#next" /></svg>
        </div>
    </div>
<?php } ?>
<div class="wItemOpus">
    <div class="fll wItemDescription">
        <?php if(trim(strip_tags($obj->text))){ ?>
            <div class="wTxt">
                <h6><?php __('product_description'); ?></h6>
                <?php echo $obj->text; ?>
            </div>
        <?php } ?>
    </div>
    <div class="flr wItemCharacteristics">
        <div class="wTxt">
            <h6><?php __('product_features'); ?></h6>
        </div>
        <ul class="wItemChList">
            <?php if($obj->parent_name){ ?>
                <li class="wItemChItem">
                    <div class="fll">
                        <?php echo __('product_f_type'); ?>:
                    </div>
                    <div class="flr">
                        <?php echo $obj->parent_name; ?>
                    </div>
                    <div class="clear"></div>
                </li>
            <?php }
            if($obj->name){ ?>
                <li class="wItemChItem">
                    <div class="fll">
                        <?php echo __('product_f_name'); ?>:
                    </div>
                    <div class="flr">
                        <?php echo $obj->name; ?>
                    </div>
                    <div class="clear"></div>
                </li>
            <?php }
            if($obj->maturity){ ?>
                <li class="wItemChItem">
                    <div class="fll">
                        <?php echo __('product_f_maturity'); ?>:
                    </div>
                    <div class="flr">
                        <?php echo $obj->maturity; ?>
                    </div>
                    <div class="clear"></div>
                </li>
            <?php }
            if($obj->heat_low){ ?>
                <li class="wItemChItem">
                    <div class="fll">
                        <?php echo __('product_f_heat'); ?>:
                    </div>
                    <div class="flr">
                        <?php echo $obj->heat_low; ?>
                    </div>
                    <div class="clear"></div>
                </li>
            <?php }
            if($obj->harvest){ ?>
                <li class="wItemChItem">
                    <div class="fll">
                        <?php echo __('product_f_harvest'); ?>:
                    </div>
                    <div class="flr">
                        <?php echo $obj->harvest; ?>
                    </div>
                    <div class="clear"></div>
                </li>
            <?php }
            if($obj->plant_size){ ?>
                <li class="wItemChItem">
                    <div class="fll">
                        <?php echo __('product_f_plant_size'); ?>:
                    </div>
                    <div class="flr">
                        <?php echo $obj->plant_size; ?>
                    </div>
                    <div class="clear"></div>
                </li>
            <?php }
            if($obj->color){ ?>
                <li class="wItemChItem">
                    <div class="fll">
                        <?php echo __('product_f_color'); ?>:
                    </div>
                    <div class="flr">
                        <?php echo $obj->color; ?>
                    </div>
                    <div class="clear"></div>
                </li>
            <?php }
            if($obj->size_large){ ?>
                <li class="wItemChItem">
                    <div class="fll">
                        <?php echo __('product_f_size'); ?>:
                    </div>
                    <div class="flr">
                        <?php echo $obj->size_large; ?>
                    </div>
                    <div class="clear"></div>
                </li>
            <?php }
            if($obj->resistances){ ?>
                <li class="wItemChItem">
                    <div class="fll">
                        <?php echo __('product_f_resistances'); ?>:
                    </div>
                    <div class="flr">
                        <?php echo $obj->resistances; ?>
                    </div>
                    <div class="clear"></div>
                </li>
            <?php } ?>
        </ul>
    </div>
    <div class="clear"></div>
</div>