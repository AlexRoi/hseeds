<div class="page-section">
    <div class="catalog">
        <ul class="catalog__list">
            <?php foreach($result AS $obj): ?>
                <?php $image = \Core\HTML::media('pic/no-image/627x432.jpg') ?>
                <?php if(is_file(HOST.\Core\HTML::media('images/catalog-tree/big/'.$obj->image))): ?>
                    <?php $image = \Core\HTML::media('images/catalog-tree/big/'.$obj->image); ?>
                <?php endif; ?>
                <li class="catalog__item">
                    <a href="<?php echo \Core\HTML::link('products/'.$obj->alias); ?>" class="catalog__item-inner" title="<?php echo $obj->name; ?>" style="background-image: url('<?php echo $image; ?>');">
                        <span class="catalog__item-title"><?php echo $obj->name; ?></span>
                        <span class="catalog__item-btn"><?php echo __('Подробнее'); ?></span>
                    </a>
                </li>
            <?php endforeach; ?>
            <li class="catalog__item catalog__item--w100p">
                <a href="<?php echo \Core\HTML::link('order-only'); ?>" class="catalog__item-inner" title="<?php echo __('Под заказ'); ?>" style="background-image: url('<?php echo \Core\HTML::media('pic/catalog-item-bg9.jpg'); ?>');">
                    <span class="catalog__item-title"><?php echo __('Под заказ'); ?></span>
                    <span class="catalog__item-btn"><?php echo __('Подробнее'); ?></span>
                </a>
            </li>
        </ul>
    </div>
</div>
