<div class="wSliderMain">
    <ul>
        <?php foreach($result AS $obj){ ?>
            <li>
                <img src="<?php echo \Core\HTML::media('images/slider/big/'.$obj->image); ?>" alt="<?php echo $obj->name; ?>">
                <div class="wSlideOpus">
                    <div class="wSlideTitle"><?php echo $obj->name; ?></div>
                    <div class="wSlideText"><?php echo $obj->text; ?></div>
                    <?php if(trim($obj->url)){ ?>
                        <a href="<?php echo \Core\HTML::link($obj->url); ?>" class="wSlideBut"><?php echo __('readmore'); ?></a>
                    <?php } ?>
                </div>
            </li>
        <?php } ?>
    </ul>
</div>