<?php use Core\HTML; ?>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo isset($title) ? $title : NULL; ?></title>
<meta name="description" lang="ru-ru" content="<?php echo isset($description) ? $description : NULL; ?>">
<meta name="keywords" lang="ru-ru" content="<?php echo isset($keywords) ? $keywords : NULL; ?>">
<meta name="author" lang="ru-ru" content="Holland Seeds">
<meta property="og:title" content="<?php echo isset($title) ? $title : NULL; ?>">
<meta property="og:type" content="website">
<meta property="og:url" content="<?php echo HTML::link(htmlspecialchars($_SERVER['REQUEST_URI']), true); ?>">
<meta property="og:image" content="<?php echo HTML::media('pic/logo.png'); ?>">
<meta property="og:description" content="<?php echo isset($description) ? $description : NULL; ?>">
<meta property="og:site_name" content="<?php echo $_SERVER['HTTP_HOST']; ?>">
<!--[if IE]><meta http-equiv="imagetoolbar" content="no"><![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="width=960">
<!-- saved from url=(0014)about:internet -->
<meta name="format-detection" content="telephone=no">
<meta name="format-detection" content="address=no">
<link rel="apple-touch-icon" sizes="57x57" href="<?php echo HTML::media('favicons/apple-touch-icon-57x57.png'); ?>">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo HTML::media('favicons/apple-touch-icon-60x60.png'); ?>">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo HTML::media('favicons/apple-touch-icon-72x72.png'); ?>">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo HTML::media('favicons/apple-touch-icon-76x76.png'); ?>">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo HTML::media('favicons/apple-touch-icon-114x114.png'); ?>">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo HTML::media('favicons/apple-touch-icon-120x120.png'); ?>">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo HTML::media('favicons/apple-touch-icon-144x144.png'); ?>">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo HTML::media('favicons/apple-touch-icon-152x152.png'); ?>">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo HTML::media('favicons/apple-touch-icon-180x180.png'); ?>">
<link rel="icon" type="image/png" href="<?php echo HTML::media('favicons/favicon-32x32.png'); ?>" sizes="32x32">
<link rel="icon" type="image/png" href="<?php echo HTML::media('favicons/favicon-194x194.png'); ?>" sizes="194x194">
<link rel="icon" type="image/png" href="<?php echo HTML::media('favicons/favicon-96x96.png'); ?>" sizes="96x96">
<link rel="icon" type="image/png" href="<?php echo HTML::media('favicons/android-chrome-192x192.png'); ?>" sizes="192x192">
<link rel="icon" type="image/png" href="<?php echo HTML::media('favicons/favicon-16x16.png'); ?>" sizes="16x16">
<link rel="manifest" href="<?php echo HTML::media('favicons/manifest.json'); ?>">
<meta name="msapplication-config" content="browserconfig.xml">
<meta name="msapplication-TileColor" content="#b91d47">
<meta name="msapplication-TileImage" content="<?php echo HTML::media('favicons/mstile-144x144.png'); ?>">
<meta name="theme-color" content="#ffffff">
<meta name="apple-mobile-web-app-title" content="<?php echo isset($title) ? $title : NULL; ?>">
<meta name="application-name" content="Holland Seeds">
<meta name="msapplication-tooltip" content="<?php echo isset($description) ? $description : NULL; ?>">
<style>.seoTxt{position:absolute;top:100%;left:0;width:100%;}</style>
<?php $css = Minify_Core::factory('css')->minify($styles);
foreach($css as $file_style){
    echo HTML::style($file_style)."\n";
} ?>
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<script>!function(t){function r(){var t=navigator.userAgent,r=!window.addEventListener||t.match(/(Android (2|3|4.0|4.1|4.2|4.3))|(Opera (Mini|Mobi))/)&&!t.match(/Chrome/);if(r)return!1;var e="test";try{return localStorage.setItem(e,e),localStorage.removeItem(e),!0}catch(o){return!1}}t.localSupport=r(),t.localWrite=function(t,r){try{localStorage.setItem(t,r)}catch(e){if(e==QUOTA_EXCEEDED_ERR)return!1}}}(window);</script>
<?php $js = Minify_Core::factory('js')->minify($scripts);
foreach ($js as $file_script){
    echo HTML::script($file_script)."\n";
}
foreach($scripts_no_minify as $file_script) {
    echo HTML::script($file_script)."\n";
} ?>

