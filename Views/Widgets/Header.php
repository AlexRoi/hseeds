<header class="wHeader">
    <div class="wSize">
        <div class="fll wLogo">
            <?php if(\Core\Route::controller() == 'index'){ ?>
                <a class="wLogoLink"><img src="<?php echo \Core\HTML::media('pic/logo.png'); ?>" alt="Holland Seeds"></a>
            <?php } else { ?>
                <a href="<?php echo \Core\HTML::link(''); ?>" class="wLogoLink"><img src="<?php echo \Core\HTML::media('pic/logo.png'); ?>" alt="Holland Seeds"></a>
            <?php } ?>
        </div>
        <div class="flr wHeaderControls">
            <div class="wTopCon">
                <div class="wLanguage">
                    <?php foreach($languages as $obj){ ?>
                        <a <?php echo (I18n::$lang == $obj->alias) ? '' : 'href="'.\Core\HTML::changeLanguage($obj->alias).'"';?> class="wLangLink wLangLink<?php echo strtoupper($obj->alias); ?> <?php echo (I18n::$lang == $obj->alias)? 'cur' : '';?>">
                            <span class="wFlag"></span>
                            <p><?php echo $obj->short_name; ?></p>
                        </a>
                    <?php } ?>
                </div>
                <form class="wFind wForm" data-form="true" action="/<?php echo I18n::$lang; ?>/search">
                    <div class="wFormRow">
                        <input type="search" required="true" minlength="3" name="query" placeholder="<?php echo __('site_search_placeholder'); ?>">
                    </div>
                    <button class="wSubmit">
                        <svg class="svg_search"> <use xlink:href="#icon_search" /></svg>
                    </button>
                </form>
            </div>
            <div class="wBotCon">
                <ul class="wMenu">
                    <?php foreach($menu[0] AS $key => $value){ ?>
                        <li class="wMenuList"><a class="wMenuLink <?php if(str_replace(I18n::$lang, '', $_SERVER['REQUEST_URI']) == '/' && $value->url == '/'){
                                echo 'cur';
                            } elseif((stripos($_SERVER['REQUEST_URI'], $value->url) !== false && $value->url != '/')){
                                echo 'cur';
                            } ?>" href="<?php echo \Core\HTML::link($value->url); ?>"><?php echo $value->name; ?></a></li>
                    <?php } ?>
                </ul>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
</header>
<!-- .wHeader -->