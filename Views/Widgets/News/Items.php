<div class="page-section">
    <div class="page-section__title tac"><span><?php echo __('Товары в тему'); ?></span></div>
    <div class="shop-items-list">
        <?php foreach($result AS $obj): ?>
            <?php echo \Core\Widgets::get('Catalog_ItemTpl', array('obj' => $obj), false); ?>
        <?php endforeach; ?>
    </div>
</div>