<footer class="wFooter">
    <div class="wSize">
        <div class="fll">
            <div class="wMenuBot">
                <ul class="wMenuB">
                    <?php foreach($menu[0] AS $key => $value){ ?>
                        <li class="wMenuBList"><a class="wMenuBLink <?php if(stripos($_SERVER['REQUEST_URI'], $value->url) !== false){ echo 'cur'; } ?>" href="<?php echo \Core\HTML::link($value->url); ?>"><?php echo $value->name; ?></a></li>
                    <?php } ?>
                </ul>
            </div>
            <div class="wCopywrite"><?php echo __('site_copyrights'); ?></div>
        </div>
        <div class="flr">
            <div class="wSocial">
                <div class="social-likes">
                    <div class="facebook" title="Поделиться ссылкой на Фейсбуке">Facebook</div>
                    <div class="vkontakte" title="Поделиться ссылкой во Вконтакте">Вконтакте</div>
                </div>
            </div>
            <a target="_blank" class="wezom" href="http://wezom.com.ua"><?php echo __('site_creation'); ?> - <span>Wezom</span></a>
        </div>
        <?php if($counters){
            foreach($counters as $counter){
                echo $counter;
            }
        } ?>
        <div class="clear"></div>
    </div>
</footer>
<!-- .wFooter -->