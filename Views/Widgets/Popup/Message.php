<div class="mfiModal big zoomAnim">
    <div class="popup-header">
        <div class="popup-header__img-holder">
            <img src="<?php echo \Core\HTML::media('pic/logo--shtamp.png'); ?>" alt="">
        </div>
        <div class="popup-header__title">
            <img src="<?php echo \Core\HTML::media('pic/popup-heading.png'); ?>" alt="">
            <p><?php echo __('HD-Moda - Ваш интернет магазин'); ?></p>
        </div>
    </div>
    <div id="enterReg" class="popup-body">
        <div class="enterReg_top no_bg">
            <div class="popupBlock enterBlock wCur">
                <p class="popupContent erTitle" style="margin-bottom: 0; font-size: 18px;"><?php echo $msg; ?></p>
            </div>
        </div>
    </div>
</div>