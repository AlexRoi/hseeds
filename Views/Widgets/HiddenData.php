<!-- WEZOM DEFS -->
<noscript>
    <input id="wzmMsg_JsClose" type="checkbox" title="<?php echo __('close'); ?>">
    <div id="wzmMsg_JsInform" class="wzmMsg_Wrapp">
        <div class="wzmMsg_Text">
            <p><?php echo __('js_disabled_string_1'); ?></p>
            <p><?php echo __('js_disabled_string_2'); ?></p>
        </div>
        <a class="wzmMsg_Link" href="http://wezom.com.ua/" target="_blank" title="Wezom Studio">
            <img src="<?php echo \Core\HTML::media('pic/wezom-info-red.gif'); ?>" width="50" height="18" alt="Wezom Studio">
        </a>
        <label for="wzmMsg_JsClose" class="wzmMsg_Close"><span>&times;</span></label>
    </div>
</noscript>
<script>
    function $wzmOldInit(){var scrpt=document.createElement("script");scrpt.src="http://verstka.vps.kherson.ua/sources/plugins/wold/wold.js";document.body.appendChild(scrpt);};try{document.addEventListener("DOMContentLoaded",$wzmOldInit,false)}catch(e){window.attachEvent("onload",$wzmOldInit)}
</script>
<!-- end WEZOM DEFS -->
<!-- SVG-SPRITE -->
<script>var spriteSvg={ns:"http://www.w3.org/2000/svg",initialize:function(e,t){if(this.prefix="spriteSvg_"+e.id,window.localSupport){var i=window.localStorage[this.prefix];if(i){var n=JSON.parse(i);this.setSprite(e,n)}else this.getJson(e,t)}else this.getJson(e,t)},getJson:function(e,t){var i=new XMLHttpRequest,n=this;i.open("GET",t,!0),i.setRequestHeader("Content-type","application/json"),i.onreadystatechange=function(){if(4==i.readyState&&200==i.status){var t=JSON.parse(i.responseText);n.setSprite(e,t),window.localSupport&&window.localWrite(n.prefix,JSON.stringify(t))}},i.send()},buildElem:function(e,t){var i,n,s;for(i in t)for(n in t[i]){var o=document.createElementNS(this.ns,n);for(s in t[i][n])"stops"===s?(this.buildElem(o,t[i][n][s])):o.setAttributeNS(null,s,t[i][n][s]);e.appendChild(o)}},isDone:function(){},setSprite:function(e,t){for(key in t){var i=t[key],n=document.createElementNS(this.ns,"symbol");n.setAttributeNS(null,"id",key),n.setAttributeNS(null,"viewBox",i.viewBox),this.buildElem(n,i.symbol),i.hasOwnProperty("gradients")&&this.buildElem(e,i.gradients),e.appendChild(n)}this.isDone(e)}};
</script>
<svg id="sprite" xmlns="http://www.w3.org/2000/svg" style="height:0;width:0;visibility:hidden;position:absolute;top:0;left:0;" onload="spriteSvg.initialize(this,'/Media/js/sprite-svg.json')"></svg>
<!-- end SVG-SPRITE -->