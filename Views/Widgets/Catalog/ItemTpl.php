<div class="shop-item">
    <div class="shop-item__inner">
        <div class="shop-item__img-container">
            <a href="<?php echo \Core\HTML::link($obj->alias.'/p'.$obj->id); ?>" title="<?php echo $obj->name; ?>">
                <?php if(is_file(HOST.\Core\HTML::media('images/catalog/small/'.$obj->image))): ?>
                    <img src="<?php echo \Core\HTML::media('images/catalog/small/'.$obj->image); ?>" alt="<?php echo $obj->name; ?>" title="<?php echo $obj->name; ?>">
                <?php else: ?>
                    <img src="<?php echo \Core\HTML::media('pic/no-image/210x320.jpg'); ?>" alt="<?php echo $obj->name; ?>" title="<?php echo $obj->name; ?>">
                <?php endif; ?>
                <div class="shop-item__hover-ico">
                    <svg>
                        <use xlink:href="#icon_search" />
                    </svg>
                </div>
            </a>
        </div>
        <div class="shop-item__separator"></div>
        <div class="shop-item__name">
            <a href="<?php echo \Core\HTML::link($obj->alias.'/p'.$obj->id); ?>" title="<?php echo $obj->name; ?>"><?php echo $obj->name; ?></a>
        </div>
        <div class="shop-item__price-container">
            <?php if($obj->sale): ?>
                <span class="shop-item__price shop-item__price--old"><?php echo \Core\Support::price($obj->price_old); ?></span>
            <?php endif; ?>
            <span class="shop-item__price"><?php echo \Core\Support::price($obj->price); ?></span>
        </div>
        <a href="<?php echo \Core\HTML::link($obj->alias.'/p'.$obj->id); ?>" title="<?php echo $obj->name; ?>" class="shop-item__btn"><?php echo __('Купить'); ?></a>
        <label class="shop-item__like" data-id="<?php echo $obj->id; ?>">
            <input type="checkbox" name="item1">
            <i></i>
        </label>
    </div>
</div>