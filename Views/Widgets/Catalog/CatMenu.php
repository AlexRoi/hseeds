<div class="wAccordeon">
    <ul class="wAccList">
        <?php foreach($groups[0] as $level1){ ?>
            <li class="wAccItem">
                <div class="wAccOpen <?php if($level1->id == $active_cat || $level1->id == $active_parent){ echo 'open';} ?>"><?php echo $level1->name; ?></div>
                <?php if(isset($groups[$level1->id]) || isset($products[$level1->id]))?>
                <ul class="wAccList wAccList2" <?php if($level1->id == $active_cat || $level1->id == $active_parent){ echo 'style="display:block"';} ?>>
                    <?php if(isset($groups[$level1->id])){ ?>
                        <?php foreach($groups[$level1->id] as $level2){ ?>
                            <li class="wAccItem">
                                <div class="wAccOpen wAccOpen2 <?php if($level2->id == $active_cat){ echo 'open';} ?>"><?php echo $level2->name; ?></div>
                                <?php if(isset($products[$level2->id])){ ?>
                                    <ul class="wAccList wAccList3" <?php if($level2->id == $active_cat){ echo 'style="display:block"';} ?>>
                                        <?php foreach($products[$level2->id] as $prod){ ?>
                                            <li><a href="<?php echo Core\HTML::link($prod->alias.'/p'.$prod->id); ?>" class="wAccLink"><?php echo $prod->name; ?></a></li>
                                        <?php } ?>
                                    </ul>
                                <?php } ?>
                            </li>
                        <?php } ?>
                    <?php } ?>
                    <?php if(isset($products[$level1->id])){ ?>
                        <?php foreach($products[$level1->id] as $prod){ ?>
                            <li class="wAccItem">
                                <a href="<?php echo Core\HTML::link($prod->alias.'/p'.$prod->id); ?>" class="wAccLink"><?php echo $prod->name; ?></a>
                            </li>
                        <?php } ?>
                    <?php } ?>
                </ul>
            </li>
        <?php } ?>
    </ul>
</div>
