<div class="contact-map__info">
    <div class="contact-map__info-header">
        <div class="contact-map__info-img">
            <img src="<?php echo \Core\HTML::media('pic/logo--shtamp.png'); ?>" alt="">
        </div>
        <div class="contact-map__info-title">
            <img src="<?php echo \Core\HTML::media('pic/popup-heading.png'); ?>" alt="">
            <p><?php echo __('Интернет-магазин'); ?></p>
        </div>
    </div>
    <div class="contact-map__info-body">
        <div class="contact-map__info-row tal">
            <?php if(count(\Core\Arr::get($phones, 2))): ?>
                <a class="link link--pink" href="tel:<?php echo preg_replace('/[^0-9]/', '', $phones[2][0]->name); ?>" title="<?php echo $phones[2][0]->name; ?>">телефон</a>
            <?php endif; ?>
            <a class="link link--pink" title="viber: hd-moda">hd-moda</a>
            <a class="link link--pink" title="whatsapp: hd-moda">whatsapp</a>
        </div>
        <?php if(count(\Core\Arr::get($phones, 2))): ?>
            <div class="contact-map__info-row">
                <?php foreach($phones[2] AS $key => $value): ?>
                    <a class="link link--big" href="tel:<?php echo preg_replace('/[^0-9]/', '', $value->name); ?>" title="<?php echo $value->name; ?>"><?php echo $value->name; ?></a>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
        <?php if(count(\Core\Arr::get($emails, 2))): ?>
            <div class="contact-map__info-row">
                <?php foreach($emails[2] AS $key => $value): ?>
                    <a class="link link--medium" href="mailto:<?php echo $value->name; ?>" title="<?php echo $value->name; ?>"><?php echo $value->name; ?></a>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
</div>