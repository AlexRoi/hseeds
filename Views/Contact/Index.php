<?php if(is_file(HOST.Core\HTML::media('images/control/big/'.$obj->image))) { ?>
    <div class="wContactBanner"><img src="<?php echo Core\HTML::media('images/control/big/'.$obj->image); ?>"></div>
<?php } ?>
<div class="wContactBlock">
    <div class="fll wContactInfo">
        <div class="wContactTitle">
            <svg class="svg_info"> <use xlink:href="#info" /></svg>
            <p><?php echo __('our_address'); ?></p>
        </div>
        <div class="wContactInfoInn">
            <?php echo $obj->text; ?>
        </div>
    </div>
    <div class="flr wContactForm">
        <div class="wContactTitle">
            <div class="wContactFormIco"></div>
            <p><?php echo __('contact_form'); ?></p>
        </div>
        <div class="wForm wFormDef" data-form="true" data-ajax="form/contacts">
            <div class="wFormRow small">
                <input data-name="name" type="text" data-rule-word="true" minlength="2" placeholder="<?php echo __('name'); ?>" name="contacts-name">
                <div class="inpInfo"><?php echo __('name'); ?></div>
            </div>
            <div class="wFormRow small">
                <input data-name="email" type="email" data-rule-email="true" required="true" placeholder="<?php echo __('el_mail'); ?>*" name="contacts-email">
                <div class="inpInfo"><?php echo __('el_mail'); ?>*</div>
            </div>
            <div class="clear"></div>
            <div class="wFormRow">
                <textarea data-name="text" minlength="10" required="true" placeholder="<?php echo __('enter_your_message'); ?>*" name="contacts-message"></textarea>
                <div class="inpInfo"><?php echo __('enter_your_message'); ?>*</div>
            </div>
            <div class="tal">
                <?php if(array_key_exists('token', $_SESSION)){ ?>
                    <input type="hidden" data-name="token" value="<?php echo $_SESSION['token']; ?>" />
                <?php } ?>
                <button class="wSubmit"><?php echo __('send'); ?></button>
                <label class="checkBlock">
                    <input data-name="copy" type="checkbox" name="send_copy" checked="checked">
                    <ins></ins>
                    <span><?php echo __('send_me_a_copy'); ?></span>
                </label>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>