<?php if(count($images)){ ?>
    <div class="wCompSlider">
        <ul class="wCompSliderList">
            <?php foreach($images as $image){ ?>
                <li class="wCompSliderItem"><img src="<?php echo Core\HTML::media('images/gallery_images/small/'.$image->image); ?>"></li>
            <?php } ?>
        </ul>
        <div class="prev">
            <svg class="svg_contr"> <use xlink:href="#prev" /></svg>
        </div>
        <div class="next">
            <svg class="svg_contr"> <use xlink:href="#next" /></svg>
        </div>
    </div>
<?php } ?>