<?php $controller = Core\Route::controller();
if(is_file(HOST.Core\HTML::media('images/'.$controller.'/big/'.$obj->image))) { ?>
    <div class="wContactBanner"><img src="<?php echo Core\HTML::media('images/'.$controller.'/big/'.$obj->image); ?>"></div>
<?php } ?>
<div class="wTxt">
    <?php echo $obj->text;
    if (count($kids)){ ?>
        <ul>
            <?php foreach($kids as $obj){ ?>
                <li>
                    <a href="<?php echo Core\HTML::link($obj->alias); ?>"><?php echo $obj->name; ?></a>
                </li>
            <?php } ?>
        </ul>
    <?php } ?>
</div>