<div class="wNewsBlock">
    <div class="wNewText wTxt">
        <div class="fll wNewsImg">
            <span class="wNewsImgMain">
                <?php if(is_file(HOST.\Core\HTML::media('images/news/medium/'.$obj->image))){ ?>
                    <img src="<?php echo \Core\HTML::media('images/news/medium/'.$obj->image); ?>" alt="<?php echo $obj->name; ?>">
                <?php } else { ?>
                    <img src="<?php echo \Core\HTML::media('pic/no-image-news.png'); ?>" alt="<?php echo $obj->name; ?>">
                <?php } ?>
            </span>
            <span class="wNewsDate">
                <p><?php echo __('news_date'); ?>: <?php echo date('d.m.Y', $obj->date); ?></p>
            </span>
        </div>
        <?php echo $obj->text; ?>
    </div>
    <div class="clear"></div>
    <?php if(count($similar_news)) { ?>
        <div class="wNewSimilar">
            <div class="wTxt">
                <h5><?php echo __('similar_news'); ?></h5>
            </div>
            <ul class="wNewSimList">
                <?php foreach($similar_news as $news_item){ ?>
                    <li class="wNewSimItem">
                        <a class="wNewSimLink" href="<?php echo \Core\HTML::link('news/'.$news_item->alias); ?>">
                            <p><?php echo $news_item->name; ?><span><?php echo date('d.m.Y', $news_item->date); ?></span></p>
                        </a>
                    </li>
                <?php } ?>
            </ul>
        </div>
    <?php } ?>
</div>