<ul class="wNewsList">
    <?php foreach($result AS $obj){ ?>
        <li class="wNewsItem">
            <div class="fll wNewsImg">
                <a href="<?php echo \Core\HTML::link('news/'.$obj->alias); ?>" class="wNewsImgBlock">
                    <span class="wNewsImgMain">
                        <?php if(is_file(HOST.\Core\HTML::media('images/news/small/'.$obj->image))){ ?>
                            <img src="<?php echo \Core\HTML::media('images/news/small/'.$obj->image); ?>" alt="<?php echo $obj->name; ?>" title="<?php echo $obj->name; ?>">
                        <?php } else { ?>
                            <img src="<?php echo \Core\HTML::media('pic/no-image-news.png'); ?>" alt="<?php echo $obj->name; ?>" title="<?php echo $obj->name; ?>">
                        <?php } ?>
                    </span>
                    <span class="wNewsDate">
                        <p><?php echo __('news_date'); ?>: <?php echo date('d.m.Y', $obj->date); ?></p>
                    </span>
                </a>
            </div>
            <div class="ovh wNewsBlock">
                <a href="<?php echo \Core\HTML::link('news/'.$obj->alias); ?>" class="wNewsName" title="<?php echo $obj->name; ?>"><?php echo $obj->name; ?></a>
                <div class="wNewsText w1Txt">
                    <p><?php echo Core\Text::limit_words(strip_tags($obj->text), 90, '...'); ?></p>
                </div>
            </div>
            <div class="clear"></div>
        </li>
    <?php } ?>
</ul>
<?php echo $pager; ?>