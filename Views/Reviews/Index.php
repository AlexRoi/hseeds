<div class="page-section">
    <div class="reviews">
        <?php if(!sizeof($result)): ?>
            <div class="wTxt"><p><?php echo __('Отзывов нет'); ?></p></div>
        <?php else: ?>
            <ul class="reviews__list">
                <?php foreach($result AS $obj): ?>
                    <li class="reviews__item">
                        <div class="comment">
                            <div class="comment__header">
                                <div class="comment__header-row">
                                    <div class="comment__name"><span><?php echo $obj->name; ?></span></div>
                                    <div class="comment__date"><span><?php echo date('d.m.Y', $obj->date); ?></span></div>
                                </div>
                                <div class="comment__header-row">
                                    <div class="rating">
                                        <input type="radio" name="rating<?php echo $obj->id; ?>" value="r5" disabled <?php echo $obj->mark == 5 ? 'checked' : NULL; ?>>
                                        <i>5</i>
                                        <input type="radio" name="rating<?php echo $obj->id; ?>" value="r4" disabled <?php echo $obj->mark == 4 ? 'checked' : NULL; ?>>
                                        <i>4</i>
                                        <input type="radio" name="rating<?php echo $obj->id; ?>" value="r3" disabled <?php echo $obj->mark == 3 ? 'checked' : NULL; ?>>
                                        <i>3</i>
                                        <input type="radio" name="rating<?php echo $obj->id; ?>" value="r2" disabled <?php echo $obj->mark == 2 ? 'checked' : NULL; ?>>
                                        <i>2</i>
                                        <input type="radio" name="rating<?php echo $obj->id; ?>" value="r1" disabled <?php echo $obj->mark == 1 ? 'checked' : NULL; ?>>
                                        <i>1</i>
                                    </div>
                                </div>
                            </div>
                            <div class="comment__txt">
                                <?php echo nl2br($obj->text); ?>
                            </div>
                        </div>
                        <?php if(trim(strip_tags($obj->answer))): ?>
                            <div class="comment comment--answer">
                                <div class="comment__header">
                                    <div class="comment__header-row">
                                        <div class="comment__name"><span>Admin</span></div>
                                        <?php if($obj->date_answer): ?>
                                            <div class="comment__date"><span><?php echo date('d.m.Y', $obj->date_answer); ?></span></div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="comment__txt">
                                    <?php echo nl2br($obj->answer); ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    </li>
                <?php endforeach; ?>
                <li>
                    <div class="tac">
                        <?php echo $pager; ?>
                    </div>
                </li>
            </ul>
        <?php endif; ?>
        <div class="reviews__form wForm wFormDef" data-form="true" data-ajax="reviews">
            <div class="wFormRow tac">
                <div><b><?php echo __('Оставить отзыв'); ?></b></div>
            </div>
            <div class="wFormRow">
                <div class="wFormInput">
                    <input data-name="name" type="text" class="wInput" name="com_name" required data-rule-word="true" data-rule-minlength="2" data-msg-required="<?php echo __('Укажите Ваше имя'); ?>" placeholder="<?php echo __('Ваше имя'); ?>">
                    <div class="inpInfo"><?php echo __('Ваше имя'); ?></div>
                </div>
            </div>
            <div class="wFormRow tac">
                <div><span><?php echo __('Оценить обслуживание'); ?></span></div>
                <div class="rating rating--large">
                    <input type="radio" data-name="mark" name="mark" value="5" checked>
                    <i>5</i>
                    <input type="radio" data-name="mark" name="mark" value="4">
                    <i>4</i>
                    <input type="radio" data-name="mark" name="mark" value="3">
                    <i>3</i>
                    <input type="radio" data-name="mark" name="mark" value="2">
                    <i>2</i>
                    <input type="radio" data-name="mark" name="mark" value="1">
                    <i>1</i>
                </div>
            </div>
            <div class="wFormRow">
                <input data-name="email" type="email" class="wInput orValid" name="com_email" required data-rule-email="true" data-msg-required="<?php echo __('Укажите e-mail'); ?>" placeholder="E-mail">
                <div class="inpInfo">E-mail</div>
            </div>
            <div class="wFormRow">
                <div class="wFormInput">
                    <input data-name="city" type="text" class="wInput" name="com_city" required data-rule-word="true" data-rule-minlength="2" data-msg-required="<?php echo __('Укажите город'); ?>" placeholder="<?php echo __('Город'); ?>">
                    <div class="inpInfo"><?php echo __('Город'); ?></div>
                </div>
            </div>
            <div class="wFormRow">
                <div class="wFormInput">
                    <textarea data-name="text" class="wTextarea" name="com_textarea" required data-rule-minlength="10" data-msg-required="<?php echo __('Введите текст'); ?>" placeholder="<?php echo __('Отзыв'); ?>"></textarea>
                    <div class="inpInfo"><?php echo __('Отзыв'); ?></div>
                </div>
            </div>
            <?php if(array_key_exists('token', $_SESSION)): ?>
                <input type="hidden" data-name="token" value="<?php echo $_SESSION['token']; ?>" />
            <?php endif; ?>
            <div class="wFormRow tac">
                <button type="button" class="wSubmit wBtn lg primary"><span><?php echo __('Отправить'); ?></span></button>
            </div>
        </div>
    </div>
</div>